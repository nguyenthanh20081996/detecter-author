@extends('user.layout.user')
@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        <div class="col-md-12">
            <input type="text" class="search-idol" placeholder="Nhập tên để tìm kiếm idol..." autocomplete="off"
                autocorrect="off" autocapitalize="off" spellcheck="false">
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-2">
            <div class="pd-6">
                <span>Quốc Gia</span>
            </div>
        </div>
        <div class="col-md-2">
            <select id="select-country" class="filter-style">
                <option value="-1">Tất Cả</option>
                @foreach ($national as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <div class="pd-6">
                <span>Năm Sinh</span>
            </div>
        </div>
        <div class="col-md-2">
            <select id="select-year-from" class="filter-style">
                <option value="-1">---</option>
                <option value="1985">1985</option>
                <option value="1986">1986</option>
                <option value="1987">1987</option>
                <option value="1988">1988</option>
                <option value="1989">1989</option>
                <option value="1990">1990</option>
                <option value="1991">1991</option>
                <option value="1992">1992</option>
                <option value="1993">1993</option>
                <option value="1994">1994</option>
                <option value="1995">1995</option>
                <option value="1996">1996</option>
                <option value="1997">1997</option>
                <option value="1998">1998</option>
                <option value="1999">1999</option>
                <option value="2000">2000</option>
                <option value="2001">2001</option>
                <option value="2002">2002</option>
                <option value="2003">2003</option>
                <option value="2004">2004</option>
                <option value="2005">2005</option>
                <option value="2006">2006</option>
                <option value="2007">2007</option>
                <option value="2008">2008</option>
            </select>
        </div>
        <div class="col-md-2">
            <select id="select-year-to" class="filter-style">
                <option value="-1">---</option>
                <option value="1985">1985</option>
                <option value="1986">1986</option>
                <option value="1987">1987</option>
                <option value="1988">1988</option>
                <option value="1989">1989</option>
                <option value="1990">1990</option>
                <option value="1991">1991</option>
                <option value="1992">1992</option>
                <option value="1993">1993</option>
                <option value="1994">1994</option>
                <option value="1995">1995</option>
                <option value="1996">1996</option>
                <option value="1997">1997</option>
                <option value="1998">1998</option>
                <option value="1999">1999</option>
                <option value="2000">2000</option>
                <option value="2001">2001</option>
                <option value="2002">2002</option>
                <option value="2003">2003</option>
                <option value="2004">2004</option>
                <option value="2005">2005</option>
                <option value="2006">2006</option>
                <option value="2007">2007</option>
                <option value="2008">2008</option>
            </select>
        </div>
        <div class="col-md-2" style="text-align: right">
            <button class="btn-search" id="filter-idol" style="height: 35px;margin-bottom: 0px;min-width:100%;">Lọc</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px">
            <div class="idol-title" id="idol-change-title" style="padding: 3px 0px;border-bottom: 1px solid #cdcdcd;font-size:20px;">
                <span><i class="fa fa-cube" aria-hidden="true"></i> Tất Cả Idol ({{number_format($total)}})</span>
            </div>
        </div>
    </div>
    <div class="row idol-list"></div>
    <div class="row idol-search" style="display: none"></div>
    <div class="loading-more"></div>
    <div class="text-center" style="margin-top: 20px">
        <img class="loading-img" src="{{asset('img/loading-idol.gif')}}" width="50" style="display: none">
    </div>
</div>
@endsection
@section('script')
<script>
    var is_loading = true;
    var total = Math.ceil(parseInt('{{$total}}')/12);
    var count = 0;
    var base_url = "{{url('/')}}";
    var width = 0;

    var national = -1;
    var birth_year_from = -1;
    var birth_year_to = -1;

    $(".menu-list-idol").addClass("menu-active");
    $(document).ready(function(){
        var data = {
            page: count,
            national: national,
            from: birth_year_from,
            to: birth_year_to
        }

        $(".loading-img").show();
        getMoreIdol(data);

        $(".search-idol").keyup(function(){
            var search_value = $(this).val();

            if(String(search_value).trim() != ''){
                $(".idol-list").hide();
                $(".idol-search").show();
                var data = {
                    searching: search_value,
                    national: national,
                    from: birth_year_from,
                    to: birth_year_to
                }

                getSearchIdol(data);
            }else{
                $(".idol-list").show();
                $(".idol-search").hide();
            }
        });

        $('#filter-idol').click(function(){
            national = $('#select-country').val();
            birth_year_from = $('#select-year-from').val();
            birth_year_to = $('#select-year-to').val();
            count = 0;

            $(".idol-list").show();
            $(".idol-search").hide();
            $(".search-idol").val('');

            var data = {
                page: count,
                national: national,
                from: birth_year_from,
                to: birth_year_to
            }
            $(".loading-img").show();
            $(".idol-list").html('');

            getNewTotalIdol(data);
        });

        $(window).scroll(function(){
            var position = $('.loading-more').position();
            if ($(window).scrollTop() + $(window).height() >= position.top){
                if (count >= total){
                    return false;
                }else{
                    if(is_loading){
                        is_loading = false;
                        var data = {
                            page:count,
                            national: national,
                            from: birth_year_from,
                            to: birth_year_to
                        }
                        $(".loading-img").show();
                        getMoreIdol(data);
                    }
                    
                }
            }
        });
    });

    function getMoreIdol(data){
        $.ajax({
        url: "{{url('ajax/idol/get')}}",
        type: 'GET',
        data: data,
        success: function (res) {
            $(".loading-img").hide();
            is_loading = true;
            if(res.status){
                createIdolList(res.data);
                count++;
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }
    

    function getNewTotalIdol(data){
        $.ajax({
        url: "{{url('ajax/total/new')}}",
        type: 'GET',
        data: data,
        success: function (res) {
            if(res.status){
                total = parseInt(res.total) / 12 ;
                if(res.national != -1 || res.from != -1 || res.to != -1){
                    $("#idol-change-title").html(`<span><i class="fa fa-filter" aria-hidden="true"></i> Lọc Idol (`+numberWithCommas(res.total)+`)</span>`);
                }else{
                    $("#idol-change-title").html(`<span><i class="fa fa-cube" aria-hidden="true"></i> Tất Cả Idol (`+numberWithCommas(res.total)+`)</span>`);
                }
                var data = {
                    page: count,
                    national: res.national,
                    from: res.from,
                    to: res.to
                }

                getMoreIdol(data);
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function getSearchIdol(data){
        $.ajax({
        url: "{{url('ajax/idol/search')}}",
        type: 'GET',
        data: data,
        success: function (res) {
            if(res.status){
                createIdolListSearch(res.data);
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function createIdolListSearch(data){
        var element = "";
        if(data.length == 0){
            element = `
                <div class="col-md-12" style="margin-top: 20px">
                    <div class="text-center">
                            <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Không tìm thấy idol!</strong>
                    </div>
                </div>
            `
        }else{
            for(let x = 0; x < data.length; x++){
                element+=`
                    <div class="col-md-3 card-item" style="margin-top: 20px">
                        <a href="`+base_url+'/idol/thong-tin/'+data[x].id+`" target="_blank">
                            <div class="card">
                                <div class="card-image">
                                    <img src="`+data[x].thumb+`" alt="`+data[x].name+`"
                                        style="width:100%">
                                </div>
                                <div class="card-container">
                                    <h4><b>`+data[x].name+`</b></h4>
                                    <b style="font-weight: normal;font-size: 12px;">`+data[x].national_name+` - `+data[x].birth+`</b><br>
                                    <div style="display: flex;">
                                        <div style="flex: 1">
                                            <i class="fa fa-picture-o" aria-hidden="true"></i><b style="font-weight: normal;font-size: 12px;"> `+numberWithCommas(data[x].number_images)+`</b>
                                        </div>
                                        <div style="flex: 1;text-align: center;">
                                            <i class="fa fa-search" aria-hidden="true"></i><b style="font-weight: normal;font-size: 12px;"> `+numberWithCommas(data[x].views)+`</b>
                                        </div>
                                        <div style="flex: 1;text-align: right;">
                                            <i class="fa fa-eye" aria-hidden="true"></i><b style="font-weight: normal;font-size: 12px;"> `+numberWithCommas(data[x].likes)+`</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                `
            }
        }

        $(".idol-search").html(element).ready(function(){
            if(width != 0){
                $('.card-image').css({'height':width+'px'});
            }
        });
    }

    function createIdolList(data){
        var element = "";
        for(let x = 0; x < data.length; x++){
            element+=`
                <div class="col-md-3 card-item" style="margin-top: 20px">
                    <a href="`+base_url+'/idol/thong-tin/'+data[x].id+`" target="_blank">
                        <div class="card">
                            <div class="card-image">
                                <img src="`+data[x].thumb+`" alt="`+data[x].name+`"
                                    style="width:100%">
                            </div>
                            <div class="card-container">
                                <h4><b>`+data[x].name+`</b></h4>
                                <b style="font-weight: normal;font-size: 12px;">`+data[x].national_name+` - `+data[x].birth+`</b><br>
                                <div style="display: flex;">
                                    <div style="flex: 1">
                                        <i class="fa fa-picture-o" aria-hidden="true"></i><b style="font-weight: normal;font-size: 12px;"> `+numberWithCommas(data[x].number_images)+`</b>
                                    </div>
                                    <div style="flex: 1;text-align: center;">
                                        <i class="fa fa-search" aria-hidden="true"></i><b style="font-weight: normal;font-size: 12px;"> `+numberWithCommas(data[x].views)+`</b>
                                    </div>
                                    <div style="flex: 1;text-align: right;">
                                        <i class="fa fa-eye" aria-hidden="true"></i><b style="font-weight: normal;font-size: 12px;"> `+numberWithCommas(data[x].likes)+`</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            `
        }

        if($(".idol-list").children().length > 0){
            $(".card-item:last-child").after(element).ready(function(){
                var card_result = $('.card').width();
                $('.card-image').css({'height':card_result+'px'});
                width = card_result;
            });
        }else{
            $(".idol-list").html(element).ready(function(){
                var card_result = $('.card').width();
                $('.card-image').css({'height':card_result+'px'});
                width = card_result;
            });
        }
    }

</script>
@endsection