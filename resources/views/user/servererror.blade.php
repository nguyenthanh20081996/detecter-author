@extends('user.layout.user')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top: 15%">
                <strong style="font-size: 45px;color:gray"><i class="fa fa-frown-o" aria-hidden="true"></i> Xảy ra lỗi trên máy chủ!</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top: 20px">
                <a href="{{url('/')}}" class="btn-search" style="padding: 12px;background-color:#dddddd;color:#13181d;font-size: 12px;">Trang Chủ</a>
            </div>
        </div>
    </div>
@endsection