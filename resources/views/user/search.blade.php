@extends('user.layout.user')
@section('content')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0&appId=1338159486335205&autoLogAppEvents=1"></script>
<div class="main-search" id="background-search">
    <div class="main-search-center" style="position: absolute">
        <h2>tìm kiếm idols</h2>
        @if(Auth::check())
        <div class="main-search-select">
            <div class="main-search-url">
                <input class="search-select" id="url-select" type="radio" name="searchSelect" value="url" checked>
                <label for="url-select">Tìm kiếm bằng URL</label>
            </div>
            <div class="main-search-upload">
                <input class="search-select" id="upload-select" type="radio" name="searchSelect" value="upload">
                <label for="upload-select">Tải ảnh từ máy bạn</label>
            </div>
        </div>
        <div class="search-numbers"></div>
        <p style="color:white">Lượt tìm kiếm sẽ được cập nhật lại trong ngày tiếp theo!</p>
        <div class="main-search-input">
            <input type="text" id="search-url" placeholder="Nhập Link Ảnh Idol..." autocomplete="off" autocorrect="off"
                autocapitalize="off" spellcheck="false"><br>
            <div class="img-idol">
                <img class="idol-img-to-search" id="idol-img-to-search" alt="idol-to-search" width="300" src="{{asset('img/review.png')}}"
                    onError="this.onError=null; this.src='{{asset('img/image-not-found.png')}}'">
            </div>
            <div class="btn-search-idol" style="margin-top: 20px">
                <button class="btn-search search-url-btn">Tìm Kiếm</button>
            </div>
        </div>
        <div class="main-search-uploadfile" style="display: none">
            <form action="" method="POST" role="form">
                <div class="upload-image">
                    <input type="file" id="idol-img-to-search-file" accept="image/x-png,image/gif,image/jpeg"
                        onchange="readURL(this);">
                </div>
                <div class="img-idol">
                    <img class="idol-img-to-search-file" id="idol-search-2" alt="idol-to-search" width="300" src="{{asset('img/review.png')}}"
                        onError="this.onError=null; this.src='{{asset('img/image-not-found.png')}}'">
                </div>
                <div class="btn-search-idol" style="margin-top: 20px">
                    <button type="button" class="btn-search search-file-btn">Tìm Kiếm</button>
                </div>
            </form>
        </div>
        @else
        <div class="main-user">
            <div class="login-user-title">
                <span>Bạn hãy đăng nhập để tìm kiếm Idol</span>
            </div>
            {{-- <div class="login-user-input">
                <button class="btn-search login-facebook" style="margin-top: 10px"><i class="fa fa-facebook-official" aria-hidden="true"></i>
                    Đăng Nhập Với Facebook</button>
            </div> --}}
            <div class="login-user-input">
                <button class="btn-search login-google"><i class="fa fa-google-plus-square" aria-hidden="true"></i> Đăng
                    Nhập Với Gmail</button>
            </div>
        </div>
        @endif
    </div>
</div>
<div class="container" style="margin-top: 600px">
    <div class="row" id="result-title" style="display: none">
        <div class="col-md-12" style="margin-top: 80px;">
            <strong>Kết quả tìm kiếm</strong>
            <hr>
        </div>
    </div>
    <div class="row idol-detected" style="margin-top: 10px;">
    </div>
    <div class="comment-vote" style="display: none">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top:20px;">
                <strong>Đánh Giá Độ Chính Xác</strong>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12 star-vote" style="margin-top: 10px;font-size: 25px;color:#ececec">
                <i class="fa fa-star star-item" aria-hidden="true" id="star-1" data-index="1"
                    style="margin-left: 15px"></i>
                <i class="fa fa-star star-item" aria-hidden="true" id="star-2" data-index="2"></i>
                <i class="fa fa-star star-item" aria-hidden="true" id="star-3" data-index="3"></i>
                <i class="fa fa-star star-item" aria-hidden="true" id="star-4" data-index="4"></i>
                <i class="fa fa-star star-item" aria-hidden="true" id="star-5" data-index="5"></i>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12 text-center">
                <textarea id="comment" cols="30" rows="10" placeholder="Thêm nhận xét..."></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
                <button class="btn-search send-voting">Gửi</button>
            </div>
        </div>
    </div>
    <div class="infor-home">
        <div class="row">
            <div class="col-md-12" style="margin-top: 80px">
                <strong style="font-size: 17px;">Tìm kiếm nhiều nhất</strong>
                <hr>
            </div>
        </div>
        <div class="row">
            @foreach ($top_idol as $item)
            <div class="col-md-3" style="margin-top: 12px">
                <a href="{!! route('IdolDetail', ['id' => $item->id]) !!}" target="_blank">
                    <div class="hot-list">
                        <div class="hot-card">
                            <div class="hot-card-header">
                                <img src="{{$item->thumb}}" alt="{{$item->name}}">
                            </div>
                            <div class="hot-card-footer">
                                <span>{{$item->name}}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <strong style="font-size: 17px;">Hướng Dẫn Sử Dụng</strong>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <strong>Bước 1: </strong><span>Nếu bạn chưa đăng nhập, hãy đăng nhập bằng Facebook hoặc
                Gmail.</span><br><br>
            <strong>Bước 2: </strong><span>Chọn cách đăng ảnh, hệ thống có thể lấy ảnh từ url hoặc lấy ảnh tải lên từ
                máy tính bạn</span><br><br>
            <strong>Bước 3: </strong><span>Sau khi tải ảnh idol lên thành công hãy bấm nút tìm kiếm, hệ thống sẽ trả kết
                quả cho bạn ngay lập tức.</span>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <strong style="font-size: 17px;">Giới Thiệu</strong>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <span><i class="fa fa-hand-o-right" aria-hidden="true"></i> Vẻ đẹp của các Idol luôn là điều khiến cho biết bao thế hệ thanh niên Việt mê đắm, dẫu vậy vì sự ngăn cách về địa lý, rào cản về ngôn ngữ đã khiến cho biết bao người dù đã diện kiến vẻ đẹp kiều diễm 
                của những cô nàng người mẫu, diễn viên, nhưng lại không thể tìm ra danh tính của họ.
            </span><br><br>
            <span><i class="fa fa-hand-o-right" aria-hidden="true"></i> Bản thân cũng là một người hâm mộ những Idol Nhật Bản nói riêng và trên toàn thế giới nói chung nên mình quyết định dành thời gian, công sức và tiền bạc để tạo ra website Nhận Diện Idol
            giúp mọi người có thể tìm kiếm được Idol mà mình hằng mong nhớ, cũng là nơi để chia sẻ Idol của bạn đến những người khác.</span><br><br>
            <span><i class="fa fa-hand-o-right" aria-hidden="true"></i> Hệ thống sử dụng công nghệ trí tuệ nhân tạo AI giúp độ chính xác đạt đến 60% - 80%.</span><br><br>
            <span><i class="fa fa-hand-o-right" aria-hidden="true"></i> Nếu hệ thống không trả lại kết quả hoặc kết quả không như bạn mong muốn, hãy vào phần <a href="{{url('chia-se-idols')}}">Chia Sẻ Idol</a> chúng tôi sẽ tìm kiếm và cập nhật vào danh sách ngay lập tức.</span>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <strong style="font-size: 17px;">Bình Luận</strong>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#detecter-idol-author" data-width="100%" data-numposts="10"></div>
        </div>
    </div>
    <div class="clear" style="padding: 20px"></div>
</div>
@endsection
@section('script')
<script src="{{asset('js/particles.min.js')}}"></script>
<script src="{{asset('js/stats.min.js')}}"></script>
<script>
    var base_url = "{{url('/')}}";
    var star_vote = 0;
    var search_numbers = 0;

  
    if (window.location.hash === "#_=_"){
        if (history.replaceState) {
            var cleanHref = window.location.href.split("#")[0];
            history.replaceState(null, null, cleanHref);

        } else {
            window.location.hash = "";
        }
    }

    @if(Auth::check())
        search_numbers = parseInt("{{Auth::user()->search_numbers}}");    
    @endif

    $(".menu-search").addClass('menu-active');
    $(document).ready(function(){
        $("#search-url").focus();

        $(".search-numbers").html('Bạn có '+search_numbers+' lần tìm kiếm!');

        $('.login-google').click(function(){
            window.location.replace("{{url('auth/google')}}");
        });

        $(".login-facebook").click(function(){
            window.location.replace("{{url('auth/facebook')}}");
        });

        $(".fa-star").mouseenter(function(){
            var index = parseInt($(this).attr("data-index"));
           
            for(let x = 1; x < index+1; x++){
                $('#star-'+x).css({'color':'#FFC107'});
            }
            
            for(let x = index + 1; x < 6; x++){
                $('#star-'+x).css({'color':'#ececec'});
            }
            star_vote = index;
        });

        $(".send-voting").click(function(){
            $(this).html(`<img src="{{asset('img/loading-ring.gif')}}" width="22"> Gửi`);
            $(this).prop('diabled', true);

            var star = star_vote;
            var note = $("#comment").val();

            var data = {
                star: star,
                note: note
            }

            sendingVote(data);
        });

        var width_card = $('.hot-card-header').width();
        $('.hot-card-header').css({'height':width_card+'px'});

        particlesJS("background-search", {
            "particles": {
                "number": {
                    "value":50, "density": {
                        "enable": true, "value_area": 800
                    }
                }
                , "color": {
                    "value": "#ffffff"
                }
                , "shape": {
                    "type":"star", "stroke": {
                        "width": 0, "color": "#000000"
                    }
                    , "polygon": {
                        "nb_sides": 5
                    }
                    , "image": {
                        "src": "img/github.svg", "width": 100, "height": 100
                    }
                }
                , "opacity": {
                    "value":0.5, "random":false, "anim": {
                        "enable": false, "speed": 1, "opacity_min": 0.1, "sync": false
                    }
                }
                , "size": {
                    "value":3, "random":true, "anim": {
                        "enable": false, "speed": 40, "size_min": 0.1, "sync": false
                    }
                }
                , "line_linked": {
                    "enable": true, "distance": 150, "color": "#ffffff", "opacity": 0.4, "width": 1
                }
                , "move": {
                    "enable":true, "speed":4, "direction":"none", "random":false, "straight":false, "out_mode":"out", "bounce":false, "attract": {
                        "enable": false, "rotateX": 600, "rotateY": 1200
                    }
                }
            }
            , "interactivity": {
                "detect_on":"canvas", "events": {
                    "onhover": {
                        "enable": false, "mode": "grab"
                    }
                    , "onclick": {
                        "enable": false, "mode": "push"
                    }
                    , "resize":true
                }
                , "modes": {
                    "grab": {
                        "distance":360, "line_linked": {
                            "opacity": 1
                        }
                    }
                    , "bubble": {
                        "distance": 400, "size": 40, "duration": 2, "opacity": 8, "speed": 3
                    }
                    , "repulse": {
                        "distance": 200, "duration": 0.4
                    }
                    , "push": {
                        "particles_nb": 4
                    }
                    , "remove": {
                        "particles_nb": 2
                    }
                }
            }
            , "retina_detect":true
        });


        $("#search-url").keyup(function(){
            var link = $(this).val();
            if(link){
                img_url = $(this).val();
                $(".idol-img-to-search").attr("src", img_url);
            }else{
                $(".idol-img-to-search").attr("src", "{{asset('img/review.png')}}");
            }
            
        });

        $(".search-url-btn").click(function(){
            var img_url = $("#search-url").val();

            if(img_url){
                $(this).html(`<img src="{{asset('img/loading-ring.gif')}}" width="22"> Tìm Kiếm`);
                $(this).prop('disabled', true);

                var data = {
                    img_url: img_url
                }

                sendPhotoToSearch(data);
            }else{
                alertify.error("Hãy nhập link ảnh");
            }            
        }); 

        $(".search-file-btn").click(function(){
            var file_data = $('#idol-img-to-search-file').prop('files')[0];
            var match = ["image/png", "image/jpg", "image/jpeg"];
            
            if(file_data){
                var type = file_data.type;
                if (type == match[0] || type == match[1] || type == match[2]) {
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $(this).html(`<img src="{{asset('img/loading-ring.gif')}}" width="22"> Tìm Kiếm`);
                    $(this).prop('disabled', true);
                    $.ajax({
                        url: "{{url('ajax/idol/search/file')}}",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (res) {
                            console.log(res);
                            if(res.status){
                                alertify.success("Đã có kết quả");
                                $(".search-file-btn").html(`Tìm Kiếm`);
                                $(".search-file-btn").prop('disabled', false);

                                $("#result-title").show(300);
                                createIdolDetected(res.idol_detected, res.confidences);
                            }else{
                                $(".search-file-btn").html(`Tìm Kiếm`);
                                $(".search-file-btn").prop('disabled', false);
                                alertify.error(res.message);
                            }
                        },
                        error: function(err){
                            console.log(err);
                        }
                    });
                } else {
                    alertify.error("Chỉ có thể tải file ảnh");
                }
            }else{
                alertify.error("Hãy chọn file");
            }
            
        });
    });

    function readURL(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#idol-search-2').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function sendPhotoToSearch(data){
        $.ajax({
        url: "{{url('search')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            $(".search-url-btn").html('Tìm Kiếm');
            $(".search-url-btn").prop("disabled", false);

            if(res.status){
                alertify.success("Đã có kết quả");
                $("#result-title").show(300);
                createIdolDetected(res.idol_detected, res.confidences);
            }else{
                alertify.error(res.message);
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function sendingVote(data){
        $.ajax({
        url: "{{url('vote')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            $(".send-voting").html('Gửi');
            $(".send-voting").prop('disabled', false);
            if(res.status){
                $(".comment-vote").hide(300);
                alertify.success(res.message);
            }else{
                alertify.error(res.message);
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function createIdolDetected(data, confidences){
        if(search_numbers > 0){
            search_numbers--;
            $(".search-numbers").html('Bạn có '+search_numbers+' lần tìm kiếm!');
        }

        var element = "";
        if(data.length == 0){
            $(".idol-detected").html(`
                    <div class="col-md-12 text-center">
                        <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Không tìm được Idol Này!</strong>  
                    </div>
            `);
        }else{
            for(let x = 0; x < data.length; x++){
                element+=`
                <div class="col-md-3">
                    <a href="`+base_url+'/idol/thong-tin/'+data[x].id+`" target="_blank">
                        <div class="card">
                            <div class="card-image">
                                <img src="`+data[x].thumb+`" alt="`+data[x].name+`"
                                    style="width:100%">
                            </div>
                            <div class="card-container">
                                <h4><b>`+data[x].name+`</b></h4>
                                <div id="myProgress">
                                    <div id="myBar" style="width: `+confidences[x]+`%">`+confidences[x]+`%</div>
                                </div>
                                <p style="font-size: 12px">Trùng Khớp</p>
                            </div>
                        </div>
                    </a>
                </div>
                `
            }

            $(".idol-detected").html(element).ready(function(){
                var card_result = $('.card').width();
                $('.card-image').css({'height':card_result+'px'});
                $("#comment").val("");
                $(".comment-vote").show(300);
            });
        }
    }

    
</script>
@endsection