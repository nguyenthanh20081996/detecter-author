<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Tìm kiếm idol</title>
		<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" type="image/png" href="{{asset('img/idol.ico')}}"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/default.min.css"/>
	  <!-- <link rel="stylesheet" type="text/css" href="{{asset('plugins/light-box/css/ekko-lightbox.css')}}"> -->
	  <link rel="stylesheet" type="text/css" href="{{asset('plugins/lightbox2/dist/css/lightbox.css')}}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-148364310-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-148364310-1');
    </script>
	</head>
	<body>
		<div class="main-menu">
      <div id="change-name" class="modal">
        <div class="modal-content">
          <div class="modal-header" style="padding: 0px;">
            <p>Đổi tên tài khoản</p>
          </div>
          <input type="text" id="name-value-input" placeholder="Hãy nhập tên mới..."><br>
          <div class="text-center">
            <button id="ok-change-name" class="btn btn-success">OK</button>
            <button id="cancel-change-name" class="btn btn-light">Hủy</button>
          </div>
        </div>
      </div>
      <div id="notify-modal" class="modal">
        <div class="modal-content">
          <div class="modal-header" style="padding: 0px;">
            <p>Thông Báo</p>
          </div>
          <div class="notify-wrap">
              @if(session()->has('notify'))
                  @for($index = 0; $index < count(session()->get('notify')); $index++)
                  <a href="{!! route('NotificationImage', ['id' => session()->get('notify')[$index]->id]) !!}">
                    <div class="notify-item">
                      <span><i class="fa fa-eercast" aria-hidden="true"></i> <strong>{{session()->get('notify')[$index]->image_numbers}} hình ảnh </strong>cho idol <strong>{{session()->get('notify')[$index]->name}}</strong> của bạn đã được thêm vào bộ sưu tập!</span><br>
                      <i class="fa fa-heart" aria-hidden="true" style="color: #ef4f3f;"></i><span> Xin cảm ơn!</span>
                    </div>
                  </a>
                  @endfor
              @endif
              @if(session()->has('idol_notify'))
                  @for($index = 0; $index < count(session()->get('idol_notify')); $index++)
                    <a href="{!! route('NotificationIdol', ['id' => session()->get('idol_notify')[$index]->id]) !!}">
                      <div class="notify-item">
                        <span><i class="fa fa-eercast" aria-hidden="true"></i> Idol <strong>{{session()->get('idol_notify')[$index]->name}}</strong> đã được thêm vào hệ thống!</span><br>
                        <i class="fa fa-heart" aria-hidden="true" style="color: #ef4f3f;"></i><span> Xin cảm ơn!</span>
                      </div>
                    </a>
                  @endfor
              @endif
          </div>
          <div class="text-center">
            <button id="cancel-notify-name" class="btn btn-light">Đóng</button>
          </div>
        </div>
      </div>
      <nav class="nav-bar">
          <div class="logo-site"> 
              <strong><img src="{{asset('img/maps-and-flags.png')}}" alt="LOGO" width="30"> TÌM KIẾM IDOL</strong>
          </div>
          <div class="menu-right">
            <ul class="menu-list">
              <li><a href="{{url('/')}}" class="menu-search"><i class="fa fa-search" aria-hidden="true"></i> Tìm Kiếm</a></li>
              {{-- <li><a href="{{url('chia-se-idols')}}" class="menu-share-idol"><i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm Lượt Tìm Kiếm</a></li> --}}
              <li><a href="{{url('danh-sach-idols')}}" class="menu-list-idol"><i class="fa fa-list" aria-hidden="true"></i> Danh Sách Idols</a></li>
              <li><a href="{{url('top-nguoi-dung')}}" class="menu-top-user"><i class="fa fa-trophy" aria-hidden="true"></i> Top Người Dùng</a></li>
              @if(Auth::check())
                <li><a href="{{url('chia-se-idols')}}" class="menu-share-idol"><i class="fa fa-share-square" aria-hidden="true"></i> Chia Sẻ Idols</a></li>
                <li>
                  <div class="user-list-item">
                    <span class="user-main-name">
                      @if(session()->has('total_notify'))
                        @if(session()->get('total_notify') > 0)
                          <img src="{{asset('img/bell.gif')}}" width="18">
                        @else
                          <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                        @endif
                      @else
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                      @endif
                     {{Auth::user()->name}}</span>
                    <div class="sub-menu-user" style="display: none">
                      <div style="padding: 5px 0px">
                        <a class="open-notify-modal" href="javascript:void(0)"><i class="fa fa-bell" aria-hidden="true"></i> @if(session()->has('total_notify')) {{session()->get('total_notify')}} @else 0 @endif Thông Báo</a>
                      </div>
                      <div style="padding: 5px 0px">
                        <a class="change-name-btn" href="javascript:void(0)"><i class="fa fa-refresh" aria-hidden="true"></i> Đổi Tên</a>
                      </div>
                      <div style="padding: 5px 0px">
                        <a href="{{url('dang-xuat')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> Đăng Xuất</a>
                      </div>
                    </div>
                  </div>
                </li>
              @endif
            </ul>
            <div class="mobile-menu">
              <span><i class="fa fa-bars" aria-hidden="true"></i></span>
            </div>
          </div>
      </nav>
      <div class="mobile-menu-list" style="display:none">
        @if(Auth::check())
        <div class="mobile-menu-item user-information">
            <span class="user-main-name"><i class="fa fa-user-circle-o" aria-hidden="true"></i> {{Auth::user()->name}}</span>
        </div>
        @endif
        <div class="mobile-menu-item">
            <a href="{{url('/')}}" class="menu-search"><i class="fa fa-search" aria-hidden="true"></i> Tìm Kiếm</a>
        </div>
        <div class="mobile-menu-item">
            <a href="{{url('danh-sach-idols')}}" class="menu-list-idol"><i class="fa fa-list" aria-hidden="true"></i> Danh Sách Idols</a>
        </div>
        <div class="mobile-menu-item">
            <a href="{{url('top-nguoi-dung')}}" class="menu-top-user"><i class="fa fa-trophy" aria-hidden="true"></i> Top Người Dùng</a>
        </div>
        @if(Auth::check())
        <div class="mobile-menu-item">
            <a href="{{url('chia-se-idols')}}" class="menu-share-idol"><i class="fa fa-share-square" aria-hidden="true"></i> Chia Sẻ Idols</a>
        </div>
        <div class="mobile-menu-item">
        <a class="open-notify-modal" href="javascript:void(0)"><i class="fa fa-bell" aria-hidden="true"></i> 10 Thông Báo</a>
        </div>
        <div class="mobile-menu-item">
            <a class="change-name-btn" href="javascript:void(0)"><i class="fa fa-refresh" aria-hidden="true"></i> Đổi Tên</a>
        </div>
        <div class="mobile-menu-item">
            <a href="{{url('dang-xuat')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> Đăng Xuất</a>
        </div>
        @endif
      </div>
    </div>
    @yield('content')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/alertify.min.js"></script>
    <!-- <script src="{{asset('plugins/light-box/js/ekko-lightbox.js')}}"></script> -->
    <script src="{{asset('plugins/lightbox2/dist/js/lightbox.js')}}"></script>
    <script>
      var sub_menu_display = false;

      $(document).ready(function(){
        $("#search-url").focus();

        $("#cancel-change-name").click(function(){
          $('#change-name').hide(200);
        });

        $("#cancel-notify-name").click(function(){
          $('#notify-modal').hide(200);
        });

        $(".open-notify-modal").click(function(){
          $(".mobile-menu-list").removeClass("mobile-active");
          $('#notify-modal').show(200);
        });

        $(".change-name-btn").click(function(){
          $("#name-value-input").val('');
          $(".mobile-menu-list").removeClass("mobile-active");
          $('#change-name').show(200);
          $('#name-value-input').focus();
        });

        $(".user-list-item").click(function(){
          if(sub_menu_display){
            $('.sub-menu-user').hide();
          }else{
            $('.sub-menu-user').show();
          }
          sub_menu_display = !sub_menu_display;
        });

        $(".search-select").click(function(){
          let search_select = $(this).val();
          if(search_select == 'url'){
            $(".main-search-input").show();
            $(".main-search-uploadfile").hide();
            $("#search-url").focus();
          }else if(search_select == 'upload'){
            $(".main-search-input").hide();
            $(".main-search-uploadfile").show();
          }
        });

        $("#ok-change-name").click(function(){
            var new_name = $("#name-value-input").val();
            if(!new_name){
              alertify.error("Hãy nhập tên mới!");
            }else{
              if(new_name.length > 50){
                alertify.error("Tên tối đa là 50 ký tự!");
              }else{
                var data = {
                  name: new_name
                }

                changeNameForUser(data);
              }
            }
        });

        $(".mobile-menu").click(function(){
          $(".mobile-menu-list").toggleClass("mobile-active");
        });
      });

      function changeNameForUser(data){
        $.ajax({
        url: "{{url('change/name')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            if(res.status){
              alertify.success(res.message);
              $('#change-name').hide(200);
              $('.user-main-name').html('<i class="fa fa-user-circle-o" aria-hidden="true"></i> ' + res.new_name);
            }else{
              alertify.error(res.message);
            }
        }, 
        error: function(error){ console.log(error); } });
      }
    </script>
    @yield('script')
	</body>
</html>
