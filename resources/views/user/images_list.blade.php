@extends('user.layout.user')
@section('content')
<div class="idol-title" id="idol-change-title" style="padding: 10px 0px;border-bottom: 1px solid #cdcdcd;font-size:20px;text-align: center">
    <i class="fa fa-cube" aria-hidden="true"></i><span>Upload By: </span><span>{{$name}}</span>
</div>
<div class="image-wrap">
</div>
<div class="loading-more"></div>
<div class="text-center" style="margin-top: 20px">
    <img class="loading-img" src="{{asset('img/loading-idol.gif')}}" width="50" style="display: none">
</div>
@endsection
@section('script')
<script>
    var user_id = {{$id}};
    var total = Math.ceil(parseInt('{{$total}}')/20);
    var count = 0;

    var width_body = $('body').width();
    var width_image = 0;
    var base_url = "{{url('/')}}";

    if(width_body > 1280){
        width_image = (width_body / 6) - 4;
    }else if(width_body > 500 && width_body < 1280){
        width_image = (width_body / 4) - 4;
    }else{
        width_image = (width_body / 2) - 4;
    }

    $(document).ready(function(){
        var data = {
            page: count,
            user_id: user_id
        }
        $(".loading-img").show();
        getMoreImages(data);

        $(window).scroll(function(){
            var position = $('.loading-more').position();
            if ($(window).scrollTop() + $(window).height() >= position.top){
                if (count >= total){
                    return false;
                }else{
                    if(is_loading){
                        is_loading = false;
                        var data = {
                            page:count,
                            user_id: user_id
                        }
                        $(".loading-img").show();
                        getMoreImages(data);
                    }
                    
                }
            }
        });
    });

    function getMoreImages(data){
        $.ajax({
        url: "{{url('ajax/top-user/images')}}",
        type: 'GET',
        data: data,
        success: function (res) {
            $(".loading-img").hide();
            is_loading = true;
            if(res.status){
               $(".loading-img").hide();
               createImageItem(res.data);
               count++;
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function createImageItem(data){
        var element = '';
        for(var index = 0; index < data.length; index++){
            element+=`
            <div class="item-image-auto" style="width: `+width_image+`px; height: `+width_image+`px">
                <a href="`+base_url+'/idol/thong-tin/'+data[index].id+`">
                    <img src="`+data[index].link+`">
                </a>
                <div class="item-image-name" style="display: none"><span>`+data[index].name+`</span></div>
            </div>
            `
        }

        $('.image-wrap').append(element);
    }
</script>
@endsection