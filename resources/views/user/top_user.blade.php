@extends('user.layout.user')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px">
            <div class="idol-title" id="idol-change-title" style="padding: 3px 0px;border-bottom: 1px solid #cdcdcd;font-size:20px;">
                <span><i class="fa fa-trophy" aria-hidden="true"></i> Top Chia Sẻ Idol</span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        @foreach($user_list_top_share as $top_share)
        <div class="col-md-3 card-item" style="margin-top: 10px">
            <a href="{!! route('TopUserIdol', ['id' => $top_share->id]) !!}">
                <div class="card">
                    <div class="card-container">
                        <h4><b>{{$top_share->name}}</b></h4>
                        <b style="font-weight: normal;font-size: 12px;color: #ecbb08;"><i class="fa fa-star" aria-hidden="true"></i> {{$top_share->idol_numbers}} Idol</b>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px">
            <div class="idol-title" id="idol-change-title" style="padding: 3px 0px;border-bottom: 1px solid #cdcdcd;font-size:20px;">
                <span><i class="fa fa-trophy" aria-hidden="true"></i> Top Upload Ảnh</span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        @foreach($user_list_top_images as $top_images)
        <div class="col-md-3 card-item" style="margin-top: 10px">
            <a href="{!! route('TopUserHinhAnh', ['id' => $top_images->id]) !!}">
                <div class="card">
                    <div class="card-container">
                        <h4><b>{{$top_images->name}}</b></h4>
                        <b style="font-weight: normal;font-size: 12px;color: #3c00f5;"><i class="fa fa-picture-o" aria-hidden="true"></i> {{$top_images->image_numbers}} Hình Ảnh</b>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    $(".menu-top-user").addClass('menu-active');
});
</script>
@endsection