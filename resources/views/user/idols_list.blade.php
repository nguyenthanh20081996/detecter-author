@extends('user.layout.user')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top: 20px">
                <div class="idol-title" id="idol-change-title" style="padding: 3px 0px;border-bottom: 1px solid #cdcdcd;font-size:20px;">
                    <span><i class="fa fa-cube" aria-hidden="true"></i> Idol được chia sẻ bởi: {{$name}}</span>
                </div>
            </div>
        </div>
        @foreach($idols as $idol)
            <a href="{!! route('IdolDetail', ['id' => $idol->id]) !!}">
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-12">
                        <div class="idol-list-row">
                            <div class="image-auto-load-size">
                                <img src="{{$idol->thumb}}">
                            </div>
                            <div class="image-idol-content">
                                <span>{{$idol->name}}</span><br>
                                <p><i class="fa fa-globe" aria-hidden="true"></i> {{$idol->national_name}}</p>
                                <p><i class="fa fa-birthday-cake" aria-hidden="true"></i> {{$idol->birth}}</p>
                                <p><i class="fa fa-picture-o" aria-hidden="true"></i> {{number_format($idol->number_images)}} Hình Ảnh</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
        <div class="row">
            <div class="col-md-12  text-center">
                {{ $idols->links() }}
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
     var width_body = $('body').width();

     $(document).ready(function(){
        var width = width_body / 8;
        $('.image-auto-load-size').css('width', width + 'px');
        $('.image-auto-load-size').css('height', width + 'px');
     });
</script>
@endsection
