@extends('user.layout.user')
@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        <div class="col-md-12">
            <div class="share-idols">
                <div class="share-notification">
                    <span><i class="fa fa-hand-o-right" aria-hidden="true"></i> Trước khi bạn chia sẻ Idol của mình hãy kiểm tra Idol đã có trong danh sách hay chưa tránh trường hợp bị trùng nhé.</span>
                </div>
                <div class="description" style="margin-top: 20px">
                    <strong>Tên Idol <span style="color: red">*</span></strong>
                </div>
                <div class="name-idol" style="margin-top: 10px">
                    <input type="text" id="idol-name" style="width: 100%" placeholder="Hãy nhập tên idol của bạn...">
                </div>
                <div class="description">
                    <strong>Thêm Miêu Tả Về Idol</strong>
                </div>
                <div class="description-txt" style="margin-top: 10px"> 
                    <textarea id="description" name="description"></textarea>
                </div>
                <div class="description" style="margin-top: 20px">
                    <strong>Ảnh Đại Diện <span style="color: red">*</span></strong>
                </div>
                <div class="main-search-select" style="margin-top: 10px">
                    <div class="main-search-url" style="color: black">
                        <input class="search-select" id="url-select" type="radio" name="uploadIdolType" value="url"
                            checked>
                        <label for="url-select">Ảnh từ URL</label>
                    </div>
                    <div class="main-search-upload" style="color: black">
                        <input class="search-select" id="upload-select" type="radio" name="uploadIdolType" value="upload">
                        <label for="upload-select">Tải ảnh từ máy bạn</label>
                    </div>
                </div>
                <div class="main-search-input">
                    <input type="text" id="upload-by-url" style="width: 100%" placeholder="Nhập Link Ảnh Idol..."
                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                    <div class="img-idol">
                        <img class="idol-img-to-search" id="idol-img-to-url" alt="idol-to-upload" width="300" src="{{asset('img/review.png')}}"
                        onError="this.onError=null; this.src='{{asset('img/image-not-found.png')}}'">
                    </div>   
                    <div class="text-center" style="margin-top: 20px">
                        <button class="btn-search btn-share-url">Chia sẻ</button>
                    </div>
                </div>
                <div class="main-search-uploadfile" style="display: none;">
                    <div class="upload-image" style="width: 100%">
                        <input type="file" id="idol-img-to-upload-file" accept="image/png,image/jpeg" onchange="readURL(this);">
                    </div>
                    <div class="img-idol">
                        <img class="idol-img-to-search" id="idol-img-to-file" alt="idol-to-upload" width="300" src="{{asset('img/review.png')}}"
                        onError="this.onError=null; this.src='{{asset('img/image-not-found.png')}}'">
                    </div>
                    <div class="text-center" style="margin-top: 20px">
                        <button class="btn-search btn-share-file">Chia sẻ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('script')
<script src='{{ asset ('js/tinymce.min.js') }}'></script>
<script src='{{ asset ('js/init-tinymce.js') }}'></script>
<script>
    $(document).ready(function(){
        tinymce.init({
            selector: '#description',
            mode: "textareas",
            height: 300,
        });
        $(".menu-share-idol").addClass("menu-active");

        $(".btn-share-url").click(function(){
            collectionDataForUrl();
        });

        $(".btn-share-file").click(function(){
            collectionDataForFile();
        });

        $("#upload-by-url").keyup(function(){
            var link = $(this).val();
            if(link){
                img_url = $(this).val();
                $(".idol-img-to-search").attr("src", img_url);
            }else{
                $(".idol-img-to-search").attr("src", `{{asset('img/review.png')}}`)
            }
        });
    });

    function collectionDataForUrl(){
        var idol_name = $("#idol-name").val();
        var description = tinyMCE.get('description').getContent();
        var file_url = $("#upload-by-url").val();

        if(!String(idol_name).trim()){
            alertify.error("Hãy nhập tên Idol");
            return false;
        }

        var data = {
            idol_name: idol_name,
            description: description,
            file_url: file_url
        }

        uploadIdolByUrl(data);
    }

    function collectionDataForFile(){
        var idol_name = $("#idol-name").val();
        var description = tinyMCE.get('description').getContent();
        var file_data = $('#idol-img-to-upload-file').prop('files')[0];

        var match = ["image/png", "image/jpg", "image/jpeg"];

        if(!String(idol_name).trim()){
            alertify.error("Hãy nhập tên Idol");
            return false;
        }

        if(file_data){
            var type = file_data.type;
            if (type == match[0] || type == match[1] || type == match[2]) {
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('idol_name', idol_name);
                form_data.append('description', description);

                uploadIdolByFile(form_data);
            } else {
                alertify.error("Chỉ có thể tải file ảnh");
            }
        }else{
            alertify.error("Hãy chọn file");
        }
    }

    function readURL(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#idol-img-to-file').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function uploadIdolByFile(data){
        $.ajax({
        url: "{{url('idol/share/file')}}",
        type: 'POST',
        contentType: false,
        processData: false,
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            if(res.status){
                alertify.success(res.message);
            }else{
                alertify.error(res.message);
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function uploadIdolByUrl(data){
        $.ajax({
        url: "{{url('idol/share/url')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            if(res.status){
                alertify.success(res.message);
            }else{
                alertify.error(res.message);
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }
</script>
@endsection