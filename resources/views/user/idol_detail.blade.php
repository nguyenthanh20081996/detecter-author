@extends('user.layout.user')
@section('content')
<div class="container">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0&appId=1338159486335205&autoLogAppEvents=1"></script>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-12">
            <strong>Thông Tin Idol</strong>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="{{$idol->thumb}}">
            </div>
        </div>
        <div class="col-md-8">
            <div class="information-idol">
                <div class="information-name" style="padding-bottom: 10px">
                    <strong>{{$idol->name}}</strong>
                </div>
                <span>{!! $idol->des !!}</span>
            </div>  
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-12">
            <strong>Bộ Sưu Tập</strong>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            @if(Auth::check())
            <label class="file-upload-button">
                <input type="file" id="file-input-handle" multiple accept="image/x-png,image/jpeg"/>
                <div class="add-file-icon"><i class="fa fa-plus" aria-hidden="true"></i> Thêm Ảnh</div>
            </label>
            @endif
            <!-- <label class="file-upload-button">
                <div class="add-file-icon"><i class="fa fa-download" aria-hidden="true"></i> Tải Bộ Ảnh</div>
            </label> -->
            <br>
            @if(Auth::check())
            <button class="menu-right custom-upload" style="display: none"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload</button>
            <div class="file-counter" style="display: none">
            </div>
            @endif
        </div>
    </div>
    <hr>
    @for($index = 0; $index < count($user_mapping); $index++)
    <div class="row" style="margin-top: 20px">
        <div class="col-md-12"> 
            <div class="author-image">
            <div class="author-name"><span style="font-size: 10px;color: #00adff;">Đăng bởi </span><span>{{$user_mapping[$index]->name}}</span></div>
            @foreach ($images[$index] as $item)
                <a class="img-idol-item" href="{{$item->link}}" data-lightbox="idol-set"> 
                    <div class="loading-image">
                        <img src="{{$item->link}}">
                    </div>
                </a>
            @endforeach
            </div>
        </div>
    </div>
    @endfor
    <div style="padding: 20px"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#detecter-idol-author-{{$idol->id}}" data-width="100%" data-numposts="10"></div>
        </div>
    </div>
    <div style="padding: 30px"></div>
</div>
@endsection
@section('script')
<script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});    
</script>
@if(Auth::check())
<script>
    var idol_id = {{$idol->id}};
    $("#file-input-handle").on('change', function(event){
        if(this.files.length){
            var file_numbers = this.files.length;
            $(".file-counter").html(`<i class="fa fa-picture-o" aria-hidden="true"></i> Đã thêm `+file_numbers+` ảnh`);
            $(".file-counter").show();
            $(".custom-upload").show();
        }else{
            $(".file-counter").hide();
            $(".custom-upload").hide();
        }
    });
    $(".custom-upload").click(function(){
        var file_data = $('#file-input-handle').prop('files');
        console.log(file_data);
        var form_data = new FormData();
        
        for(var index = 0; index < file_data.length; index++){
            form_data.append('files['+index+']', file_data[index]);
        }

        form_data.append('idol_id', idol_id);

        uploadImageByUser(form_data);
    });

    function uploadImageByUser(data){
        $(".custom-upload").html(`<img src="{{url('img/loading-ring.gif')}}" style="width: 19px"> Upload`);
        $.ajax({
        url: "{{url('ajax/image-idol/by-user')}}",
        type: 'POST',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
            alertify.success(res.message);
           }else{
            alertify.error(res.message);
           }

            $(".custom-upload").html(`<i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload`);
            $(".file-counter").hide();
            $(".custom-upload").hide();
        }, 
        error: function(error){ console.log(error); } }); 
    }
</script>
@endif
@endsection