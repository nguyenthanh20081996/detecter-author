<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Idol - Cong tac vien</title>
	<link rel="stylesheet" href="{{asset('css/app.css')}}" />
	<link href="{{asset('collaborators/css/bootstrap.min.css')}}" rel="stylesheet" />
	<link href="{{asset('collaborators/css/datepicker3.css')}}" rel="stylesheet" />
	<link href="{{asset('collaborators/css/bootstrap-table.css')}}" rel="stylesheet" />
	<link href="{{asset('collaborators/css/styles.css')}}" rel="stylesheet" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/alertify.min.css" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/default.min.css" />
	<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
	<link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
		rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<script src="{{asset('collaborators/js/jquery-1.11.1.min.js')}}"></script>
	<script src="{{asset('collaborators/js/bootstrap.min.js')}}"></script>
	<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
	<!--Icons-->
	<script src="{{asset('collaborators/js/lumino.glyphs.js')}}"></script>
	<script src="{{ asset('collaborators/js/ckeditor/ckeditor.js') }}"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		.ajax-load {
			background: #e1e1e1;
			padding: 10px 0px;
			width: 100%;
		}
	</style>
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>idolcuatui.com</span></a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user">
								<use xlink:href="#stroked-male-user"></use>
							</svg> {{Auth::user()->name}} </a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{url('collaborators/profile')}}"><svg class="glyph stroked male-user">
										<use xlink:href="#stroked-male-user"></use>
									</svg> Hồ sơ</a></li>
							<li><a href="{{ url('collaborators/logout') }}"><svg class="glyph stroked cancel">
										<use xlink:href="#stroked-cancel"></use>
									</svg> Đăng xuất</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group txt-center">
				<img src="https://img.icons8.com/officel/80/000000/conference-call.png">
			</div>
		</form>
		<ul class="nav menu">
			<li id="dashboard"><a href="{{ url('collaborators/dashboard') }}"><svg class="glyph stroked dashboard-dial">
						<use xlink:href="#stroked-dashboard-dial"></use>
					</svg> Thống Kê </a></li>
			<li id="upload-image"><a href="{{ url('collaborators/upload-image') }}"><svg class="glyph stroked arrow up">
						<use xlink:href="#stroked-arrow-up" /></svg>
					Tải Lên</a></li>
		</ul>
	</div>
	@yield('content')
	<script>
		var menuActive = window.location.href;
        if(menuActive.includes('collaborators/dashboard')){
            $('#dashboard').addClass('active');
        }else if(menuActive.includes('collaborators/upload-image')){
            $('#upload-image').addClass('active');
        }

		$(document).ready(function() {
		
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");

			$(window).on('resize', function () {
			if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
			});
			$(window).on('resize', function () {
			if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
			});

		});
	</script>
	<script src="{{asset('js/dropzone.js')}}"></script>
	<script src="{{asset('collaborators/js/progress-bar.js')}}"></script>
	<script src="{{asset('collaborators/js/select-custom.js')}}"></script>
	<script src="{{asset('collaborators/js/input_form_custom.js')}}"></script>
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/alertify.min.js"></script>
	@yield('script')
</body>

</html>