@extends('collaborators.layout')
@section('content')
<!-- Multi step form -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	@if(isset($id))
	<input type="hidden" id="idol-id" value="{{$id}}">
	@else
	<input type="hidden" id="idol-id" value="0">
	@endif
	<section class="multi_step_form">
		<div id="msform">
			<!-- Tittle -->
			<div class="tittle">
				<h2>Cập Nhật Idol</h2>
				<p>Để Idol của bạn được phê duyệt, bạn cần hoàn thành các bước cập nhật thông tin cơ bản.</p>
			</div>
			<!-- progressbar -->
			<ul id="progressbar">
				<li class="active">Thông tin cơ bản</li>
				<li @if(isset($id)) class="active" @endif>Ảnh đại diện</li>
				<li @if(isset($id)) class="active" @endif>Tệp ảnh nhận diện</li>
			</ul>
			<!-- fieldsets -->
			@if(!isset($id))
			<fieldset>
				<div class="input-group">
					<div class="form-group col-md-12">
						<div class="wrap" style="height:100px">
							<div>
								<label for="idol-name">Tên Idol</label>
								<input id="idol-name" type="text" name="idol-name" class="form-control"
									value="{{!empty($idol_name) ? $idol_name : null}}">
							</div>
							<div>
								<label for="idol-birth">Năm Sinh</label>
								<input id="idol-birth" type="text" name="idol-birth" class="form-control"
									value="{{!empty($idol_birth) ? $idol_birth : null}}">
							</div>
						</div>
					</div>
					<div class="form-group col-md-12">
						<div class="drop">
							@if(isset($idol_national) && !empty($idol_national))
							<div class="option placeholder">
								Chọn Quốc Gia
							</div>
							@foreach ($nationals as $item)
							@if($idol_national == $item->id)
							<div class="option active" data-value="{{ $item->id }}">
								{{ $item->name }}
							</div>
							@else
							<div class="option" data-value="{{ $item->id }}">
								{{ $item->name }}
							</div>
							@endif
							@endforeach
							@else
							<div class="option active placeholder">
								Chọn Quốc Gia
							</div>
							@foreach ($nationals as $item)
							<div class="option" data-value="{{ $item->id }}">
								{{ $item->name }}
							</div>
							@endforeach
							@endif
						</div>
					</div>
				</div>
				<button type="button" class="next action-button">Tiếp Tục</button>
			</fieldset>
			<fieldset>
				<div class="input-group">
					<div class="form-group col-md-12">
						@if(!empty($idol_image))
						<input type="radio" id="file-upload" name="upload_method" checked value="1">
						<label for="file-upload">Tải lên bằng file</label>
						<input type="radio" id="url-upload" name="upload_method" value="2">
						<label for="url-upload">Tải ảnh bằng url</label>
						<div id="mod-upload" class="upload-btn-wrapper">
							<button class="upload-btn">Cập nhật ảnh</button>
							<input type="file" id="input-by-file">
						</div>
						@else
						<input type="radio" id="file-upload" name="upload_method" checked value="1">
						<label for="file-upload">Tải lên bằng file</label>
						<input type="radio" id="url-upload" name="upload_method" value="2">
						<label for="url-upload">Tải ảnh bằng url</label>
						<div id="mod-upload" class="upload-btn-wrapper">
							<button class="upload-btn">Upload ảnh</button>
							<input type="file" id="input-by-file">
						</div>
						@endif
					</div>
					<div class="form-group col-md-12">
						<textarea id="idol_detail" name="idol_detail" class="form-control" rows="3">
							{{ !empty($idol_description) ? strval($idol_description) : null }}</textarea>
						<script>
							CKEDITOR.replace('idol_detail', {
								enterMode: CKEDITOR.ENTER_P,
								shiftEnterMode: CKEDITOR.ENTER_BR
							});
						</script>
					</div>
				</div>
				<button type="button" class="action-button previous previous_button">Quay Lại</button>
				<button type="button" id="first-check-button" class="action-button">Tiếp Tục</button>
				<input type="hidden" id="next-trigger" class="next" />
			</fieldset>
			@endif
			<fieldset>
				<h3>Tệp Ảnh Nhận Diện</h3>
				<h6>Tổng Số Ảnh: #<span id="counter">0</span></h6>
				<div class="form-group">
					<form action="{{url('collaborators/upload-idols/upload-images')}}" class="dropzone" id="my-dropzone"
						method="POST" enctype="multipart/form-data">
						@csrf
						<input type="hidden" id="idol-upload" @if(isset($id)) value="{{$id}}" @endif name="idol_id" />
					</form>
				</div>
				<button type="button" id="submit-button" class="action-button">Hoàn Thành</button>
			</fieldset>
		</div>
	</section>
</div>
<!-- End Multi step form -->
@endsection
@section('script')
<script>
	var counter = 0;
	Dropzone.options.myDropzone = {
        uploadMultiple: true,
        parallelUploads: 2,
        maxFilesize: 16,
        addRemoveLinks: true,
        dictRemoveFile: 'Xóa Tệp',
		dictDefaultMessage: 'Thả file hoặc nhấn vào đây để upload ảnh',
        dictFileTooBig: 'Kích thước ảnh lớn hơn 16MB',
		timeout: 10000,
		
		init: function () {
			this.on("removedfile", function (file) {
				console.log(file.name);
				$.ajax({
					url: "{{url('collaborators/upload-idols/remove-images')}}",
					type: 'POST',
					data: {name: file.name},
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					dataType: 'json',
					success: function (data) {
						counter--;
						$("#counter").text(counter);
					},
					error: function(err) {
						console.log(err);
					}
				}); 
            });
        },
        success: function (file, response) {
			counter = this.files.length;
            $("#counter").text(counter);
        }
    };

	$("body").on("click", "#first-check-button", function() {
		if($("input[name='upload_method']:checked").val() === '1') {
			collectionDataForFile();
		}
		else{
			collectionDataForUrl();		
		}
	});

	function collectionDataForUrl(){
        var idol_name = $("#idol-name").val();
		var idol_national = $(".option.active").attr("data-value");
		var idol_birth = $("#idol-birth").val();
		var ido_detail = CKEDITOR.instances.idol_detail.getData();
		var idol_id = $("#idol-id").val();
		if(!String(CKEDITOR.instances.idol_detail.document.getBody().getText().trim()))
			idol_detail = '';
		var file_url = $("#input-by-url").val();
		
		var form_data = new FormData();
		form_data.append('idol_name', idol_name);
		form_data.append('idol_national', idol_national);
		form_data.append('idol_birth', idol_birth);
		form_data.append('idol_detail', idol_detail);
		form_data.append('idol_id', idol_id);
		form_data.append('file_url', file_url);
        uploadIdolByUrl(form_data);
    }

    function collectionDataForFile(){
        var idol_name = $("#idol-name").val();
		var idol_national = $(".option.active").attr("data-value");
		var idol_birth = $("#idol-birth").val();
		var idol_detail = CKEDITOR.instances.idol_detail.getData();
		var idol_id = $("#idol-id").val();
		if(!String(CKEDITOR.instances.idol_detail.document.getBody().getText().trim()))
			idol_detail = '';
        var file_data = $('#input-by-file').prop('files')[0];

        var form_data = new FormData();
		form_data.append('idol_name', idol_name);
		form_data.append('idol_national', idol_national);
		form_data.append('idol_birth', idol_birth);
		form_data.append('idol_detail', idol_detail);
		form_data.append('idol_id', idol_id);
        if(file_data) {
			form_data.append('file_data', file_data);
		} else {
			form_data.append('file_data', '');
		}
        uploadIdolByFile(form_data);
    }

	function uploadIdolByFile(data){
        $.ajax({
			url: "{{url('collaborators/upload-idols/file')}}",
			type: 'POST',
			contentType: false,
			processData: false,
			data: data,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (res) {
				if(res.status){
					alertify.success(res.message);
					$('#next-trigger').click();
					$('#idol-upload').val(res.idol_id);
				}else{
					for(var i = 0; i < res.errors.length; i++){
						alertify.error(res.errors[i]);
					}
				}
			},
			error: function(error){ console.log(error); }
		}); 
	}
	
	function uploadIdolByUrl(data){
        $.ajax({
			url: "{{url('collaborators/upload-idols/url')}}",
			type: 'POST',
			contentType: false,
			processData: false,
			data: data,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (res) {
				if(res.status){
					alertify.success(res.message);
					$('#next-trigger').click();
					$('#idol-upload').val(res.idol_id);
				}else{
					for(var i = 0; i < res.errors.length; i++){
						alertify.error(res.errors[i]);
					}
				}
			},
			error: function(error){ console.log(error); }
		}); 
    }

	$("input[name='upload_method']").change(function(){ 
		if($(this).val()==='1'){
			$("#mod-upload").html(`<button class="upload-btn">Upload ảnh</button>
							<input type="file" id="input-by-file">`);
		}else{
			$("#mod-upload").html(`<input type="text" class="form-control" id="input-by-url" placeholder="Nhập URL của hình ảnh" required>`)
		}
	});

	$('#submit-button').on('click', function() {
		setTimeout(
            function() {
                if(counter > 0) {
					window.location = "{{url('collaborators/dashboard')}}";
				} else {
					alertify.error("Tập ảnh nhận diện không được bỏ trống!");
				}
            },
            2000);
	});
</script>
@endsection