<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Idol-Collaborators</title>

        <link href="{{asset('collaborators/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('collaborators/css/datepicker3.css') }}" rel="stylesheet">
        <link href="{{asset('collaborators/css/bootstrap-table.css')}}" rel="stylesheet">
        <link href="{{asset('collaborators/css/styles.css')}}" rel="stylesheet">

        
    <!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
    </head>
    <body class="login-wrap">
        <!-- Start row -->
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default background-login">
                    <div class="panel-heading background-login text-center custom-title">Công Tác Viên</div>
                    <div class="panel-body">
                        <form role="form" method="post" action="{{ url('collaborators/login') }}">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" required  type="text" name="user_name" 
                                        autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" required placeholder="Mật khẩu"  type="password"
                                        value="" name="password">
                                </div>
                                <div>
                                    @error('msg')
                                    <p class="txt-error">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="txt-center">
                                    <button type="submit" class="btn btn-primary">Đăng nhập</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
    </body>
</html>