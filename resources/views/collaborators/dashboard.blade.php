@extends('collaborators.layout')
@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Trang chủ cộng tác viên</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-blue panel-widget ">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left">
						<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper" />
						</svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="text-muted">Tổng tiền</div>
						<div class="large text-custom">{{number_format($payment)}} VND</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-orange panel-widget">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left approved">
						<svg class="glyph stroked checkmark">
							<use xlink:href="#stroked-checkmark" />
						</svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="text-muted">Phê Duyệt</div>
						<div class="large">{{number_format($number_approve)}} Idol</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-teal panel-widget">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left reject">
						<svg class="glyph stroked cancel">
							<use xlink:href="#stroked-cancel" />
						</svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="text-muted">Từ Chối</div>
						<div class="large">{{number_format($number_reject)}} Idol</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-red panel-widget">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left pending">
						<svg class="glyph stroked hourglass">
							<use xlink:href="#stroked-hourglass" /></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="text-muted">Đang Chờ</div>
						<p class="large">{{number_format($number_pending)}} Idol</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Lịch sử làm việc
				</div>
				<div class="panel-body" id="report-data">
					@include('collaborators.data')
				</div>
				<div class="ajax-load text-center" style="display:none">
					<p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Tải thêm báo cáo</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--/.main-->
@endsection
@section('script')
<script>
	var page = 1;
	var moreItem = true;
	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() >= $(document).height()) {
			if(moreItem) {
				page++;
				loadMoreData(page);
			}
		}
	});

	function loadMoreData(page) {
		$.ajax({
			url: '?page=' + page,
                type: 'GET',
                contentType: false,
				processData: false,
				beforeSend: function() {
					$('.ajax-load').show();
				},
				success: function (res) {
					if(res.status){
						$('.ajax-load').hide();
						$('#report-data').append(res.html);
					}else{
						if(moreItem) {
							moreItem = false;
							$('.ajax-load').hide();
							alertify.warning(res.warning);
						}
					}
				},
				error: function(error){ console.log(error); }
		});
	}
</script>
@endsection