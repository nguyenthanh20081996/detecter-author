@foreach($reports as $report)
<div class="content">
    <div>
        @if($report->status == 0)
        <h2>{{ $report->idol_name }} <span>+0 VND</span></h2>
        <p><i class="fa fa-exclamation-circle text-warning"></i>&nbsp;&nbsp;<span>Đang chờ</span></p>
        @elseif($report->status == 2)
        <h2>{{ $report->idol_name }} <span>+0 VND</span></h2>
        <p><i class="fa fa-times-circle text-danger"></i>&nbsp;&nbsp;<span>Từ chối</span></p>
        <p><b>Lý do:</b></p>
        <?php
            echo '<pre>';
            echo $report->note;
            echo '</pre>';
        ?>
        <a href="{{url('collaborators/upload-image/'.$report->idol_id)}}" class="btn btn-primary"
            style="margin: 20px 0;">Chỉnh sửa</a>
        @else
        <h2>{{ $report->idol_name }} <span>+{{$report->earned}} VND</span></h2>
        <p><i class="fa fa-check-circle text-success"></i>&nbsp;&nbsp;<span>Đã phê duyệt</span></p>
        @endif
    </div>
</div>
@endforeach