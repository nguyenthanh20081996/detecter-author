@extends('collaborators.layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Hồ sơ cộng tác viên</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Thông tin cơ bản
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-2 offset-lg-4">
                                    <button type="button" id="information-btn" class="btn btn-block btn-dark active">Cập
                                        nhật
                                        thông
                                        tin</button>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" id="change-password-btn" class="btn btn-block btn-dark">Đổi
                                        mật
                                        khẩu</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="wrap form-group" id="information-form">
                                <h1>{{Auth::user()->user_name}}</h1>
                                <h3>Chào mừng cộng tác viên của Idolcuatui</h3>
                                <div>
                                    <label for="bank-name">Tên ngân hàng</label>
                                    <input id="bank-name" type="text" class="form-control"
                                        value="{{Auth::user()->bank_name}}" />
                                </div>
                                <div>
                                    <label for="account-number">Số tài khoản</label>
                                    <input id="account-number" type="text" class="form-control"
                                        value="{{Auth::user()->bank_number}}" />
                                </div>
                                <div>
                                    <label for="account-name">Chủ tài khoản</label>
                                    <input id="account-name" type="text" class="form-control"
                                        value="{{Auth::user()->bank_account_holder}}" />
                                </div>
                                <div>
                                    <label for="department">Chi nhánh</label>
                                    <input id="department" type="text" class="form-control"
                                        value="{{Auth::user()->bank_location}}" />
                                </div>
                                <div>
                                    <label for="realname">Tên hiển thị</label>
                                    <input id="realname" type="text" class="form-control"
                                        value="{{Auth::user()->name}}" />
                                </div>
                                <div>
                                    <button type="button" id="submit-information-btn" class="btn btn-warning">Cập
                                        nhật</button>
                                </div>
                            </div>
                            <!-- second form -->
                            <div class="wrap form-group" id="change-password-form">
                                <h1>{{Auth::user()->user_name}}</h1>
                                <h3>Chào mừng cộng tác viên của Idolcuatui</h3>
                                <div>
                                    <label for="current-password">Mật khẩu hiện tại</label>
                                    <input id="current-password" type="password" class="form-control" />
                                </div>
                                <div>
                                    <label for="password">Mật khẩu mới</label>
                                    <input id="password" type="password" class="form-control" />
                                </div>
                                <div>
                                    <label for="confirm-password">Xác nhận mật khẩu</label>
                                    <input id="confirm-password" type="password" class="form-control" />
                                </div>
                                <div>
                                    <button type="button" id="submit-change-password-btn" class="btn btn-warning">Cập
                                        nhật</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <!--/.row-->
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#change-password-form').hide();

        $('#information-btn').on('click', function() {
            $(this).addClass('active');
            $('#change-password-btn').removeClass('active');
            $('#change-password-form').hide();
            $('#information-form').show();
        });

        $('#change-password-btn').on('click', function() {
            $(this).addClass('active');
            $('#information-btn').removeClass('active');
            $('#information-form').hide();
            $('#change-password-form').show();
        });

        $('#submit-information-btn').on('click', function() {
            collectInformationData();
        });

        $('#submit-change-password-btn').on('click', function() {
            collectPasswordData();
        });

        function collectInformationData() {
            var bank_name = $("#bank-name").val();
            var account_name = $("#account-name").val();
            var account_number = $("#account-number").val();
            var department = $("#department").val();
            var realname = $("#realname").val();

            var form_data = new FormData();
            form_data.append('bank_name', bank_name);
            form_data.append('account_name', account_name);
            form_data.append('account_number', account_number);
            form_data.append('department', department);
            form_data.append('realname', realname);
            submitInformationData(form_data);
        }

        function collectPasswordData() {
            var current_password = $('#current-password').val();
            var password = $('#password').val();
            var password_confirmation = $('#confirm-password').val();

            var form_data = new FormData();
            form_data.append('current_password', current_password);
            form_data.append('password', password);
            form_data.append('password_confirmation', password_confirmation);
            submitPasswordData(form_data);
        }

        function submitInformationData(data) {
            $.ajax({
                url: "{{url('collaborators/profile/update')}}",
                type: 'POST',
                contentType: false,
                processData: false,
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {
                    if(res.status){
                        alertify.success(res.message);
                    }else{
                        for(var i = 0; i < res.errors.length; i++){
                            alertify.error(res.errors[i]);
                        }
                    }
                },
                error: function(error){ console.log(error); }
            });
        }

        function submitPasswordData(data) {
            $.ajax({
                url: "{{url('collaborators/profile/updatePassword')}}",
                type: 'POST',
                contentType: false,
                processData: false,
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {
                    if(res.status){
                        alertify.success(res.message);
                    }else{
                        for(var i = 0; i < res.errors.length; i++){
                            alertify.error(res.errors[i]);
                        }
                    }
                },
                error: function(error){ console.log(error); }
            });
        }
    });
</script>
@endsection