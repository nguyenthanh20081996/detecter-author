<!doctype html>
<html lang="en">

<head>
  <title>Đăng Nhập</title>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS -->
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/alertify.min.css"/>
  <!-- Default theme -->
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/default.min.css"/>
  <!-- Semantic UI theme -->
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/semantic.min.css"/>
  <link href="{{asset('css/material-dashboard.css')}}" rel="stylesheet" />
  <style>
      .access-wrap{
        position: fixed;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
        background: white;
        z-index: 10;
      }
  </style>
</head>
<body>
   <div class="access-wrap">
        <form action="{{url('admin/access/handle')}}" method="POST">
            {!! csrf_field() !!}
            <div class="card" style="width: 390px;margin:10% auto;">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Đăng Nhập</h4>
                    <p class="card-category"> Hãy Nhập Mã Truy Cập</p>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="access_token" class="form-control" placeholder="Hãy nhập mã truy cập...">
                            @if($errors->any())
                                <p style="color: red;font-size:13px">{{$errors->first()}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-success" style="padding: 10px;">Truy Cập</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
  </div>
</body>
</html>