@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Quản Lý Cộng Tác Viên</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
        </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <form action="{{url('admin/add-collaborator')}}" method="POST"> 
                {!! csrf_field() !!}
                <div class="row">
                        <div class="col-md-2">
                            <div style="padding: 12px;font-size: 15px;">
                                <span>Tên CTV</span>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                <div class="row">
                        <div class="col-md-2">
                            <div style="padding: 12px;font-size: 15px;">
                                <span>User Name</span>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="user_name">
                        </div>
                    </div>   
                    <div class="row">
                            <div class="col-md-2">
                                <div style="padding: 12px;font-size: 15px;">
                                    <span>Mật Khẩu</span>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="pass">
                            </div>
                        </div> 
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button class="btn btn-success" type="submit">Thêm</button>
                        </div>    
                    </div>     
        </form>
    </div> 
</div>
@endsection