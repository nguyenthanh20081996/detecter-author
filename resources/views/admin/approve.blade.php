@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Xác Nhận Idol Của Người Dùng</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Danh Sách Đợi Phê Duyệt</h4>
                            <p class="card-category"> Tổng số Idol chờ phê duyệt: {{number_format($idol_waiting_numbers)}}</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th>
                                                Tên
                                            </th>
                                            <th>
                                                Hình Đại Diện
                                            </th>
                                            <th>
                                                Miêu Tả
                                            </th>
                                            <th>
                                                Thao Tác
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($idol_waiting as $item)
                                        <tr>
                                            <td>
                                                {{$item->name}}
                                            </td>
                                            <td>
                                                <img src="{{$item->thumb}}" style="width: 120px">
                                            </td>
                                            <td>
                                                {!! $item->des !!}
                                            </td>
                                            <td id="idol-{{$item->id}}"> 
                                                <button class="btn btn-success approved" data-id={{$item->id}} style="padding: 5px 10px;"><i class="fa fa-check-circle" aria-hidden="true"></i> Phê Duyệt</button>
                                                <button class="btn btn-danger delete" data-id={{$item->id}} style="padding: 5px 10px;"><i class="fa fa-trash" aria-hidden="true"></i>
                                                    Xóa</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){   
        $(".approved").click(function(){
            var idol_id = $(this).attr("data-id");
            var data = {
                idol_id: idol_id
            }

            approveIdol(data);
        });

        $(".delete").click(function(){
            var idol_id = $(this).attr("data-id");
            var data = {
                idol_id: idol_id
            }

            deleteIdol(data);
        });
    });

    function approveIdol(data){
        $.ajax({
        url: "{{url('admin/approve/handle')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
                alertify.success(res.message);
                var idol_id = res.id;
                $("#idol-"+idol_id).html(`<i class="fa fa-check-circle" aria-hidden="true" style="color: green"></i> Đã phê duyệt`);

           }else{
                alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function deleteIdol(data){
        $.ajax({
        url: "{{url('admin/delete/handle')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
                alertify.success(res.message);
                var idol_id = res.id;
                $("#idol-"+idol_id).html(`<i class="fa fa-trash-o" aria-hidden="true" style="color: red"></i> Đã xóa`);
           }else{
                alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

</script>
@endsection