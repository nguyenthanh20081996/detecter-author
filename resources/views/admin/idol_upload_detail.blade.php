@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Phê duyệt Idol</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <ul class="tab-nav">
                    <span class="bottom-border"></span>
                    <li class="tab-nav__item active"><a href="#first">Phê Duyệt</a></li>
                    <li class="tab-nav__item"><a href="#second">Từ Chối</a></li>
                </ul>
            </div>
            <div class="col-md-12 tab active" data-tab="#first">
                @if(!empty($approve_status))
                @if($approve_status == 1)
                <div class="row">
                    <div class="col-md-12">
                        <div style="padding: 10px; background-color: green; color: white">
                            <span>Đã cập nhật</span>
                        </div>
                    </div>
                </div>
                @elseif($approve_status == 0)
                <div class="row">
                    <div class="col-md-12">
                        <div style="padding: 10px; background-color: red; color: white">
                            <span>Cập nhật bị lỗi</span>
                        </div>
                    </div>
                </div>
                @endif
                @endif
                <form action="#" method="POST" style="margin: 40px 0;" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="idol-id" value="{{$idol->id}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">
                                    <div style="padding: 10px 0px">
                                        <span>Ảnh đại diện</span>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type='file' id="idol_thumb" name="idol_thumb" />
                                        </div>
                                        <div class="col-md-12">
                                            <img id="idol_thumb_preview" src="{{$idol->thumb}}" alt="{{$idol->name}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div style="padding: 10px 0px">
                                        <span>Tên Idol</span>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="{{$idol->name}}" name="idol_name"
                                        id="idol_name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div style="padding: 10px 0px">
                                        <span>Năm Sinh</span>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" value="{{$idol->birth}}" name="idol_birth"
                                        id="idol_birth">
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px">
                                <div class="col-md-2">
                                    <div style="padding: 10px 0px">
                                        <span>Quốc Gia</span>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <select class="form-control" name="idol_country" id="idol_country">
                                        @foreach ($national as $item)
                                        <option @if($item->id == $idol->national_id) selected @endif
                                            value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="padding: 10px 0px">
                                        <span>Mô tả</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea style="height: 150px" id="idol_description"
                                        name="idol_description">{!!$idol->des!!}</textarea>
                                    <input type="hidden" name="idol_id" value="{{$idol->id}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <div style="padding: 20px 0px">
                                <span>Danh sách ảnh tranining</span>
                            </div>
                            <div class="col-md-12 text-center" id="slide-upload-images">
                                @foreach($idolImages as $idolImage)
                                <div id={{"training-image-".$idolImage->id}} class="tranining-image">
                                    <img src="{{$idolImage->link}}" alt="{{$idolImage->original_name}}" />
                                    <div class="sub-menu">
                                        <div class="form-group float-left">
                                            @if($idolImage->is_showing == 1)
                                            <input type="checkbox" id={{"image-display-".$idolImage->id}}
                                                name="image-display" value={{$idolImage->id}} checked />
                                            @else
                                            <input type="checkbox" id={{"image-display-".$idolImage->id}}
                                                name="image-display" value={{$idolImage->id}} />
                                            @endif
                                            <label for={{"image-display-".$idolImage->id}}>Hiển thị</label>
                                        </div>
                                        <button data-id="{{$idolImage->id}}" class="btn btn-danger delete-image-btn">
                                            Xóa
                                        </button>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center" style="margin-top: 20px">
                            <button type="submit" id="approve-btn" class="btn btn-success">Cập Nhật</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12 tab" data-tab="#second">
                @if(!empty($denied_status))
                @if($denied_status == 1)
                <div class="row">
                    <div class="col-md-12">
                        <div style="padding: 10px; background-color: green; color: white">
                            <span>Đã cập nhật</span>
                        </div>
                    </div>
                </div>
                @elseif($denied_status == 0)
                <div class="row">
                    <div class="col-md-12">
                        <div style="padding: 10px; background-color: red; color: white">
                            <span>Cập nhật bị lỗi</span>
                        </div>
                    </div>
                </div>
                @endif
                @endif
                <form action="#" method="POST">
                    @csrf
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12 text-center">
                            <div style="padding: 10px 0px">
                                <span>Lý do từ chối</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <textarea id="reject_description" name="reject_description"></textarea>
                            <input type="hidden" name="idol_id" value="{{$idol->id}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center" style="margin-top: 20px">
                            <button type="submit" id="reject-btn" class="btn btn-success">Cập Nhật</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset ('js/tinymce.min.js') }}"></script>
<script src="{{ asset ('js/init-tinymce.js') }}"></script>
<script src="{{asset('js/multi-tab.js')}}"></script>
<script>
    tinymce.init({
        selector: '#idol_description',
        valid_elements: '*[*]',
        mode: "textareas",
        height: 180
    });
    tinymce.init({
        selector: '#reject_description',
        valid_elements: '*[*]',
        mode: "textareas",
        height: 300
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#idol_thumb_preview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#idol_thumb").change(function() {
        readURL(this);
    });

    $(document).ready(function(){
        $('.delete-image-btn').on('click', function() {
            event.preventDefault();
            deleteIdolImage($(this).attr('data-id'));
        });

        function deleteIdolImage(id) {
            $.ajax({
                url: '{{url("admin/idol-image/delete")}}',
                type: "GET",
                data: {
                    id: id
                },
                success: function(res) {
                    if(res.status) {
                        $("#training-image-"+res.id).remove();
                        alertify.success(res.message);
                    } else {
                        alert(res.idolImage);
                        alertify.error(res.error);
                    }
                },
                error: function(error){ console.log(error); }
            });
        }
        
        $('#approve-btn').on('click', function() {
            event.preventDefault();
            collectDataForApprove();
        });

        function collectDataForApprove() {
            var idol_id = $('#idol-id').val();
            var idol_thumb = $('#idol_thumb').prop('files')[0];
            var idol_name = $('#idol_name').val();
            var idol_birth = $('#idol_birth').val();
            var idol_country = $("#idol_country :selected").val();
            var idol_description = tinymce.get('idol_description').getContent();
            var idol_images = [];
            
            var form_data = new FormData();
            form_data.append('idol_id', idol_id);
            if(idol_thumb) {
                form_data.append('idol_thumb', idol_thumb);
            } else {
                form_data.append('idol_thumb', '');
            }
            form_data.append('idol_name', idol_name);
            form_data.append('idol_birth', idol_birth);
            form_data.append('idol_country', idol_country);
            form_data.append('idol_description', idol_description);
            $.each($('input[name="image-display"]:checked'), function() {
                idol_images.push(parseInt($(this).val()));
            });
            form_data.append('idol_images', idol_images);
            console.log(form_data.get('idol_description'));
            uploadApproveIdol(form_data);
        }

        function uploadApproveIdol(data) {
            $.ajax({
                url: "{{url('admin/collaborator/check_approve')}}",
                type: "POST",
                contentType: false,
                processData: false,
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    if(res.status) {
                        alertify.success(res.message);

                        setInterval(() => {
                            window.location = "{{url('admin/pendent-list')}}";
                        }, 500);

                    } else {
                        for(let i=0; i<res.errors.length; i++) {
                            alertify.error(res.errors[i]);
                        }
                        console.log(res.message);
                    }
                },
                error: function(error) { console.log(error); }
            });
        }

        $('#reject-btn').on('click', function() {
            event.preventDefault();
            collectDataForReject();
        });

        function collectDataForReject() {
            var idol_id = $('#idol-id').val();
            var reject_description = tinymce.get('reject_description').getContent();
            var form_data = new FormData();
            form_data.append('idol_id', idol_id);
            form_data.append('reject_description', reject_description);
            uploadRejectIdol(form_data);
        }

        function uploadRejectIdol(data) {
            $.ajax({
                url: "{{url('admin/collaborator/check_reject')}}",
                type: "POST",
                contentType: false,
                processData: false,
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    if(res.status) {
                        alertify.success(res.message);

                        setInterval(() => {
                            window.location = "{{url('admin/pendent-list')}}";
                        }, 500);
                    } else {
                        for(let i=0; i<res.errors.length; i++) {
                            alertify.error(res.errors[i]);
                        }
                        console.log(res.message);
                    }
                },
                error: function(error) { console.log(error); }
            });
        }
    });

</script>
@endsection