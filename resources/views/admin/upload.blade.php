@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Up Person</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
               <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Up Idols</h4>
                        <p class="card-category">Tất cả Idol sẽ được đưa lên vào lúc 12h</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="title-upload">
                                    <span>Chọn Group</span>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="group-selected">
                                    @foreach ($group_list as $item)
                                        <option value="{{$item->personGroupId}}">{{$item->personGroupId}} - {{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="upload-person" style="margin-top: 20px">
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button class="btn btn-success add-person" style="padding: 8px 12px;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm Person</button>
                                    <button class="btn btn-primary upload-idol" style="padding: 8px 12px;" disabled><i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload</button>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    var idol_element = 0;
    var training_picture = 0;
    var image_id;
    var picture_element;
    
    $(document).ready(function(){
        $('.add-person').click(function(){
            $(".upload-idol").prop('disabled', false);
            createTraningIdolElement();
        });

        $(document).on('click', '.delete-idols', function(){
            var idol_id = $(this).attr('data-id');
            $(".training-element-"+idol_id).remove();
            if($(".upload-person").children().length == 0){
                $(".upload-idol").prop('disabled', true);
            }
        });

        $(document).on('click', '.add-training-picture', function(){
            var idol_id = $(this).attr('data-id');
            createTraningInputElement(idol_id);
        }); 

        $(document).on('click', '.delete-picture-training-item', function(){
            var picture_training_item = $(this).attr('data-id');
            var idol_id = $(this).attr('data-idol');
            
            $(".picture-training-item-"+picture_training_item+'-'+idol_id).remove();
        });

        $(".upload-idol").click(function(){
            $(this).html(`<img src="{{asset('img/loading.gif')}}" width="17">`);
            $(this).prop('disabled', true);
            collectData();
        });

        // Check image for thumbnail
        $(document).on('keyup','.thumbnail-image', function(){
            var image_source = $(this).val();
            var idol_id = $(this).attr("data-id");
            image_id = idol_id;
            testImage(image_source);
        });

        $(document).on('keyup', '.picture-training-event', function(){
            var idol_id = $(this).attr("data-id");
            var element_id = $(this).attr("data-element");
            var url = $(this).val();

            picture_element = "#check-picture-result-"+idol_id+"-"+element_id; 
             
            testPicture(url);
        });

    });

    function testPicture(URL){
        var tester=new Image();
        tester.src=URL;
        tester.onload=pictureFound;
        tester.onerror=pictureNotFound;
    }
    

    function testImage(URL) {
        var tester=new Image();
        tester.src=URL;
        tester.onload=imageFound;
        tester.onerror=imageNotFound;
    }

    function pictureFound(){
        $(picture_element).html(`<i class="fa fa-check" aria-hidden="true" style="color:green;font-size: 20px;"></i>`);
    }

    function pictureNotFound(){
        $(picture_element).html(`<i class="fa fa-times" aria-hidden="true" style="color:red;font-size: 20px;"></i>`);
    }

    function imageFound() {
        $('#check-image-result-'+image_id).html(`<i class="fa fa-check" aria-hidden="true" style="color:green;font-size: 20px;"></i>`);
        console.log("Found");
    }

    function imageNotFound() {
        $('#check-image-result-'+image_id).html(`<i class="fa fa-times" aria-hidden="true" style="color:red;font-size: 20px;"></i>`);      
        console.log("Not found");
    }

    function collectData(){
        var idol_list = $(".training-item");
        var group_selected = $("#group-selected").val();

        var idol_arr = [];
        
        for(let x = 0; x < idol_list.length; x++){
            var picture_item = [];
            var idol_id = $(idol_list[x]).attr('data-id');
            var idol_name = $('#name-'+idol_id).val();
            var idol_thumb = $('#thumb-'+idol_id).val();

            var input_training_picture = $('.input-training-picture-'+idol_id);
            for(let z = 0; z < input_training_picture.length; z++){

                var link_picture = $(input_training_picture[z]).val();
                if(String(link_picture).trim() != ''){
                    picture_item.push(String(link_picture).trim());
                }
            }

            var temp_data = {
                name: idol_name,
                id_element: idol_id,
                thumb: idol_thumb,
                group_selected: group_selected,
                picture_arr: picture_item
            }   

            idol_arr.push(temp_data);
        }

        var data = {
            idol_arr: idol_arr
        }
        $(".training-item").css('background-color','white');
        uploadIdolToServer(data);
    }

    function uploadIdolToServer(data){
        $.ajax({
        url: "{{url('admin/up-person/upload')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            $(".upload-idol").prop('disabled', false);
            $(".upload-idol").html(`<i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload`);
            if(res.status){
                alertify.success('Đã Upload');
            }else{
                alertify.error(res.message);
            }
            console.log(res);
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function createTraningInputElement(idol_id){
        var number_traning_item = $(".block-training-picture-"+idol_id).children().length;
        number_traning_item++;

        var element = `
                            <div class="picture-training-item-`+number_traning_item+`-`+idol_id+`">
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-md-9">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">Ảnh Training</label>
                                                <input type="text" data-element="`+number_traning_item+`" data-id="`+idol_id+`" class="form-control input-training-picture-`+idol_id+` picture-training-event">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="picture-checking" id="check-picture-result-`+idol_id+`-`+number_traning_item+`" style="padding: 12px 0px;">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="delete-element text-left">
                                                <button class="btn btn-danger delete-picture-training-item" data-idol="`+idol_id+`" data-id="`+number_traning_item+`" style="padding: 5px 5px;width: 100%"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
        `

        $(".block-training-picture-"+idol_id).append(element);
    }

    function createTraningIdolElement(){
        idol_element++;
        var element = `
                    <div class="training-item training-element-`+idol_element+`" data-id="`+idol_element+`">
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-11">
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Tên Idol</label>
                                    <input type="text" class="form-control" id="name-`+idol_element+`">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="delete-element text-left">
                                    <button data-id="`+idol_element+`" class="btn btn-danger delete-idols" style="padding: 5px 5px;width: 100%"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-11">
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Ảnh đại diện</label>
                                    <input type="text" class="form-control thumbnail-image" data-id="`+idol_element+`" id="thumb-`+idol_element+`">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div id="check-image-result-`+idol_element+`" style="padding: 12px 0px;">
                                </div>
                            </div>
                        </div>
                        <div class="block-training-picture-`+idol_element+`">
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12 text-left">
                                <button class="btn btn-primary add-training-picture" data-id="`+idol_element+`" style="padding: 5px 5px;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm Ảnh</button>
                            </div>
                        </div>
                    </div>    
        `
        if($(".upload-person").children().length > 0){
            $(".training-item:last").after(element);
        }else{
            $(".upload-person").html(element);
        }
    }

</script>
@endsection