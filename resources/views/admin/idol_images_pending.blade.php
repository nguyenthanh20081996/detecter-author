@extends('admin.layout.admin')
@section('content')
<div class="container">
    @foreach($idol_list as $item)
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12">
            <a href="{!! route('PendingImages', ['id' => $item->id]) !!}">
                <div class="idols-pending">
                    <span>{{$item->name}}</span><br>
                    <p>{{$item->pending_numbers}} Hình Ảnh</p>
                </div>
            </a>
        </div>
    </div>
    @endforeach
</div>
@endsection