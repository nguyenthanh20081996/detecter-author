@if($records->isEmpty())
<tr>
    <td colspan="5" align="center">Không tìm thấy bất cứ kết quả nào!</td>
</tr>
@else
@foreach ($records as $record)
<tr>
    <td>
        {{$record->user_name}}
    </td>
    <td>
        {{$record->name}}
    </td>
    <td>
        {{$record->idol_numbers}}
    </td>
    <td>
        {{$record->created_at}}
    </td>
    <td>
        @if($record->is_blocked == 1)
        <button class="btn btn-success approved" id={{"btn-unblock-".$record->id}} data-id={{$record->id}}
            style="padding: 5px 10px; display: block"><i class="fa fa-check-circle" aria-hidden="true"></i> Mở
            khóa</button>
        <button class="btn btn-danger delete" id={{"btn-block-".$record->id}} data-id={{$record->id}}
            style="padding: 5px 10px; display: none"> <i class="fa fa-trash" aria-hidden="true"></i> Khóa</button>
        @else
        <button class="btn btn-success approved" id={{"btn-unblock-".$record->id}} data-id={{$record->id}}
            style="padding: 5px 10px; display: none"><i class="fa fa-check-circle" aria-hidden="true"></i> Mở
            khóa</button>
        <button class="btn btn-danger delete" id={{"btn-block-".$record->id}} data-id={{$record->id}}
            style="padding: 5px 10px; display: block"> <i class="fa fa-trash" aria-hidden="true"></i> Khóa</button>
        @endif
    </td>
</tr>
@endforeach
<tr>
    <td colspan="5" align="center">
        {!! $records->links() !!}
    </td>
</tr>
@endif