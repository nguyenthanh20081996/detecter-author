@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Danh Sách Người Dùng</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
            <span class="bmd-form-group">
                <div class="input-group no-border">
                    <input type="text" value="" class="form-control searching-person" placeholder="Tìm kiếm Idol...">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Idols</h4>
                        <p class="card-category"> Tất cả idols trên hệ thống</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive init-data">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Tên
                                        </th>
                                        <th>
                                            Ảnh Đại Diện
                                        </th>
                                        <th>
                                            Azure ID
                                        </th>
                                        <th>
                                            Group ID
                                        </th>
                                        <th>
                                            Số Ảnh
                                        </th>
                                        <th>
                                            Thao Tác
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($idols as $item)
                                        <tr class="idol-{{$item->id}}">
                                            <td class="name-{{$item->id}}">{{$item->name}}</td>
                                            <td>
                                                <div class="idol-thumb">
                                                    <img src="{{$item->thumb}}" alt="{{$item->name}}">
                                                </div>
                                            </td>
                                            <td>{{$item->person_id}}</td>
                                            <td>{{$item->group_id}}</td>
                                            <td>{{$item->total_face}}</td>
                                            <td class="action-person">
                                                <a href="{!! route('AddImage', ['id' => $item->id]) !!}" class="btn btn-success" style="padding: 5px 10px;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm Ảnh</a><br>
                                                <a href="{!! route('SetImageShowing', ['id' => $item->id]) !!}" class="btn btn-success" style="padding: 5px 10px;"><i class="fa fa-file-image-o" aria-hidden="true"></i> Bộ Ảnh</a><br>
                                                <a class="btn btn-success" style="padding: 5px 10px;" href="{!! route('Description', ['id' => $item->id]) !!}"><i class="fa fa-file-text" aria-hidden="true"></i> Miêu Tả</a><br>
                                                <button class="btn btn-primary update-person" data-name="{{$item->name}}" data-id="{{$item->id}}" style="padding: 5px 10px;"><i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật</button><br>
                                                <button class="btn btn-danger delete-person" data-name="{{$item->name}}" data-id="{{$item->id}}" style="padding: 5px 10px;"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive search-data" style="display: none">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Tên
                                        </th>
                                        <th>
                                            Ảnh Đại Diện
                                        </th>
                                        <th>
                                            Azure ID
                                        </th>
                                        <th>
                                            Group ID
                                        </th>
                                        <th>
                                            Số Ảnh
                                        </th>
                                        <th>
                                            Thao Tác
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 paginate-link">
                {{$idols->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){

        $(document).on('click', '.update-person', function(){
            var idol_name = $(this).attr("data-name");
            var idol_id = $(this).attr("data-id");

            alertify.prompt( '<i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật Idol', 'Tên Idol', idol_name
            , function(evt, value) { 

                var data = {
                    new_name: value,
                    idol_id: idol_id
                }
                console.log(value);
                updateIdol(data);
            }, function() {});
        });

        $(document).on('click', '.delete-person', function(){
            var idol_name = $(this).attr("data-name");
            var idol_id = $(this).attr("data-id");

            alertify.confirm('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Thông Báo', 'Xóa tất cả dữ liệu của <strong>'+idol_name + '</strong>?', function(){
                var data = {
                    idol_id: idol_id
                }

                deleteIdol(data);
            }, function(){});
        });


        $(".searching-person").keyup(function(){
            var search_value = $(this).val();
            console.log("Search Value", search_value);
            if(search_value){
                var data = {
                    searching: search_value
                }
            
                searchIdol(data);
                $(".search-data").show();
                $(".init-data").hide();
                $('.paginate-link').hide();
            }else{
                $(".search-data").hide();
                $(".init-data").show();
                $('.paginate-link').show();
            }

            
        });

    });

    function createElementData(data){
        var element = "";
        
        if(data.length == 0){
            element = 'Không tìm thấy idols';
        }else{
            for(let x = 0; x < data.length; x++){
            let person_id = "";
            if(data[x].person_id){
                person_id = data[x].person_id;
            }

                element+=`
                                            <tr class="idol-`+data[x].id+`">
                                                <td class="name-`+data[x].id+`">`+data[x].name+`</td>
                                                <td>
                                                    <div class="idol-thumb">
                                                        <img src="`+data[x].thumb+`" alt="`+data[x].name+`">
                                                    </div>
                                                </td>
                                                <td>`+person_id+`</td>
                                                <td>`+data[x].group_id+`</td>
                                                <td>`+data[x].total_face+`</td>
                                                <td>
                                                    
                                                    <button class="btn btn-primary update-person" data-name="`+data[x].name+`" data-id="`+data[x].id+`" style="padding: 5px 10px;"><i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật</button>
                                                    <button class="btn btn-danger delete-person" data-name="`+data[x].name+`" data-id="`+data[x].id+`" style="padding: 5px 10px;"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</button>
                                                </td>
                                            </tr>
                `
            }
        }

        $('.search-data > table > tbody').html(element);
    }

    function searchIdol(data){
        $.ajax({
        url: "{{url('admin/person/search')}}",
        type: 'GET',
        data: data,
        success: function (res) {
           console.log(res);
           if(res.status){
                createElementData(res.data);
           }else{
               console.log(res);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function updateIdol(data){
        $.ajax({
        url: "{{url('admin/person/update')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
                alertify.success(res.message);
                var idol_id = res.idol_id;
                $(".name-"+idol_id).html(res.name);
           }else{
                alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }


    function deleteIdol(data){
        $.ajax({
        url: "{{url('admin/person/delete')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            if(res.status){
                alertify.success(res.message);
                $(".idol-"+res.idol_id).remove();
            }else{
                alertify.error(res.message);
            }
        }, 
        error: function(error){ console.log(error); } }); 
    }

</script>
@endsection