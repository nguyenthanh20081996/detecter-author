@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Báo Cáo</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @foreach ($reports as $report)
                    <div class="report-item" 
                    @if($report->status == 200) 
                        style="background-color: green;margin-top: 20px;" 
                    @else 
                        style="background-color: #bb0d00;margin-top: 20px;"  
                    @endif>
                        <span>{{$report->text}}</span>
                    </div>
                    @foreach ($report_image_arr[$report->id] as $item)
                        <div class="report-item"
                        @if($item->status == 200) 
                            style="background-color: green;" 
                        @else 
                            style="background-color: #bb0d00;"  
                        @endif
                        ><i class="fa fa-eercast" aria-hidden="true"></i> {{$item->text}}</div>
                    @endforeach
                @endforeach
            </div>
        </div>
        @if(count($report_list_add_image) > 0)
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12">
                <strong>Ảnh được thêm trong ngày</strong>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                @foreach ($report_list_add_image as $item)
                    <div class="report-item"
                    @if($item->status == 200) 
                        style="background-color: green;" 
                    @else 
                        style="background-color: #bb0d00;"  
                    @endif
                    ><i class="fa fa-eercast" aria-hidden="true"></i> {{$item->text}}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection