@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Chọn Ảnh Hiển Thị</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
    <form action="{{url('admin/images/setting')}}" method="POST">
        <div class="row">
            <div class="col-md-12">
                <strong>Bộ Sưu Tập</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="check-all-images"><input type="checkbox" id="check-all-images"> Chọn tất cả</label>
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
            <div class="col-md-12">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{$person_id}}" name="person_id">
                    @foreach ($images as $item)
                        <div class="img-select" style="margin-top: 10px;" id="image-idol-{{$item->id}}">
                            <div class="img-select-content">
                                <img src="{{$item->link}}">
                            </div>
                            <div class="img-select-footer">
                                <label><input type="checkbox" name="show[]" value="{{$item->id}}" @if($item->is_showing) checked @endif> Hiện Thị</label>
                                @if($item->is_upload)
                                    <i class="fa fa-check-circle" aria-hidden="true" style="color: green;margin-left: 10px"></i>
                                @else
                                    <i class="fa fa-check-circle" aria-hidden="true" style="color: #b9b9b9;margin-left: 10px"></i>
                                @endif
                                <button type="button" class="btn btn-danger delete-img" data-id="{{$person_id}}" data-img="{{$item->id}}"  style="padding: 5px 10px;margin-left: 10px"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top: 20px">
                <button class="btn btn-primary" type="submit">Cập Nhật</button>
            </div>
        </div>
    </form>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    $(".delete-img").click(function(){
        var img_id = $(this).attr("data-img");
        var person_id = $(this).attr("data-id");

        var data = {
            img_id: img_id,
            idol_id: person_id
        }

        deleteImageIdol(data);  
    });

    $("#check-all-images").click(function(){
        var check_all = $(this).is(':checked');
        $('input[name="show[]"]').each(function(){
            $(this).prop("checked" , check_all);
        });
    });
});

function deleteImageIdol(data){
    $.ajax({
        url: "{{url('admin/person/image/delete/face')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            if(res.status){
                $("#image-idol-"+res.img_id).hide(300, function(){
                    $("#image-idol-"+res.img_id).remove();
                });
               
                alertify.success(res.message);
            }else{
                alertify.error(res.message);
            }
        }, 
        error: function(error){ console.log(error); } }); 
}
</script>
@endsection