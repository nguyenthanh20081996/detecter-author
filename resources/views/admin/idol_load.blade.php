@foreach($queue as $report)
<div class="content">
    <div>
        <h2>
            <a href="{{url('admin/collaborator/check_approve/'.$report->idol_id)}}">
                {{ $report->idol_name }}
            </a>
        </h2>
        <p><b>Upload By:</b> {{ $report->user_name }}</p>
    </div>
</div>
@endforeach
<div>{!! $queue->links() !!}</div>