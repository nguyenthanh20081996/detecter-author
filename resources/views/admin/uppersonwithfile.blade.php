@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Up Person</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Up Idols</h4>
                        <p class="card-category">Tất cả Idol sẽ được đưa lên vào lúc 12h</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="title-upload">
                                    <span>Chọn Idol</span>
                                </div>
                            </div>
                            <div class="col-md-9 col-lg-9 col-sm-9 col-xl-9">
                                <span class="bmd-form-group">
                                    <div class="input-group no-border">
                                        <input type="text" value="" class="form-control searching-person"
                                            placeholder="Tìm kiếm Idol...">
                                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                            <i class="material-icons">search</i>
                                            <div class="ripple-container"></div>
                                        </button>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="idol-search-list">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="padding: 20px;"></div>
                                <p>Tổng Số Ảnh: <span id="counter"># 0</span></p>
                                <p>Ảnh Của Idol: <span id="idol_selected"># NULL</span></p>
                                <form method="post" action="{{ url('admin/images-save') }}"
                                    enctype="multipart/form-data" class="dropzone" id="my-dropzone"
                                    style="min-height: 500px">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="idol_upload" name="idol_id" value="1">
                                    <div class="dz-message">
                                        <div class="col-xs-8">
                                            <div class="message">
                                                <p>Drop files here or Click to Upload</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fallback">
                                        <input type="file" name="file" multiple>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="preview" style="display: none;">

                            <div class="dz-preview dz-file-preview">
                                <div class="dz-image"><img data-dz-thumbnail /></div>

                                <div class="dz-details">
                                    <div class="dz-size"><span data-dz-size></span></div>
                                    <div class="dz-filename"><span data-dz-name></span></div>
                                </div>
                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                <div class="dz-error-message"><span data-dz-errormessage></span></div>



                                <div class="dz-success-mark">

                                    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1"
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                        <title>Check</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            sketch:type="MSPage">
                                            <path
                                                d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                id="Oval-2" stroke-opacity="0.198794158" stroke="#747474"
                                                fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup">
                                            </path>
                                        </g>
                                    </svg>

                                </div>
                                <div class="dz-error-mark">

                                    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1"
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                        <title>error</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            sketch:type="MSPage">
                                            <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474"
                                                stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                                                <path
                                                    d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                    id="Oval-2" sketch:type="MSShapeGroup"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    var total_photos_counter = 0;
    Dropzone.options.myDropzone = {
        uploadMultiple: true,
        parallelUploads: 2,
        maxFilesize: 16,
        previewTemplate: document.querySelector('#preview').innerHTML,
        addRemoveLinks: true,
        dictRemoveFile: 'Remove file',
        dictFileTooBig: 'Image is larger than 16MB',
        timeout: 10000,
    
        init: function () {
            this.on("removedfile", function (file) {
                $.post({
                    url: "{{url('admin/images-delete')}}",
                    data: {id: file.name, _token: $('[name="_token"]').val()},
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        total_photos_counter--;
                        $("#counter").text("# " + total_photos_counter);
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            });
        },
        success: function (file, done) {
            total_photos_counter++;
            $("#counter").text("# " + total_photos_counter);
        }
    };

    $(".searching-person").keyup(function(){
            var search_value = $(this).val();
            console.log("Search Value", search_value);
            if(search_value){
                var data = {
                    searching: search_value
                }
                searchIdol(data);
            }else{
                $(".idol-search-list").html('');
            }

            if(String(search_value).trim() == ''){
                $(".idol-search-list").html('');
            }
    });

    $(document).on('click', '.select-idol-dropzone', function(){
        let idol_id = $(this).attr('data-id');
        let idol_name = $(this).attr('data-name');
        alertify.success('Đã chọn ' + idol_name);
        $("#idol_selected").text('# ' + idol_name);
        $("#idol_upload").val(idol_id);
    });

    function searchIdol(data){
        $.ajax({
        url: "{{url('admin/person/search')}}",
        type: 'GET',
        data: data,
        success: function (res) {
           console.log(res);
           if(res.status){
                createIdolSearchList(res.data);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }    

    function createIdolSearchList(data){
        if(data.length == 0){
            var element = `<div style="text-align: center; padding: 10px"><p>Không tìm thấy kết quả</p></div>`;
            $(".idol-search-list").html(element);
        }else{
            var element = '';
            for(let x = 0; x < data.length; x++){
                element+=`
                    <div class="select-idol-dropzone" data-id="`+data[x].id+`" data-name="`+data[x].name+`">
                        `+data[x].name+`
                    </div>
                `
            }

            $(".idol-search-list").html(element);
        }
    }

</script>
@endsection