@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Thêm Ảnh Cho Idol</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <strong>Tên IDOL: {{$idol->name}}</strong>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12 text-center">
                <div class="img-wrap">
                    <img id="thumb_idol" src="{{$idol->thumb}}">
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12" style="text-align: center">
                <input type="file" id="thumb-change-input">
                <button id="change-thumb-action" class="btn btn-success" data-id="{{$idol->id}}" style="padding: 7px 7px;"><i
                    class="fa fa-refresh" aria-hidden="true"></i> Thay Ảnh</button>
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
            <div class="col-md-12">
                <div class="images-block">
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12 text-center">
                <button class="btn btn-success add-photo"><i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm
                    Ảnh</button>
                <button class="btn btn-primary upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    Upload</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    var input_element = 0;
    var input_element_process = 0;

    $(document).ready(function(){
        $(".add-photo").click(function(){
            createInputElement();
        });

        $(document).on('click', '.delete-input', function(){
            console.log("Clock");
            var id_element = $(this).attr("data-id");
            $("#input-item-"+id_element).remove();
        })

        $(document).on('keyup', '.input-images', function(){
            var URL = $(this).val();
            input_element_process = $(this).attr("data-id");
            testPicture(URL);
        });

        $(".upload").click(function(){
            $(this).html(`<img src="{{asset('img/loading.gif')}}" width="15">`);
            $(this).prop("disabled", true);
            collectionData();
        });

        $("#change-thumb-action").click(function(){
            var idol_id = $(this).attr("data-id");
            var file_data = $('#thumb-change-input').prop('files')[0];

            if(file_data){
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('idol_id', idol_id);
                changeThumbForIdol(form_data);
            }else{
                alertify.error("Hãy Chọn File");
            }
        });
    });

    function changeThumbForIdol(form_data){
        $.ajax({
        url: "{{url('admin/person/upload/addimage/changethumb')}}",
        type: 'POST',
        contentType: false,
        processData: false,
        data: form_data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
                alertify.success(res.message);
                $("#thumb_idol").attr('src', res.url);              
           }else{
               console.log(res);
               alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function testPicture(URL){
        var tester=new Image();
        tester.src=URL;
        tester.onload=pictureFound;
        tester.onerror=pictureNotFound;
    }

    function pictureFound(){
        $(".check-valid-"+input_element_process).html(`<i class="fa fa-check-circle" aria-hidden="true" style="color:green"></i>`);
    }

    function pictureNotFound(){
        $(".check-valid-"+input_element_process).html(`<i class="fa fa-times-circle" aria-hidden="true" style="color: red"></i>`);
    }

    function collectionData(){
        var img_link_arr = [];

        $('input[name^="link_img"]').each(function() {
            var link = $(this).val();
            img_link_arr.push(link);
        });

        var data = {
            img_link_arr: img_link_arr,
            idol_id : "{{$idol->id}}"
        }
        uploadImageForIdol(data);
    }

    function uploadImageForIdol(data){
        $.ajax({
        url: "{{url('admin/person/upload/addimage')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           $(".upload").html(`<i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload`); 
           $(".upload").prop("disabled", false);
           if(res.status){
                alertify.success(res.message);
           }else{
                alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function createInputElement(){
        input_element++;
        var element = `
                    <div class="row" id="input-item-`+input_element+`">
                        <div class="col-md-10">
                            <div class="input-item" style="margin-top: 20px">
                                <input type="text" name="link_img[]" data-id="`+input_element+`" class="form-control input-images" placeholder="Nhập link ảnh">
                            </div>
                        </div>
                        <div class="check-valid-`+input_element+`" style="margin-top: 28px;">
                        </div>
                        <div class="col-md-1">
                            <div style="margin-top: 20px;">
                                <button class="btn btn-danger delete-input" style="padding: 5px 10px" data-id="`+input_element+`"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</button>
                            </div>
                        </div>
                    </div>       
        `

        $(".images-block").append(element);
    }
</script>
@endsection