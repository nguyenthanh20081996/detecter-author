@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="#pablo">Thống Kê</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
  </div>
</nav>
<!-- End Navbar -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="card card-stats">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">people</i>
            </div>
            <p class="card-category">Group</p>
            <h3 class="card-title">{{number_format($group_numbers)}}
            </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">people</i>
              Tất cả group trong hệ thống
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">face</i>
            </div>
            <p class="card-category">Person</p>
            <h3 class="card-title">{{number_format($person_numbers)}}</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">face</i> Tất cả idol trong hệ thống
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="card card-stats">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">person_pin</i>
            </div>
            <p class="card-category">User</p>
            <h3 class="card-title">{{number_format($user_numbers)}}
            </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">person_pin</i>
              Tất cả người dùng trong hệ thống
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">folder</i>
            </div>
            <p class="card-category">MB</p>
            <h3 class="card-title">{{number_format($capacity)}}</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">folder</i>Dung lượng ảnh trên {{number_format($image_numbers)}} ảnh
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
        <a href="{{url('admin/collaborator-management')}}" class="btn btn-primary btn-large btn-block">Thêm Công Tác
          Viên</a>
      </div>
      <div class="col-md-4">
        <a href="{{url('admin/collaborator-payment')}}" class="btn btn-primary btn-large btn-block">Thanh Toán</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <a href="{{url('admin/collaborator-blocking')}}" class="btn btn-primary btn-large btn-block">Khóa Công Tác
          Viên</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <a href="{{url('admin/pendent-list')}}" class="btn btn-primary btn-large btn-block">
          <strong class="fa-stack-1x">{{$pendent_number}}</strong><br>
          Phê Duyệt
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <a href="{{url('admin/images-pending-list')}}" class="btn btn-primary btn-large btn-block">
          <strong class="fa-stack-1x">{{$number_images}}</strong><br>
          Phê Duyệt Ảnh Người Dùng
        </a>
      </div>
    </div>
  </div>
</div>
@endsection