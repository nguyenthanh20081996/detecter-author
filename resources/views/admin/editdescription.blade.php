@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Cập Nhật Miêu Tả</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        @if($status == 1)
            <div class="row">
                <div class="col-md-12">
                    <div style="padding: 10px; background-color: green; color: white">
                        <span>Đã cập nhật</span>
                    </div>
                </div>
            </div>
        @elseif($status == 0)
        <div class="row">
                <div class="col-md-12">
                    <div style="padding: 10px; background-color: red; color: white">
                        <span>Cập nhật bị lỗi</span>
                    </div>
                </div>
            </div>
        @endif
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12">
                <strong>Tên: {{$idol->name}}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <strong>Person ID: {{$idol->person_id}}</strong>
            </div>
        </div>
        <form action="{{url('admin/des/update')}}" method="POST">
            {!! csrf_field() !!}
            <div class="row" style="margin-top: 20px">
                <div class="col-md-12">
                    <textarea id="description" name="des">{{$idol->des}}</textarea>
                    <input type="hidden" name="person_id" value="{{$idol->id}}">
                </div>
            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-md-1">
                    <div style="padding: 10px 0px">
                        <span>Quốc Gia</span>
                    </div>
                </div>
                <div class="col-md-11">
                    <select class="form-control" name="country">
                        @foreach ($national as $item)
                            <option @if($item->id == $idol->national_id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                    <div style="padding: 10px 0px">
                        <span>Năm Sinh</span>
                    </div>
                </div>
                <div class="col-md-11">
                    <input type="number" class="form-control" value="{{$idol->birth}}" name="birth_year">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="margin-top: 20px">
                    <button type="submit" class="btn btn-success">Cập Nhật</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script src='{{ asset ('js/tinymce.min.js') }}'></script>
<script src='{{ asset ('js/init-tinymce.js') }}'></script>
<script>
        tinymce.init({
                selector: '#description',
                mode: "textareas",
                height: 300,
            });
</script>
@endsection