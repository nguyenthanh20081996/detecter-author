@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Voting</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Đánh Giá</h4>
                        @for($x = 0; $x < $vote_avg; $x++)
                            <i class="fa fa-star" aria-hidden="true"></i>
                        @endfor
                        @for($x = $vote_avg; $x < 5; $x++)
                            <i class="fa fa-star" aria-hidden="true" style="color:#9c9c9c"></i>
                        @endfor
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Đánh Giá
                                        </th>
                                        <th>
                                            Ghi Chú
                                        </th>
                                        <th>
                                            Ngày Tạo
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($voting as $item)
                                        <tr>
                                            <td>
                                                @for($x = 0; $x < $item->stars; $x++)
                                                    <i class="fa fa-star" aria-hidden="true" style="color:#FFC107"></i>
                                                @endfor
                                                @for($x = $item->stars; $x < 5; $x++)
                                                    <i class="fa fa-star" aria-hidden="true" style="color:#d3d3d3"></i>
                                                @endfor
                                            </td>
                                            <td>{{$item->note}}</td>
                                            <td>{{date('d/m/Y', strtotime($item->created_at))}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 paginate-link">
                {{$voting->links()}}
            </div>
        </div>
    </div>
</div>
@endsection