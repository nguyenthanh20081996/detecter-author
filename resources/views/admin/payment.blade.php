@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Phê duyệt Idol</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <div class="card">
                  <div class="card-header card-header-primary">
                    <h4 class="card-title ">Thanh Toán</h4>
                    <p class="card-category"> Tất cả CTV trong hệ thống</p>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive init-data">
                      <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th>
                                    Tên CTV
                                </th>
                                <th>
                                    User Name
                                </th>
                                <th>
                                    Ngân Hàng
                                </th>
                                <th>
                                    STK
                                </th>
                                <th>
                                    Chủ Tài Khoản
                                </th>
                                <th>
                                    Chi Nhánh
                                </th>
                                <th>
                                    Số Tiền
                                </th>
                                <th>
                                    Thanh Toán
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($list_payment as $item)   
                                <tr id="ctv-{{$item->id}}">
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->user_name}}</td>
                                    <td>{{$item->bank_name}}</td>
                                    <td>{{$item->bank_number}}</td>
                                    <td>{{$item->bank_account_holder}}</td>
                                    <td>{{$item->bank_location}}</td>
                                    <td>{{number_format($item->mustpaid)}}</td>
                                    <td><button class="btn btn-success" id="paid" data-id="{{$item->id}}" style="padding: 5px 5px;">Thanh Toán</button></td>
                                </tr>
                             @endforeach  
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#paid').click(function(){
            var id = $(this).attr('data-id');
            
            var data = {
                user_id: id
            }

            payment(data);
        });
    });

    function payment(data){
        $.ajax({
        url: "{{url('admin/ctv/payment')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           $('#ctv-'+res.id).remove();
           alertify.success(res.message);
        }, 
        error: function(error){ console.log(error); } }); 
    }
</script>
@endsection