<!doctype html>
<html lang="en">

<head>
  <title>Ảnh Idols - Trang quản trị</title>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="shortcut icon" type="image/png" href="{{asset('img/idol.ico')}}" />
  <link rel="stylesheet" type="text/css"
    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS -->
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/alertify.min.css" />
  <!-- Default theme -->
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/default.min.css" />
  <!-- Semantic UI theme -->
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/semantic.min.css" />
  <link href="{{asset('css/material-dashboard.css')}}" rel="stylesheet" />
  <link href="{{asset('css/multi-tab.css')}}" rel="stylesheet" />
  <link href="{{asset('css/custom-checkbox.css')}}" rel="stylesheet" />
  <link href="{{asset('css/dropzone.css')}}" rel="stylesheet" />
</head>

<body>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('img/sidebar-1.jpg')}}">
      <div class="logo">
        <a href="javascript:void(0)" class="simple-text logo-mini">
          Quản Trị Viên
        </a>
        <a href="javascript:void(0)" class="simple-text logo-normal">
          <img src="{{asset('img/earth.gif')}}" alt="logo" width="100">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item" id="dashboard">
            <a class="nav-link" href="{{url('admin/dashboard')}}">
              <i class="material-icons">dashboard</i>
              <p>Thống Kê</p>
            </a>
          </li>
          <li class="nav-item" id="group">
            <a class="nav-link" href="{{url('admin/groups')}}">
              <i class="material-icons">supervisor_account</i>
              <p>Group</p>
            </a>
          </li>
          <li class="nav-item" id="person">
            <a class="nav-link" href="{{url('admin/person')}}">
              <i class="material-icons">face</i>
              <p>Person</p>
            </a>
          </li>
          <li class="nav-item" id="up-person">
            <a class="nav-link" href="{{url('admin/up-person')}}">
              <i class="material-icons">backup</i>
              <p>Up Person</p>
            </a>
          </li>
          <li class="nav-item" id="up-person-with-file">
            <a class="nav-link" href="{{url('admin/up-file')}}">
              <i class="material-icons">
                file_copy
              </i>
              <p>Thêm Ảnh Bằng File</p>
            </a>
          </li>
          <li class="nav-item" id="report-today">
            <a class="nav-link" href="{{url('admin/report-today')}}">
              <i class="material-icons">receipt</i>
              <p>Báo Cáo</p>
            </a>
          </li>
          <li class="nav-item" id="approve">
            <a class="nav-link" href="{{url('admin/approve')}}">
              <i class="material-icons">check_circle</i>
              <p>Phê Duyệt</p>
            </a>
          </li>
          <li class="nav-item" id="voting">
            <a class="nav-link" href="{{url('admin/voting')}}">
              <i class="material-icons">stars</i>
              <p>Đánh Giá</p>
            </a>
          </li>
          <!-- your sidebar here -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      @yield('content')
    </div>
  </div>
  <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/alertify.min.js"></script>
  <script src="{{asset('js/core/jquery.min.js')}}"></script>
  <script src="{{asset('js/core/popper.min.js')}}"></script>
  <script src="{{asset('js/core/bootstrap-material-design.min.js')}}"></script>
  <script src="{{asset('js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <script src="{{asset('js/plugins/moment.min.js')}}"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="{{asset('js/plugins/sweetalert2.js')}}"></script>
  <!-- Forms Validations Plugin -->
  <script src="{{asset('js/plugins/jquery.validate.min.js')}}"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="{{asset('js/plugins/jquery.bootstrap-wizard.js')}}"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="{{asset('js/plugins/bootstrap-selectpicker.js')}}"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="{{asset('js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="{{asset('js/plugins/bootstrap-tagsinput.js')}}"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="{{asset('js/plugins/jasny-bootstrap.min.js')}}"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="{{asset('js/plugins/fullcalendar.min.js')}}"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="{{asset('js/plugins/jquery-jvectormap.js')}}"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{asset('js/plugins/nouislider.min.js')}}"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="{{asset('js/plugins/arrive.min.js')}}"></script>
  <!-- Chartist JS -->
  <script src="{{asset('js/plugins/chartist.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('js/material-dashboard.js?v=2.1.1')}}" type="text/javascript"></script>
  <script src="{{asset('js/dropzone.js')}}" type="text/javascript"></script>
  <script>
    var url = window.location.href;
    if(url.includes('/dashboard')){
      $("#dashboard").addClass('active');
    }else if(url.includes('/groups')){
      $("#group").addClass('active');
    }else if(url.includes('/up-person')){
      $("#up-person").addClass('active');
    }else if(url.includes('/person')){
      $("#person").addClass('active');
    }else if(url.includes('/report-today')){
      $("#report-today").addClass('active');
    }else if(url.includes('/approve')){
      $("#approve").addClass('active');
    }else if(url.includes('/voting')){
      $("#voting").addClass('active');
    }else if(url.includes('/up-file')){
      $("#up-person-with-file").addClass('active');
    }
  </script>

  @yield('script')
</body>

</html>