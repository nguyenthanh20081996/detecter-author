@extends('admin.layout.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @foreach($idol_images as $item)
            <div class="img-select" id="session-{{$item->id}}" style="margin-top: 10px;" id="image-idol-2273">
                <div class="img-select-content">
                        <img src="{{$item->link}}">
                </div>
                <div class="img-select-footer">
                    <input type="checkbox" class="select-handle" value="{{$item->id}}">
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="margin-top: 10px; text-align: center">
            <button class="btn btn-success" id="approve-btn">Phê Duyệt <span class="approve-text">0</span></button>
            <button class="btn btn-danger" id="delete-btn">Xóa <span class="reject-text">0</span></button>
        </div>
    </div>
    <div style="padding: 30px"></div>
</div>
@endsection
@section('script')
<script>

    var select_handle = [];
    var idol_id = {{$idol_id}};

    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };


    $(document).ready(function(){
        $('.select-handle').on('change', function(){
            if($(this).prop("checked")){
                select_handle.push($(this).val());
            }else{
                select_handle.remove($(this).val());
            }

            $('.approve-text').text(select_handle.length);
            $('.reject-text').text(select_handle.length);
        });

        $('#approve-btn').click(function(){
            var data = {
                images: select_handle,
                idol_id: idol_id
            }

            approveHandle(data);
        });

        $('#delete-btn').click(function(){
            var data = {
                images: select_handle
            }

            deleteHandle(data);
        });
    });

    function approveHandle(data){
        $.ajax({
        url: "{{url('admin/pending-image/approve')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
               var data_back = res.data;
               for(var index = 0; index < data_back.length; index++){
                   $('#session-'+data_back[index]).remove();
               } 

               select_handle = [];
               alertify.success(res.message);
               $('.approve-text').text(select_handle.length);
               $('.reject-text').text(select_handle.length);
           }else{
               alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function deleteHandle(data){
        $.ajax({
        url: "{{url('admin/pending-image/delete')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
               var data_back = res.data;
               for(var index = 0; index < data_back.length; index++){
                   $('#session-'+data_back[index]).remove();
               } 

               select_handle = [];
               alertify.success(res.message);
               $('.approve-text').text(select_handle.length);
               $('.reject-text').text(select_handle.length);
           }else{
               alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }
</script>
@endsection