@extends('admin.layout.admin')
@section('content')
<!-- The Modal -->
<div id="myModal" class="modal">
    <div class="modal-content">
        <p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Tạo Group Mới</p>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Group ID</label>
                    <input type="text" id="group-id" class="form-control">
                </div>
            </div>
        </div>
        <div class="clear-10"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Tên Group</label>
                    <input type="text" id="group-name" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button class="btn btn-primary" style="padding: 9px 15px;" id="accept-create">Thêm</button>
                    <button class="btn btn-default" style="padding: 9px 15px;" id="close">Hủy</button>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="#pablo">Group</a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <span class="bmd-form-group">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control searching-group" placeholder="Tìm kiếm group...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </span>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <div class="text-right">
                    <button class="btn btn-success add-group"><i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm Group</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-primary">
                    <h4 class="card-title ">Group</h4>
                    <p class="card-category"> Tất cả group trên hệ thống</p>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive init-data">
                      <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th>
                                    Group ID
                                </th>
                                <th>
                                    Tên
                                </th>
                                <th>
                                    Đã Training
                                </th>
                                <th>
                                    Số Người
                                </th>
                                <th>
                                    Ngày Tạo
                                </th>
                                <th>
                                    Thao Tác
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($group_list as $item)
                                <tr class="item-{{$item->personGroupId}}">
                                    <td>
                                        {{$item->personGroupId}}
                                    </td>
                                    <td class="name-{{$item->personGroupId}}">
                                        {{$item->name}}
                                    </td>
                                    <td class="training-status-{{$item->personGroupId}}">
                                        @if($item->is_training)
                                            <i class="fa fa-check" aria-hidden="true" style="color: green"></i>
                                        @else
                                            <i class="fa fa-times" aria-hidden="true" style="color: red"></i>
                                        @endif
                                    </td>
                                    <td>
                                        {{$item->person_numbers}}
                                    </td>
                                    <td>
                                        {{date('d/m/Y', strtotime($item->created_at))}}
                                    </td>
                                    <td>
                                        <button class="btn btn-success traning-item-{{$item->personGroupId}} training" style="padding: 5px 10px;" @if($item->is_training) disabled @endif data-id="{{$item->personGroupId}}"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Training</button>
                                        <button class="btn btn-primary update-group update-item-{{$item->personGroupId}}" style="padding: 5px 10px;" data-name="{{$item->name}}" data-id="{{$item->personGroupId}}"><i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật</button>
                                        <button class="btn btn-danger delete-group delete-item-{{$item->personGroupId}}" style="padding: 5px 10px;" data-id="{{$item->personGroupId}}"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    <div class="table-responsive search-data" style="display: none">
                        <table class="table">
                          <thead class=" text-primary">
                              <tr>
                                  <th>
                                      Group ID
                                  </th>
                                  <th>
                                      Tên
                                  </th>
                                  <th>
                                      Đã Training
                                  </th>
                                  <th>
                                      Số Người
                                  </th>
                                  <th>
                                      Ngày Tạo
                                  </th>
                                  <th>
                                      Thao Tác
                                  </th>
                              </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
        </div>
    </div>
  </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){

        $(document).on('click','.add-group', function(){
            $("#myModal").fadeIn();
        });

        $(document).on('click', '.training', function(){
            var group_id = $(this).attr("data-id");
            var data = {
                group_id: group_id
            }
            trainingGroup(data);
            console.log(group_id);
        });

        $("#accept-create").click(function(){
            var group_id = $("#group-id").val();
            var group_name = $("#group-name").val();

            $("#accept-create").prop('disabled', true);
            $("#accept-create").html(`<img src="{{asset('img/loading.gif')}}" width="17">`);

            if(!String(group_id).trim() || !String(group_name).trim()){
                alertify.error('Hãy nhập đủ thông tin');
                console.log("Null");
            }else{
                var data = {
                    group_id: group_id,
                    name: group_name
                }
                createGroup(data);
            }   
        });

        $(".searching-group").keyup(function(){
            var searching = $(this).val();
            if(String(searching).trim() != ''){
                var data = {
                    searching: searching
                }

                searchGroup(data);
                $(".search-data").show();
                $(".init-data").hide();
            }else{
                $(".search-data").hide();
                $(".init-data").show();
            }
            
        });

        $(document).on('click', '.update-group', function(){
            var group_id = $(this).attr('data-id');
            var group_name = $(this).attr('data-name');
            
            alertify.prompt( '<i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật Group', 'Tên Group', group_name
            , function(evt, value) { 
                var data = {
                    new_id: value,
                    group_id: group_id
                }

            $(".update-item-"+group_id).prop('disabled', true);
            $(".update-item-"+group_id).html(`<img src="{{asset('img/loading.gif')}}" width="17">`);

            updateGroup(data);
            }
            , function() {});
        });

        $(document).on('click', '.delete-group', function(){
            var group_id = $(this).attr('data-id');

            var message = 'Bạn có chắc chắn xóa ' + '<strong>' + group_id + '</strong>';
            alertify.confirm('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Thông Báo', message, function(){ 
                $(".delete-item-"+group_id).prop('disabled', true);
                $(".delete-item-"+group_id).html(`<img src="{{asset('img/loading.gif')}}" width="17">`);

                var data = {
                    group_id: group_id
                }
                deleteGroup(data);
            }
                , function(){});
        });

        $("#close").click(function(){
            $("#myModal").fadeOut();
        });
    });

    function trainingGroup(data){
        $.ajax({
        url: "{{url('admin/group/training')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            console.log(res);
           if(res.status){
                alertify.success(res.message);
                var group_change = res.group_id;
                $(".training-status-"+group_change).html(`<i class="fa fa-check" aria-hidden="true" style="color: green"></i>`);
                $(".traning-item-"+group_change).prop("disabled", true);
           }else{
                alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function createGroup(data){
        $.ajax({
        url: "{{url('admin/group/create')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
                console.log(res);
                $("#myModal").fadeOut();
                alertify.success(res.message);
                createOneGroup(res);
                $("#accept-create").prop('disabled', false);
                $("#accept-create").html(`Thêm`);
           }else{
                alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function deleteGroup(data){
        $.ajax({
        url: "{{url('admin/group/delete')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
           if(res.status){
                alertify.success(res.message);
                $('.item-'+res.group_id).hide(300);
                $(".delete-item-"+group_id).prop('disabled', false);
                $(".delete-item-"+group_id).html(`<i class="fa fa-trash" aria-hidden="true"></i> Xóa`);
           }else{
                $(".delete-item-"+group_id).prop('disabled', false);
                $(".delete-item-"+group_id).html(`<i class="fa fa-trash" aria-hidden="true"></i> Xóa`);
                alertify.error(res.message);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function updateGroup(data){
        $.ajax({
        url: "{{url('admin/group/update')}}",
        type: 'POST',
        data: data,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            console.log(res);
           if(res.status){
                alertify.success(res.message);
                console.log(res);
                $(".name-"+res.personGroupId).html(res.new_id); 
                $(".update-item-"+res.personGroupId).prop('disabled', false);
                $(".update-item-"+res.personGroupId).html(`<i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật`);
           }else{
                alertify.error(res.message);
                $(".update-item-"+res.personGroupId).prop('disabled', false);
                $(".update-item-"+res.personGroupId).html(`<i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật`);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function searchGroup(data){
        $.ajax({
        url: "{{url('admin/group/search')}}",
        type: 'GET',
        data: data,
        success: function (res) {
           if(res.status){
                createGroupSearching(res.data);
           }else{
               console.log(res);
           }
        }, 
        error: function(error){ console.log(error); } }); 
    }

    function createGroupSearching(data){
        var element = "";
        if(data.length == 0){
            $(".search-data > table > tbody").html(`<strong>Không tìm thấy Group</strong>`);
        }else{
            for(let x = 0; x < data.length; x++){
            element+= `
                                <tr class="item-`+data[x].personGroupId+`">
                                    <td>
                                        `+data[x].personGroupId+`
                                    </td>
                                    <td class="name-`+data[x].personGroupId+`">
                                        `+data[x].name+`
                                    </td>
                                    <td class="training-status-`+data[x].personGroupId+`">
                                        <i class="fa fa-times" aria-hidden="true" style="color: red"></i>
                                    </td>
                                    <td>
                                        0
                                    </td>
                                    <td>
                                        `+data[x].created_at+`
                                    </td>
                                    <td>
                                        <button class="btn btn-success traning-item-`+data[x].personGroupId+` training" style="padding: 5px 10px;" data-id="`+data[x].personGroupId+`"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Training</button>
                                        <button class="btn btn-primary update-group update-item-`+data[x].personGroupId+`" style="padding: 5px 10px;" data-name="`+data[x].name+`" data-id="`+data[x].personGroupId+`"><i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật</button>
                                        <button class="btn btn-danger delete-group delete-item-`+data[x].personGroupId+`" style="padding: 5px 10px;" data-id="`+data[x].personGroupId+`"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</button>
                                    </td>
                                </tr>
        
            `
            }

            $(".search-data > table > tbody").html(element);
        }
    }

    function createOneGroup(data){
        var element = `
                                <tr class="item-`+data.personGroupId+`">
                                    <td>
                                        `+data.personGroupId+`
                                    </td>
                                    <td class="name-`+data.personGroupId+`">
                                        `+data.name+`
                                    </td>
                                    <td class="training-status-`+data[x].personGroupId+`">
                                        <i class="fa fa-times" aria-hidden="true" style="color: red"></i>
                                    </td>
                                    <td>
                                        0
                                    </td>
                                    <td>
                                        `+data.created_at+`
                                    </td>
                                    <td>
                                        <button class="btn btn-success traning-item-`+data.personGroupId+` training" style="padding: 5px 10px;" data-id="`+data[x].personGroupId+`"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Training</button>
                                        <button class="btn btn-primary update-group update-item-`+data.personGroupId+`" update-group" style="padding: 5px 10px;" data-name="`+data.name+`" data-id="`+data.personGroupId+`"><i class="fa fa-pencil" aria-hidden="true"></i> Cập Nhật</button>
                                        <button class="btn btn-danger delete-group delete-item-`+data.personGroupId+`" style="padding: 5px 10px;" data-id="`+data.personGroupId+`"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</button>
                                    </td>
                                </tr>
        
        `
        if ( $('tbody').children().length > 0 ) {
            $('tbody>tr:first-child').before(element);
        }else{
            $('tbody').append(element);
        }

        
    }


</script>
@endsection