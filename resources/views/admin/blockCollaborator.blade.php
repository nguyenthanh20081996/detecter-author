@extends('admin.layout.admin')
@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Quản lý cộng tác viên</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
    </div>
</nav>
<!-- End Navbar -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Danh Sách cộng tác viên</h4>
                        <p class="card-category"> Tổng số cộng tác viên trong hệ thống
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="title-upload">
                                    <span>Chọn cộng tác viên</span>
                                </div>
                            </div>
                            <div class="col-md-9 col-lg-9 col-sm-9 col-xl-9">
                                <span class="bmd-form-group">
                                    <div class="input-group no-border">
                                        <input type="text" value="" class="form-control searching-person"
                                            placeholder="Tìm kiếm cộng tác viên...">
                                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                            <i class="material-icons">search</i>
                                            <div class="ripple-container"></div>
                                        </button>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th>
                                                    Tên Tài Khoản
                                                </th>
                                                <th>
                                                    Tên Đầy Đủ
                                                </th>
                                                <th>
                                                    Số Idol Được Phê Duyệt
                                                </th>
                                                <th>
                                                    Ngày Tham Gia
                                                </th>
                                                <th>
                                                    Khóa
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="idol-search-list">
                                            @include('admin.collaborator_load')
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){

        function fetch_data(data) {
            $.ajax({
                url:"{{url('admin/collaborator/fetch-data')}}",
                type: 'GET',
                data: data,
                success:function(list)
                {
                    $('tbody').html('');
                    $('tbody').html(list);
                },
                error: function(error){ console.log(error); } 
            })
        }

        $(document).on('keyup', '.searching-person', function() {
            var search_value = $(this).val();
            var page = 1;
            var data = {
                searching: search_value,
                page: page
            }
            $('#hidden_page').val(page);
            fetch_data(data);
        });

        $(document).on('click', '.pagination a', function(event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            var search_value = $('.searching-person').val();
            var data = {
                searching: search_value,
                page: page
            }
            $('#hidden_page').val(page);
            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(data);
        });

        $(document).on('click', '.approved', function(event) {
            let collaborator_id = parseInt($(this).attr('data-id'));
            var data = {
                collaborator_id: collaborator_id,
                status: 0
            }
            updateBlockingStatus(data);
        });

        $(document).on('click', '.delete', function(event) {
            let collaborator_id = parseInt($(this).attr('data-id'));
            var data = {
                collaborator_id: collaborator_id,
                status: 1
            }
            updateBlockingStatus(data);
        });

        function updateBlockingStatus(data) {
            $.ajax({
                url: "{{url('admin/collaborator/blocking')}}",
                type: 'POST',
                // contentType: false,
                // processData: false,
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {
                    if(res.status){
                        alertify.success(res.message);
                        changeBlockingStatus(res.id, res.is_blocked);
                    }else{
                        alertify.error(res.error);
                    }
                },
                error: function(error){ console.log(error); }
            });
        }

        function changeBlockingStatus(id, status) {
            if(status == 1) {
                $('#btn-block-'+id).hide();
                $('#btn-unblock-'+id).show();
            } else {
                $('#btn-block-'+id).show();
                $('#btn-unblock-'+id).hide();
            }
        }

    });
</script>
@endsection