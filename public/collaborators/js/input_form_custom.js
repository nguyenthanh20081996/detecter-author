$(document).ready(function() {
    $('input').each(function(index, item) {
        if (this.value) {
            $(this).parent().find('label').addClass('label-active');
        }
    });

    $('input').on('focusin', function() {
        $(this).parent().find('label').addClass('label-active');
    });

    $('input').on('focusout', function() {
        if (!this.value) {
            $(this).parent().find('label').removeClass('label-active');
        }
    });
});