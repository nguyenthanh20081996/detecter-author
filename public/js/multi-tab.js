// Selects clicked tab-nav
// Selects current content tab
// Selects previous active tab
// Selects previous content tab
// Removes classes of previous tabs
// Adds classes for clicked tab
function changeTab(element) {
    var currentTabNav = $(element).closest('.tab-nav');
    var currentContentTab = $('div[data-tab="' + element.children('a').attr('href') + '"]');
    var previousTab = currentTabNav.find('.tab-nav__item.active');
    var previousContentTab = $('div[data-tab="' + previousTab.children('a').attr('href') + '"]');
    previousTab.removeClass('active');
    previousContentTab.removeClass('active');
    element.addClass('active');
    currentContentTab.addClass('active');
}

// Set the slider for the active tabs
// animate parameter defines whether the slider should have an animation or not
function updateBorderBottom(animate) {
    $('.tab-nav__item.active a').each(function(index, element) {
        var activeTab = $(element);
        var bottomBorder = activeTab.closest('.tab-nav').find('.bottom-border');
        var styles = {
            top: (activeTab.position().top + 24),
            width: activeTab.width(),
            left: activeTab.position().left,
            transition: animate ? 'all 0.1s ease' : 'none'
        }
        bottomBorder.css(styles);
    });
}

// Change tab and content tab whenever a tab is clicked
$('.tab-nav__item a').click(function(e) {
    e.preventDefault()
    var element = $(e.target.parentElement);
    changeTab(element);
    updateBorderBottom(true);
});

// Updates slider as the screen resizes
$(window).resize(function() {
    updateBorderBottom(false);
});

// Set the slider for the active tabs
updateBorderBottom(false);