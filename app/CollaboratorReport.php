<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollaboratorReport extends Model
{
    protected $table = 'collaborator_report';
}
