<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdolImage extends Model
{
    protected $table = 'idol_images';
}
