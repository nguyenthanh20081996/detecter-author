<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use App\System;
use Hash;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $maxAttempts = 5; // change to the max attemp you want.
    protected $decayMinutes = 3; // change to the minutes you want.

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function RedirectToProviderFacebook(){
        return Socialite::driver('facebook')->redirect();
    }

    public function RedirectToProviderGoogle(){
        return Socialite::driver('google')->redirect();
    }

    public function HandelProviderGoogleCallback(){
        $user = Socialite::driver('google')->user();
        $auth_id = $user['id'];
        $name = $user['name'];

        $user_checker = User::where('app_id', '=', $auth_id)->first();
        if(empty($user_checker)){
            $search_number_limit = System::where('id', '=', 2)->first()->value_site;
            
            $user = new User;
            $user->app_id = $auth_id;
            $user->name = $name;
            $user->search_numbers = (int)$search_number_limit;
            $user->role = 2;
            $user->save();

            $user_update = User::where('id', '=', $user->id)->first();
            $user_update->password =  Hash::make($user->id.'2ab4aaf60646df13a089aacf1648191e');
            $user_update->save();

            Auth::attempt(['id' => $user->id, 'password' => $user->id.'2ab4aaf60646df13a089aacf1648191e']);
            return redirect('/');
        }else{
            $user = User::where('app_id', '=', $auth_id)->first();
            Auth::attempt(['id' => $user->id, 'password' => $user->id.'2ab4aaf60646df13a089aacf1648191e']);
            return redirect('/');
        }
    }


    public function HandelProviderFacebookCallback(){
        $user = Socialite::driver('facebook')->user();
        $auth_id = $user['id'];
        $name = $user['name'];

        $user_checker = User::where('app_id', '=', $auth_id)->first();
        if(empty($user_checker)){
            $search_number_limit = System::where('id', '=', 2)->first()->value_site;
            
            $user = new User;
            $user->app_id = $auth_id;
            $user->name = $name;
            $user->search_numbers = (int)$search_number_limit;
            $user->role = 2;
            $user->save();

            $user_update = User::where('id', '=', $user->id)->first();
            $user_update->password =  Hash::make($user->id.'2ab4aaf60646df13a089aacf1648191e');
            $user_update->save();

            Auth::attempt(['id' => $user->id, 'password' => $user->id.'2ab4aaf60646df13a089aacf1648191e']);
            return redirect('/');
        }else{
            $user = User::where('app_id', '=', $auth_id)->first();
            Auth::attempt(['id' => $user->id, 'password' => $user->id.'2ab4aaf60646df13a089aacf1648191e']);
            return redirect('/');
        }
    }

    public function CtvLogin(Request $request){
        $credentials = $request->only('user_name', 'password');

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return redirect('collaborators/login')
            ->withErrors(['msg'=> 
                'Bạn đã đăng nhập lỗi quá '.$this->maxAttempts().
                ' lần, thử lại sau '.$this->decayMinutes().' phút']);
        }
        
        if(Auth::attempt($credentials)) {
            if($this->guard()->user()->is_blocked === 0) {
                $this->clearLoginAttempts($request);
                return redirect('collaborators/dashboard');
            } else {
                $this->guard()->logout();
                return redirect('collaborators/login')
                ->withErrors(['msg'=> 'Tài khoản của bạn đã bị khóa!']);
            }
        } else {
            $this->incrementLoginAttempts($request);
            return redirect('collaborators/login')
            ->withErrors(['msg'=>'Tài khoản hoặc mật khẩu không chính xác.']);
        }
    }

    public function CtvLogout(){
        Auth::logout();
        return redirect()->to('collaborators/login');
    }
}
