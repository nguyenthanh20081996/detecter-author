<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use URL;
use App\Idol;
use App\IdolImage;
use App\Vote;
use App\User;
use App\National;
use App\ImageNotify;
use App\IdolNotify;
use Auth;
use DB;

class UserController extends Controller
{
    private $key = '2e5659bee964404ab3625a7322ae3555';
    private $end_point = 'https://eastasia.api.cognitive.microsoft.com/face/v1.0/';
    private $groupId = "idol-detect";

    public function GetSearchPage(Request $request){
        try{
            $top_idol = Idol::where('is_approve', '=', 1)->orderBy('views', 'DESC')->limit(8)->get();
            return view('user.search', [
                'top_idol' => $top_idol
            ]);
        }catch(\Exception $e){
            return view('user.notfound');
        }
    }

    public function GetListIdolsPage(){
        try{
            $total = Idol::count();
            $national = National::all();

            return view('user.listidols',[
                'total' => $total,
                'national' => $national
            ]);
        }catch(\Exception $e){
            return view('user.notfound');
        }
    }

    public function GetTopUserWithImage(Request $request){
        $id = $request->id;

        $image_numbers = IdolImage::where('user_id', '=', $id)->where('is_showing', '=', 1)->count();
        $user_name = User::where('id', '=', $id)->first()->name;

        return view('user.images_list', [
            'id' => $id,
            'total' => $image_numbers,
            'name' => $user_name
        ]);
    }


    public function UserChangeName(Request $request){
        $new_name = $request->name;

        if(Auth::check()){
            if($new_name == null || $new_name == ''){
                return Response::json([
                    'status' => 0,
                    'message' => 'Bạn hãy nhập tên!'
                ]);
            }

            if(strlen($new_name) > 50){
                return Response::json([
                    'status' => 0,
                    'message' => 'Tên tối đa 50 ký tự!'
                ]);
            } 

            $user_id = Auth::user()->id;
            $user = User::where('id', '=', $user_id)->first();
            $user->name = $new_name;
            $user->save();

            return Response::json([
                'status' => 1,
                'new_name' => $new_name,
                'message' => 'Đã cập nhật tên!'
            ]);

        }else{
            return Response::json([
                'status' => 0,
                'message' => 'Bạn cần phải đăng nhập!'
            ]);
        }
    }


    public function GetTopUserWithIdol(Request $request){
        $id = $request->id;

        $name = User::where('id', '=', $id)->first()->name;
        $idols = DB::table('idols')
        ->join('idol_images', 'idols.id', 'idol_images.idol_id')
        ->select('idols.name', 'idols.id', 'idols.birth', 'idols.national_name', 'idols.thumb', DB::raw('count(*) as number_images'))
        ->groupBy('idols.name', 'idols.id', 'idols.birth', 'idols.national_name', 'idols.thumb') 
        ->where('idol_images.is_showing', '=', 1)
        ->where('idols.user_id', '=', $id)
        ->where('idols.is_approve', '=', 1)
        ->paginate(25);

        return view('user.idols_list', [
            'name' => $name,
            'idols' => $idols
        ]);
    }

    public function GetImageByUserId(Request $request){
        try{
            $user_id = $request->user_id;

            $skip = $request->page;
            $skip = $skip * 20;

            $images = DB::table('idols')
            ->join('idol_images', 'idols.id', 'idol_images.idol_id')
            ->select('idols.id', 'idols.name', 'idol_images.link')
            ->where('idol_images.user_id', '=', $user_id)
            ->where('idol_images.is_showing', '=', 1)
            ->skip($skip)->take(20)
            ->orderBy('id', 'DESC')
            ->get();

            return Response::json([
                'status' => 1,
                'data' => $images,
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Không lấy được dữ liệu!'
            ]);
        }
    }

    public function GetTopUserPage(){
        try{
            $user_list_top_images = DB::table('idol_images')
            ->join('users', 'idol_images.user_id', 'users.id')
            ->select('users.id', 'users.name', DB::raw('count(*) as image_numbers'))
            ->where('idol_images.is_showing', '=', 1)
            ->groupBy('users.id', 'users.name')
            ->orderBy('image_numbers', 'DESC')
            ->limit(25)
            ->get();

            $user_list_top_share = DB::table('idols')
            ->join('users', 'idols.user_id', 'users.id')
            ->select('users.id', 'users.name', DB::raw('count(*) as idol_numbers'))
            ->groupBy('users.id', 'users.name')
            ->orderBy('idol_numbers', 'DESC')
            ->limit(25)
            ->get();
            
            return view('user.top_user', [
                'user_list_top_images' => $user_list_top_images,
                'user_list_top_share' => $user_list_top_share
            ]);

        }catch(\Exception $e){
            return view('user.notfound');
        }
    }

    public function GetIdolDetail(Request $request){
        try{
            $idol_id = $request->id;

            $idol = Idol::where('id', '=', $idol_id)->first();
            
            $user_uploaded = DB::table('idol_images')
            ->join('users', 'idol_images.user_id', 'users.id')
            ->select('idol_images.user_id', 'users.name', DB::raw('count(*) as image_numbers'))
            ->where('idol_images.idol_id', '=', $idol_id)
            ->where('idol_images.is_showing', '=', 1)
            ->groupBy('idol_images.user_id', 'users.name')
            ->orderBy('image_numbers', 'DESC')
            ->get();

            $images_with_user = array();
            
            foreach($user_uploaded as $user){
                $images = IdolImage::where('idol_id', '=', $idol_id)->where('user_id', '=', $user->user_id)->where('is_showing', '=', 1)->get();
                array_push($images_with_user, $images);
            }

            if(empty($idol)){
                return view('user.notfound');
            }

            return view('user.idol_detail', [
                'idol' => $idol,
                'images' => $images_with_user,
                'user_mapping' => $user_uploaded
            ]);
        }catch(\Exception $e){
            return view('user.notfound');
        }
    }

    public function GetShareIdolPage(){
        return view('user.uploadidol');
    }

    public function UploadIdolByFile(Request $request){
        try{
            $idol_name = $request->idol_name;
            $description = $request->description;
            $file = $request->file;

            $extension = $file->getClientOriginalExtension();

            if(!Auth::check()){
                return Response::json([
                    'status' => 0,
                    'message' => 'Bạn cần đăng nhập trước'
                ]);
            }

            if($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg'){
                return Response::json([
                    'status' => 0,
                    'message' => 'Không tìm thấy ảnh'
                ]);
            }

            if(empty($idol_name)){
                return Response::json([
                    'status' => 0,
                    'message' => 'Hãy nhập tên của Idol'
                ]);
            }

            $check_idol = Idol::where('name', 'like', '%'.$idol_name.'%')->first();
            if(!empty($check_idol)){
                return Response::json([
                    'status' => 0,
                    'message' => 'Idol đã có trên hệ thống'
                ]);
            }

            if(empty($description)){
                $description = "";
            }

            $user_id = Auth::user()->id;
     
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name = $name . str_random(2) . '.' . $extension;

            $idol = new Idol;
            $idol->name = $idol_name;
            $idol->des = $description;
            $idol->user_id = $user_id;
            $idol->save();

            if (!file_exists(public_path('/photos'). '/'. $idol->id)) {
                mkdir(public_path('/photos'). '/'.$idol->id, 0777, true);
            }

            Image::make($file)
            ->resize(720, null, function ($constraints) {
                $constraints->aspectRatio();
            })
            ->save(public_path('/photos') . '/'.$idol->id.'/'. $resize_name);

            $idol->thumb = URL::to('photos').'/'.$idol->id.'/'.$resize_name;
            $idol->thumb_file_name = public_path('/photos') .'/'.$idol->id.'/' . $resize_name;
            $idol->save();

            return Response::json([
                'status' => 1,
                'message' => 'Cảm ơn bạn đã chia sẻ'
            ]);

        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Không thể tải lên Idol',
                'catch' => $e->getMessage()
            ]);
        }
    }

    public function UploadIdolByUrl(Request $request){

        try{
            $idol_name = $request->idol_name;
            $description = $request->description;
            $file_url = $request->file_url;

            $extension = pathinfo($file_url, PATHINFO_EXTENSION);

            if(!Auth::check()){
                return Response::json([
                    'status' => 0,
                    'message' => 'Bạn cần đăng nhập trước'
                ]);
            }

            if($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg'){
                return Response::json([
                    'status' => 0,
                    'message' => 'Không tìm thấy ảnh'
                ]);
            }

            if(empty($idol_name)){
                return Response::json([
                    'status' => 0,
                    'message' => 'Hãy nhập tên của Idol'
                ]);
            }

            $check_idol = Idol::where('name', 'like', '%'.$idol_name.'%')->first();
            if(!empty($check_idol)){
                return Response::json([
                    'status' => 0,
                    'message' => 'Idol đã có trên hệ thống'
                ]);
            }

            if(empty($description)){
                $description = "";
            }

            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name = $name . str_random(2) . '.' . $extension;

            $file = file_get_contents($file_url);

            $user_id = Auth::user()->id;

            $idol = new Idol;
            $idol->name = $idol_name;
            $idol->des = $description;
            $idol->user_id = $user_id;
            $idol->save();

            if (!file_exists(public_path('/photos'). '/'. $idol->id)) {
                mkdir(public_path('/photos'). '/'.$idol->id, 0777, true);
            }

            Image::make($file)
            ->resize(720, null, function ($constraints) {
                $constraints->aspectRatio();
            })
            ->save(public_path('/photos') . '/'.$idol->id.'/'. $resize_name);

            $idol->thumb = URL::to('photos').'/'.$idol->id.'/'.$resize_name;
            $idol->thumb_file_name = public_path('/photos') . '/'.$idol->id.'/'. $resize_name;
            $idol->save();
            return Response::json([
                'status' => 1,
                'message' => 'Cảm ơn bạn đã chia sẻ'
            ]);

        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Không thể tải lên Idol',
                'catch' => $e->getMessage()
            ]);
        }

    }

    public function Voting(Request $request){
        try{
            $star = $request->star;
            $note = $request->note;

            $vote = new Vote;
            $vote->stars = $star;
            $vote->note = $note;
            $vote->save();

            return Response::json([
                'status' => 1,
                'message' => 'Cảm ơn bạn đã đánh giá'
            ]); 
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Đánh giá bị lỗi'
            ]); 
        }
    }

    public function SearchIdol(Request $request){
        $searching = $request->searching;
        //$idol_list = Idol::select('id', 'name', 'thumb','national_name','birth')->where('is_approve', '=', 1)->where('name', 'like', '%'.$searching.'%')->limit(12)->get();
        $idol_list = DB::table('idols')
        ->leftjoin('idol_images', 'idols.id', 'idol_images.idol_id')
        ->select('idols.id', 'idols.name', 'idols.thumb', 'idols.national_name', 'idols.views', 'idols.likes','idols.birth',DB::raw('count(*) as number_images'))
        ->where('idols.is_approve', '=', 1)
        ->where('idols.name', 'like', '%'.$searching.'%')
        ->where('idol_images.is_showing', '=', 1)
        ->groupBy('idols.id', 'idols.name', 'idols.thumb', 'idols.national_name', 'idols.views', 'idols.likes','idols.birth')
        ->limit(12)
        ->get();

        return Response::json([
            'status' => 1,
            'data' => $idol_list,
            'message' => 'Đã lấy dữ liệu'
        ]);

    }

    public function SearchIdolWithFile(Request $request){
        try{
            if(!Auth::check()){
                return Response::json([
                    'status' => 0,
                    'message' =>  'Bạn cần đăng nhập để thực hiện'
                ]);
            }

            if(Auth::user()->search_numbers == 0){
                return Response::json([
                    'status' => 0,
                    'message' =>  'Bạn đã hết lượt tìm kiếm trong ngày'
                ]);
            }

            $photos = $request->file;
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name = $name . str_random(2) . '.' . $photos->getClientOriginalExtension();
            
            Image::make($photos)
            ->resize(720, null, function ($constraints) {
                $constraints->aspectRatio();
            })
            ->save(public_path('/temp') . '/' . $resize_name);

            $url_to_file = URL::to('temp').'/'.$resize_name;

            $response_result = $this->DetectFaceImage($url_to_file);
            $response_code = $response_result[1];

            if(file_exists(public_path('/temp') . '/' . $resize_name)){
                unlink(public_path('/temp') . '/' . $resize_name);
            }
            
            if($response_code == 200){
                $face_arr = $response_result[0];
                if(count($face_arr) == 0){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Không nhận diện được ảnh này'
                    ]);
                }

                $face_list = array();
                foreach($face_arr as $item){
                    array_push($face_list, $item->faceId);
                }

                $identify_result = $this->IdentifyIdol($face_list);
                $identify_code = $identify_result[1];

                if($identify_code == 200){
                    $user_id = Auth::user()->id;
                    $user = User::where('id', '=', $user_id)->first();
                    $search_numbers = $user->search_numbers;
                    $user->search_numbers = $search_numbers - 1;
                    $user->save();
                    
                    $identify_result_idol = json_decode($identify_result[0]);
                    $candidates = $identify_result_idol[0]->candidates;

                    $idol_detected = array();
                    $confidences = array();

                    foreach($candidates as $item){
                        $person_id = $item->personId;
                        $confidence = round($item->confidence * 100);
                        // Chưa xử lý thêm view được
                        $idol = Idol::select('id','name','thumb','views')->where('person_id', '=', $person_id)->first();
                        $view_numbers = $idol->views;
                        $increase = intval($view_numbers) + 1;
                        $idol->views = $increase;
                        $idol->save();

                        array_push($idol_detected, $idol);
                        array_push($confidences, $confidence);
                    }   

                    return Response::json([
                        'status' => 1,
                        'idol_detected' => $idol_detected,
                        'confidences' => $confidences,
                        'message' =>  'Đã tìm idol'
                    ]);

                }else if($identify_code == 400 || $identify_code == 401 || $identify_code == 403 || $identify_code == 409 || $identify_code == 415 || $identify_code == 429){
                    return Response::json([
                        'status' => 0,
                        'message' =>  $identify_code . 'Identity Error'
                    ]);  
                }

            }else if($response_code == 400 || $response_code == 401 || $response_code == 403 || $response_code == 408 || $response_code == 415 || $response_code == 429){
                return Response::json([
                    'status' => 0,
                    'message' =>  $response_code . 'Detect Error'
                ]);
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' =>  $e->getMessage()
            ]);
        }
    }

    public function SeachIdol(Request $request){
        try{

            if(!Auth::check()){
                return Response::json([
                    'status' => 0,
                    'message' =>  'Bạn cần đăng nhập để thực hiện'
                ]);
            }

            if(Auth::user()->search_numbers == 0){
                return Response::json([
                    'status' => 0,
                    'message' =>  'Bạn đã hết lượt tìm kiếm trong ngày'
                ]);
            }
            
            $img_url = $request->img_url;

            if(trim($img_url) == ''){
                return Response::json([
                    'status' => 0,
                    'message' =>  'Hãy nhập link ảnh'
                ]);
            } 

            $extension = pathinfo($img_url, PATHINFO_EXTENSION);
            if($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg'){
                return Response::json([
                    'status' => 0,
                    'message' => 'Chỉ có thể dùng link ảnh'
                ]);
            }

            $response_result = $this->DetectFaceImage($img_url);
            
            $response_code = $response_result[1];
            
            if($response_code == 200){
                $user_id = Auth::user()->id;
                $user = User::where('id', '=', $user_id)->first();
                $search_numbers = $user->search_numbers;
                $user->search_numbers = $search_numbers - 1;
                $user->save();

                $face_arr = $response_result[0];
                
                if(count($face_arr) == 0){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Không nhận diện được ảnh này'
                    ]);
                }

                $face_list = array();
                foreach($face_arr as $item){
                    array_push($face_list, $item->faceId);
                }

                $identify_result = $this->IdentifyIdol($face_list);
                $identify_code = $identify_result[1];

                if($identify_code == 200){
                    $identify_result_idol = json_decode($identify_result[0]);
                    $candidates = $identify_result_idol[0]->candidates;

                    $idol_detected = array();
                    $confidences = array();

                    foreach($candidates as $item){
                        $person_id = $item->personId;
                        $confidence = round($item->confidence * 100);
                        // Chưa xử lý thêm view được
                        $idol = Idol::select('id','name','thumb', 'views')->where('person_id', '=', $person_id)->first();
                        $view_numbers = $idol->views;
                        $increase = intval($view_numbers) + 1;
                        $idol->views = $increase;
                        $idol->save();

                        array_push($idol_detected, $idol);
                        array_push($confidences, $confidence);
                    }   

                    return Response::json([
                        'status' => 1,
                        'idol_detected' => $idol_detected,
                        'confidences' => $confidences,
                        'message' =>  'Đã tìm idol'
                    ]);
                }else if($identify_code == 400 || $identify_code == 401 || $identify_code == 403 || $identify_code == 409 || $identify_code == 415 || $identify_code == 429){
                    return Response::json([
                        'status' => 0,
                        'message' =>  'Lỗi tìm kiếm'
                    ]);    
                }
            }else if($response_code == 400 || $response_code == 401 || $response_code == 403 || $response_code == 408 || $response_code == 415 || $response_code == 429){
                return Response::json([
                    'status' => 0,
                    'message' =>  'Lỗi tìm kiếm'
                ]);
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' =>  'Lỗi tìm kiếm'
            ]);
        }

    }

    public function DetectFaceImage($url){
        try{
            $body = array("url" => $url);
            $body_string = json_encode($body);

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'/detect',
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $body_string,
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                        "cache-control: no-cache"
                    ),
                )
            );

            $repost_result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close ($ch);

            return [json_decode($repost_result), $httpcode];
        }catch(\Exception $e){
            return 0;
        }
    }

    public function IdentifyIdol($face_id){
        try{
            $body = array(
                "personGroupId" => $this->groupId,
                "faceIds" => $face_id,
                "maxNumOfCandidatesReturned" => 1,
                "confidenceThreshold" => 0.5
            );
            $body_string = json_encode($body);

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'/identify',
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $body_string,
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                        "cache-control: no-cache"
                    ),
                )
            );

            $repost_result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close ($ch);

            return [$repost_result, $httpcode];
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public function GetNewTotal(Request $request){
        try{
            $national = $request->national;
            $from = $request->from;
            $to = $request->to;
            $page = $request->page;

            $idols = Idol::select('id', 'name', 'thumb','national_name','birth')->where('is_approve', '=', 1);
            
            if($national != '-1'){
                $idols->where('national_id', '=', $national);
            }

            if($from != '-1'){
                $from_int = intval($from);
                $from_int = $from_int - 1;
                $idols = $idols->where('birth', '>', $from_int);
            }

            if($to != '-1'){
                $to_int = intval($to);
                $to_int = $to_int + 1;
                $idols = $idols->where('birth', '<', $to_int);
            }

            $idols =  $idols->count();

            return Response::json([
                'status' => 1,
                'total' => $idols,
                'page' => $page,
                'national' => $national,
                'from' => $from,
                'to' => $to,
                'message' => 'Đã lấy dữ liệu'
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Không thể lấy dữ liệu'
            ]);
        }
    }

    public function RemoveNotificationImage(Request $request){
        try{
            $id = $request->id;
            $user_id = Auth::user()->id;
    
            $notify = ImageNotify::where('idol_id', '=', $id)->where('user_id', '=', $user_id)->get();
            foreach($notify as $item){
                $item->delete();
            }
    
            return redirect('idol/thong-tin/'.$id);
        }catch(\Exception $e){
            return view('user.notfound');
        }
    }

    public function RemoveNotificationIdol(Request $request){
        try{
            $id = $request->id;
            $user_id = Auth::user()->id;
    
            $notify = IdolNotify::where('idol_id', '=', $id)->where('user_id', $user_id)->first();
            
            $notify->delete();
    
            return redirect('idol/thong-tin/'.$id);
        }catch(\Exception $e){
            return view('user.notfound');
        }
    }

    public function GetMoreIdol(Request $request){
        try{
            $skip = $request->page;
            $skip = $skip * 12;
            $national = $request->national;
            $from = $request->from;
            $to = $request->to;

            //$idols = Idol::select('id', 'name', 'thumb','national_name','birth')->where('is_approve', '=', 1);

            $idols = DB::table('idols')
            ->leftjoin('idol_images', 'idols.id', 'idol_images.idol_id')
            ->select('idols.id', 'idols.name', 'idols.thumb', 'idols.national_name', 'idols.views', 'idols.likes','idols.birth',DB::raw('count(*) as number_images'))
            ->where('idol_images.is_showing', '=', 1)
            ->where('idols.is_approve', '=', 1);

            if($national != '-1'){
                $idols->where('idols.national_id', '=', $national);
            }

            if($from != '-1'){
                $from_int = intval($from);
                $from_int = $from_int - 1;
                $idols = $idols->where('idols.birth', '>', $from_int);
            }

            if($to != '-1'){
                $to_int = intval($to);
                $to_int = $to_int + 1;
                $idols = $idols->where('idols.birth', '<', $to_int);
            }
            
            $idols =  $idols->groupBy('idols.id', 'idols.name', 'idols.thumb', 'idols.national_name', 'idols.views', 'idols.likes','idols.birth')->skip($skip)->take(12)->orderBy('idols.id', 'DESC')->get();

            return Response::json([
                'status' => 1,
                'data' => $idols,
                'message' => 'Đã lấy dữ liệu'
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Không thể lấy dữ liệu'
            ]);
        }
    }

    public function UploadImageByUser(Request $request){
       try{
            $files = $request->file('files');
            $idol_id = $request->idol_id;

            if(!Auth::check()){
                return Response::json([
                    'status' => 0,
                    'message' => 'Bạn Hãy đăng nhập'
                ]);
            }

            $file_numbers = 0;

            foreach($files as $file){
                $extension = $file->getClientOriginalExtension();
                if($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg'){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Chỉ có thể upload file ảnh!'
                    ]);
                }

                if($file->getSize() > 3145728 ){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Ảnh upload dung lượng tối đa 3MB!'
                    ]);
                }
            }
        
            foreach($files as $file){
                $extension = $file->getClientOriginalExtension();

                $name = sha1(date('YmdHis') . str_random(30));
                $resize_name = $name . str_random(2) . '.' . $extension;

                if (!file_exists(public_path('/photos'). '/'. $idol_id)) {
                    mkdir(public_path('/photos'). '/'.$idol_id, 0777, true);
                }

                Image::make($file)
                ->resize(720, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save(public_path('/photos') . '/'.$idol_id.'/'. $resize_name);

                $new_image = new IdolImage;
                $new_image->link = URL::to('photos').'/'.$idol_id.'/'.$resize_name;
                $new_image->file_name = public_path('/photos') .'/'.$idol_id.'/' . $resize_name;
                $new_image->is_showing = 0;
                $new_image->original_name = basename($file->getClientOriginalName());
                $new_image->idol_id = $idol_id;
                $new_image->user_id = Auth::user()->id;
                $new_image->save();
                
                $file_numbers+=1;
                
            }

            return Response::json([
                'status' => 1,
                'message' => 'Đã tải lên '.$file_numbers.' ảnh!'
            ]);
       }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi máy chủ!'
            ]);
       }
    }

}
