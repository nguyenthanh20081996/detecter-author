<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Group;
use App\Idol;
use App\IdolImage;
use App\Report;
use App\ReportImage;
use App\CollaboratorReport;
use App\System;
use App\Vote;
use App\National;
use App\User;
use App\ImageNotify;
use App\IdolNotify;
use DB;
use File;
use URL;
use Session;
use Hash;
use Validator;
use Carbon\Carbon;

class AdminController extends Controller
{   
    private $key = '2e5659bee964404ab3625a7322ae3555';
    private $end_point = 'https://eastasia.api.cognitive.microsoft.com/face/v1.0/';
    private $photos_path;

    public function __construct(){
        $this->photos_path = public_path('/photos');
    }

    public function UpPersonWithFile(){
        $group_list = Group::all();
        
        return view('admin.uppersonwithfile',[
            'group_list' => $group_list
        ]);
    }

    public function SaveImageDropzoneHandle(Request $request){
        $photos = $request->file('file');
        $idol_id = $request->idol_id;
 
        if (!is_array($photos)) {
            $photos = [$photos];
        }
         
        if (!file_exists($this->photos_path . '/'. $idol_id)) {
            mkdir($this->photos_path . '/'. $idol_id, 0777, true);
        }
 
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();
 
            Image::make($photo)
                ->resize(720, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->photos_path . '/'.$idol_id.'/'. $resize_name);

            $new_image = new IdolImage;
            $new_image->link = URL::to('photos').'/'.$idol_id.'/'.$resize_name;
            $new_image->file_name = $this->photos_path .'/'.$idol_id.'/' . $resize_name;
            $new_image->is_showing = 0;
            $new_image->original_name = basename($photo->getClientOriginalName());
            $new_image->idol_id = $idol_id;
            $new_image->image_approve = 1;
            $new_image->save();      
        }

        $idol = Idol::where('id', '=', $idol_id)->first();
        $idol->is_handle = 0;
        $idol->save();

        return Response::json([
            'message' => 'Image saved Successfully'
        ], 200);
    }

    public function DeleteImageDropzoneHandle(Request $request){
        $filename = $request->id;
        $uploaded_image = IdolImage::where('original_name', basename($filename))->first();
 
        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Sorry file does not exist'], 400);
        }
 
 
        if (file_exists($uploaded_image->file_name)) {
            unlink($uploaded_image->file_name);
        }
  
        if (!empty($uploaded_image)) {
            $uploaded_image->delete();
        }
 
        return Response::json(['message' => 'File successfully delete', 'base' => basename($filename)], 200);
    }

    /**
     * GetPageDashboard
     * 
     * Trả về trang dashboard của Admin
     *
     * @return void
     */
    public function GetPageDashboard(){
        $person_numbers = Idol::count();
        $group_numbers = Group::count();
        $user_numbers = User::count();
        
        $file_size = 0;
        $file_numbers = 0;
        if(file_exists('/home/admin/web/vultr.guest/public_html/public/photos/')){
            foreach( File::allFiles('/home/admin/web/vultr.guest/public_html/public/photos/') as $file)
            {
                $file_size += $file->getSize();
                $file_numbers++;
            }
        }
        //bonus
        $pendent_number = CollaboratorReport::where('status','=',0)->count();
        $number_images_pending = DB::table('users')
        ->join('idol_images', 'users.id', 'idol_images.user_id')
        ->where('users.role', '=', 2)
        ->where('idol_images.image_approve', '=', 0)->count();
        
        return view('admin.dashboard', [
            'person_numbers' => $person_numbers,
            'group_numbers' => $group_numbers,
            'user_numbers' => $user_numbers,
            'capacity' => $file_size * 0.000001,
            'image_numbers' => $file_numbers,
            'pendent_number' => $pendent_number,
            'number_images' => $number_images_pending
        ]);
    }

    public function GetVotingPage(){
        $voting = Vote::orderBy('id', 'DESC')->paginate(500);
        $vote_avg = 0;
        $sum_vote = 0;
        $sum_number = 0;
        foreach($voting as $vote){
            $star = $vote->stars;
            $sum_number++;
            $sum_vote+=$star;
        }
        
        if($sum_number > 0){
            $vote_avg = floor($sum_vote/$sum_number);
        }
        
        return view('admin.voting', [
            'voting' => $voting,            
            'vote_avg' => $vote_avg
        ]);
    }

    public function GetPaymentPage(){
        $list_payment = DB::table('users')
        ->join('collaborator_report', 'users.id', '=' ,'collaborator_report.user_id')
        ->select('users.name', 
        'users.id',
        'users.user_name', 
        'users.bank_name', 
        'users.bank_number', 
        'users.bank_account_holder', 
        'users.bank_location', 
        DB::raw('SUM(collaborator_report.earned) as mustpaid'))
        ->where('users.role', '=', 1)
        ->where('collaborator_report.paid', '=', 0)
        ->groupBy('users.name', 
        'users.id',
        'users.user_name', 
        'users.bank_name', 
        'users.bank_number', 
        'users.bank_account_holder', 
        'users.bank_location')
        ->orderBy('mustpaid', 'DESC')
        ->get();

        return view('admin.payment', [
            'list_payment' => $list_payment
        ]);
    }

    public function CTVPayment(Request $request){
        $user_id = $request->user_id;
        $muspaid = CollaboratorReport::where('user_id', '=', $user_id)->where('paid', '=', 0)->get();

        foreach($muspaid as $item){
            $item->paid = 1;
            $item->save();
        }

        return Response::json([
            'status' => 1,
            'id' => $user_id,
            'message' => 'Đã thanh toán'
        ]);
    }

    public function GetHandlePendingImagePage(Request $request){
        $idol_id = $request->id;
        $idol_images = IdolImage::where('idol_id', '=', $idol_id)->where('image_approve', '=', 0)->get();

        return view('admin.pending_image_list', [
            'idol_images' => $idol_images,
            'idol_id' => $idol_id
        ]);
    }

    public function UpdateDescription(Request $request){
        $person_id = $request->id;
        $idol = Idol::where('id', '=', $person_id)->first();
        $national = National::all();
 
        return view('admin.editdescription', [
            'idol' => $idol,
            'national' => $national,
            'status' => -1
        ]);
    }

    public function ImagePendingPage(){
        $idol_list = DB::table('users')
        ->join('idol_images', 'users.id', 'idol_images.user_id')
        ->join('idols', 'idols.id', 'idol_images.idol_id')
        ->select('idols.id', 'idols.name', DB::raw('count(*) as pending_numbers'))
        ->where('image_approve', '=', 0)
        ->where('users.role', '=', 2)
        ->groupBy('idols.id', 'idols.name')
        ->orderBy('pending_numbers', 'DESC')
        ->paginate(50);

        return view('admin.idol_images_pending', [
            'idol_list' => $idol_list
        ]);
    }

    public function ChangeThumbForIdol(Request $request){
        try{
            $idol_id = $request->idol_id;
            $new_image = $request->file;
            // Đang làm dở
            $idol = Idol::where('id', '=', $idol_id)->first();

            // Xóa file cũ
            $old_image = $idol->thumb_file_name;
            if(file_exists($old_image)){
                unlink($old_image);
            }

            $extension = $new_image->getClientOriginalExtension();
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name = $name . str_random(2) . '.' . $extension;

            if (!file_exists($this->photos_path . '/'. $idol_id)) {
                mkdir($this->photos_path . '/'. $idol_id, 0777, true);
            }

            Image::make($new_image)
            ->resize(720, null, function ($constraints) {
                $constraints->aspectRatio();
            })
            ->save(public_path('/photos') . '/'.$idol_id.'/'. $resize_name);

            $idol->thumb = URL::to('photos').'/'.$idol_id.'/'.$resize_name;
            $idol->thumb_file_name = public_path('/photos') .'/'.$idol_id.'/' . $resize_name;
            $idol->save();

            return Response::json([
                'status' => 1,
                'message' => 'Đã thay ảnh',
                'url' => URL::to('photos').'/'.$idol_id.'/'.$resize_name
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi thay ảnh',
                'error' => $e->getMessage()
            ]);
        }

    }

    public function SetDescription(Request $request){
        try{
            $des = $request->des;
            $person_id = $request->person_id;
            $country_id = $request->country;
            $birth_year = $request->birth_year;

            $national_select = National::where('id', '=', $country_id)->first();

            $idol = Idol::where('id', '=', $person_id)->first();
            $idol->des = $des;
            $idol->national_id = $national_select->id;
            $idol->national_name = $national_select->name;
            $idol->birth = $birth_year;
            $idol->save();

            $national = National::all();

            return view('admin.editdescription', [
                'idol' => $idol,
                'national' => $national,
                'status' => 1
            ]);
        }catch(\Exception $e){
            $idol = Idol::where('id', '=', $person_id)->first();

            return view('admin.editdescription', [
                'idol' => $idol,
                'national' => $national,
                'status' => 0
            ]);
        }
    }

    public function DeleteFace(Request $request){
        $img_id = $request->img_id;
        $idol_id = $request->idol_id;

        $idol = Idol::where('id', '=', $idol_id)->first();

        $idol_image = IdolImage::where('id', '=', $img_id)->first();
        if($idol_image->is_upload == 1){
            $group_id = $idol->group_id;
            $person_id = $idol->person_id;
            $face_id = $idol_image->persistedFaceId;

            $azure_code = $this->DeleteFaceInAzure($group_id, $person_id, $face_id);

            if($azure_code == 200){
                $image_link = $idol_image->file_name;
                if(file_exists($image_link)){
                    unlink($image_link);
                }

                $idol_image->delete();

                return Response::json([
                    'status' => 1,
                    'message' => 'Đã xóa hình ảnh',
                    'img_id' => $img_id
                ]);
            }else if($azure_code == 401){
                return Response::json([
                    'status' => 0,
                    'message' => 'Key không chính xác'
                ]);
            }else if($azure_code == 403){
                return Response::json([
                    'status' => 0,
                    'message' => 'Đã hết tiền'
                ]);
            }else if($azure_code == 404){
                return Response::json([
                    'status' => 0,
                    'message' => 'Person group không tìm thấy'
                ]);
            }else if($azure_code == 409){
                return Response::json([
                    'status' => 0,
                    'message' => 'Group đang Training'
                ]);
            }else if($azure_code == 429){
                return Response::json([
                    'status' => 0,
                    'message' => 'Đạt giới hạn Request'
                ]);
            }

        }else{
            $image_link = $idol_image->file_name;
            if(file_exists($image_link)){
                unlink($image_link);
            }

            $idol_image->delete();
            return Response::json([
                'status' => 1,
                'message' => 'Đã xóa hình ảnh',
                'img_id' => $img_id
            ]);
        }

    }

    public function UpdateImagesShowing(Request $request){
        $showing_list = $request->show;
        $person_id = $request->person_id;

        $images = IdolImage::where('idol_id', '=', $person_id)->get();

        if(!empty($showing_list)){
            foreach($images as $item){
                if($this->SearchInShowingList($showing_list, $item->id)){
                    $item->is_showing = 1;
                    $item->save();
                }else{
                    $item->is_showing = 0;
                    $item->save();
                }
            }
        }
        
        return view('admin.imageshowing', [
            'images' => $images,
            'person_id' => $person_id 
        ]);
    }

    private function SearchInShowingList($arr, $value){
        foreach($arr as $item){
            if($item == $value){
                return 1;
            }
        }

        return 0;
    }

    public function GetPageSetImageShowing(Request $request){
        $person_id = $request->id;

        $group_id = Idol::where('id', '=', $person_id)->first()->group_id;
        $images = IdolImage::where('idol_id', '=', $person_id)->where('image_approve', '=', 1)->get();

        return view('admin.imageshowing', [
            'images' => $images,
            'person_id' => $person_id,
            'group_id' => $group_id 
        ]);
    }

    public function GetLoginPage(){
        if(Session::has('access_token')){
            return redirect('admin/dashboard');
        }else{
            return view('admin.access');
        }
    }

    public function Login(Request $request){
        try{
            $access_token = $request->access_token;
            $token_system = System::where('option_site', '=', 'token_system')->first()->value_site;

            if($access_token == $token_system){
                Session::put('access_token', $token_system);
                return redirect('admin/dashboard');
            }else{
                return redirect('admin/access')->withErrors(['Mã truy cập không chính xác']);
            }
        }catch(\Exception $e){
            return redirect('admin/access')->withErrors(['Xảy ra lỗi đăng nhập']);
        }
        
    }

    public function GetApprovePage(){
        $idol_waiting_numbers = Idol::where('is_approve', '=', 0)->count();
        $idol_waiting_numbers = DB::table('users')
        ->join('idols', 'users.id', 'idols.user_id')
        ->where('is_approve', '=', 0)
        ->where('users.role', '=', 2)->count();
        $idol_waiting = DB::table('users')
        ->join('idols', 'users.id', 'idols.user_id')
        ->where('is_approve', '=', 0)
        ->where('users.role', '=', 2)->limit(500)->get();

        return view('admin.approve', [
            'idol_waiting_numbers' => $idol_waiting_numbers,
            'idol_waiting' => $idol_waiting
        ]);
    }

    public function GetAddImagePage(Request $request){
        $idol_id = $request->id;
        $idol = Idol::where('id', '=', $idol_id)->first();

        return view('admin.addimage', [
            'idol' => $idol
        ]);
    }

    public function UploadAddImage(Request $request){
        try{
            $img_link_arr = $request->img_link_arr;
            $idol_id = $request->idol_id;

            if (!file_exists($this->photos_path . '/'. $idol_id)) {
                mkdir($this->photos_path . '/'. $idol_id, 0777, true);
            }

            foreach($img_link_arr as $item){
                $extension = pathinfo($item, PATHINFO_EXTENSION);
                $file = file_get_contents($item);
        
                $name = sha1(date('YmdHis') . str_random(30));
                $resize_name = $name . str_random(2) . '.' . $extension;
        
                Image::make($file)
                ->resize(720, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->photos_path . '/'.$idol_id.'/'. $resize_name);

                $new_image = new IdolImage;
                $new_image->link = URL::to('photos').'/'.$idol_id.'/'.$resize_name;
                $new_image->file_name = $this->photos_path .'/'.$idol_id.'/' . $resize_name;
                $new_image->is_showing = 0;
                $new_image->idol_id = $idol_id;
                $new_image->image_approve = 1;
                $new_image->save();
            }

            $idol = Idol::where('id', '=', $idol_id)->first();
            $idol->is_handle = 0;
            $idol->save();

            return Response::json([
                'status' => 1,
                'message' => 'Đã Upload'
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Xảy ra lỗi upload'
            ]);
        }
    }

    public function ApproveHandle(Request $request){
        $idol_id = $request->idol_id;

        $idol = Idol::where('id', '=', $idol_id)->first();
        $idol->is_approve = 1;
        $idol->save();

        $idol_notify = new IdolNotify;
        $idol_notify->idol_id = $idol->id;
        $idol_notify->user_id = $idol->user_id;
        $idol_notify->save();

        return Response::json([
            'status' => 1,
            'id' => $idol_id,
            'message' => 'Đã phê duyệt'
        ]);
    }
    
    public function DeleteHandle(Request $request){
        $idol_id = $request->idol_id;

        $idol = Idol::where('id', '=', $idol_id)->first();
        $thumb_file = $idol->thumb_file_name;
        if(file_exists($thumb_file)){
            unlink($thumb_file);
        }
        $idol->delete();

        return Response::json([
            'status' => 1,
            'id' => $idol_id,
            'message' => 'Đã xóa'
        ]);
    }

    public function GetPersonPage(){
        $idols = DB::table('idols')
        ->leftjoin('idol_images', 'idols.id', 'idol_images.idol_id')
        ->select('idols.id','idols.name', 'idols.person_id', 'idols.thumb', 'idols.group_id', DB::raw('count(idol_images.id) as total_face'))
        ->where('is_approve', '=', 1)
        ->groupBy('idols.id','idols.name', 'idols.person_id', 'idols.thumb', 'idols.group_id')
        ->orderBy('id', 'DESC')
        ->paginate(500);

        return view('admin.person', [
            'idols' => $idols
        ]);
    }

    public function UpdatePerson(Request $request){
        $idol_id = $request->idol_id;
        $new_name = $request->new_name;

        try{
            if(!empty($new_name)){
                $idol = Idol::where('id', '=', $idol_id)->first();
                $idol->name = $new_name;
                $idol->save();
    
                return Response::json([
                    'status' => 1,
                    'idol_id' => $idol_id,
                    'name' => $new_name,
                    'message' => 'Đã cập nhật'
                ]);
            }else{
                return Response::json([
                    'status' => 0,
                    'message' => 'Hãy điền tên mới'
                ]);
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi trên máy chủ'
            ]);
        }

    }

    public function SearchPerson(Request $request){
        $searching = $request->searching;
        $idol_list = DB::table('idols')
        ->leftjoin('idol_images', 'idols.id', 'idol_images.idol_id')
        ->select('idols.id','idols.name', 'idols.person_id', 'idols.thumb', 'idols.group_id', DB::raw('count(idol_images.id) as total_face'))
        ->where('idols.name', 'like', '%'.$searching.'%')
        ->groupBy('idols.id','idols.name', 'idols.person_id', 'idols.thumb', 'idols.group_id')
        ->orderBy('id', 'DESC')
        ->limit(20)
        ->get();

        return Response::json([
            'status' => 1,
            'data' => $idol_list,
            'message' => 'Đã lấy dữ liệu'
        ]);
    }

    public function DeletePerson(Request $request){
        try{
            $idol_id = $request->idol_id;

            $idol = Idol::where('id', '=', $idol_id)->first();
            $person_id = $idol->person_id;
            $group_id = $idol->group_id;

            if(empty($group_id)){
                $image_idol = IdolImage::where('idol_id', '=', $idol_id)->get();
                foreach($image_idol as $item){
                    $link_to_delete = $item->file_name;

                    if(file_exists($link_to_delete)){
                        unlink($link_to_delete);
                    }

                    $item->delete();
                }


                if(file_exists($idol->thumb_file_name)){
                    unlink($idol->thumb_file_name);
                }
                $idol->delete();

                return Response::json([
                    'status' => 1,
                    'idol_id' => $idol_id,
                    'message' => 'Đã Xóa Idol'
                ]);

            }else{
                $response_code = $this->DeletePersonInAzure($group_id, $person_id);
                if($response_code == 200){
                    $image_idol = IdolImage::where('idol_id', '=', $idol_id)->get();
                    foreach($image_idol as $item){
                        $link_to_delete = $item->file_name;

                        if(file_exists($link_to_delete)){
                            unlink($link_to_delete);
                        }

                        $item->delete();
                    }


                    if(file_exists($idol->thumb_file_name)){
                        unlink($idol->thumb_file_name);
                    }
                    $idol->delete();


                    return Response::json([
                        'status' => 1,
                        'idol_id' => $idol_id,
                        'message' => 'Đã Xóa Idol'
                    ]);

                }else if($response_code == 401){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Key người dùng không đúng'
                    ]);
                }else if($response_code == 403){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Đã Hết Tiền'
                    ]);
                }else if($response_code == 404){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Không tìm thấy group'
                    ]);    
                }else if($response_code == 409){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Group đang training'
                    ]); 
                }else if($response_code == 429){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Đã đạt giới hạn, hãy đợi 26s'
                    ]); 
                }else if($response_code == 0){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Lỗi trên máy chủ'
                    ]); 
                }
            }

            
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi trên máy chủ'
            ]); 
        }

    }

    public function GetReportToday(){
        $now = Carbon::now();
        $report_list = Report::whereDate('created_at', Carbon::today())->get();
        $report_list_arr = array();
        foreach($report_list as $item){
            $report_image_item = ReportImage::where('report_id', '=', $item->id)->get();
            $report_list_arr = array_add($report_list_arr ,$item->id ,$report_image_item);
        }

        $report_list_add_image = ReportImage::whereDate('created_at', Carbon::today())->where('report_id', '=', -1)->get();

        //$report_image_list = 
        return view('admin.report', [
            'reports' => $report_list,
            'report_image_arr' => $report_list_arr,
            'report_list_add_image' =>$report_list_add_image
        ]);
    }

    // Phần Group =========================================================================================

    public function CreateGroup(Request $request){
        try{
            $group_id = $request->group_id;
            $name = $request->name;
        
            if(!empty($group_id)){
                // Tên không được chứa khoảng trắng
                if(strpos($group_id, ' ') !== false){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Group ID không được chứa khoảng trắng'
                    ]);
                }

                // Kiểm tra trùng tên
                $group_checker = Group::where('personGroupId', '=', $group_id)->first();
                if(!empty($group_checker)){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Group đã tồn tại'
                    ]);
                }

                $reponse_code =  $this->CreateGroupApi($group_id, $name);
    
                if($reponse_code == 200){
                    $new_group = new Group;
                    $new_group->personGroupId = $group_id;
                    $new_group->name = $name;
                    $new_group->save();

                    return Response::json([
                        'status' => 1,
                        'message' => 'Đã thêm group',
                        'personGroupId' => $group_id,
                        'name' => $name,
                        'created_at' => date('d/m/Y', strtotime($new_group->created_at))
                    ]);
                }else if($reponse_code == 400){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Tên group không hợp lệ'
                    ]);
                }else if($reponse_code == 401){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Key người dùng không đúng'
                    ]);
                }else if($reponse_code == 403){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Đã Hết Tiền'
                    ]);
                }else if($reponse_code == 409){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Group đã tồn tại'
                    ]);
                }else if($reponse_code == 415){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Body không hợp lệ'
                    ]);
                }else if($reponse_code == 429){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Đã đạt giới hạn, hãy đợi 26s'
                    ]);
                }else if($reponse_code == 0){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Lỗi gọi API'
                    ]);
                }

            }else{
                return Response::json([
                    'status' => 0,
                    'message' => 'Hãy nhập Group ID'
                ]);
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi trên máy chủ'
            ]);
        }
        
    }

    public function UpdateGroup(Request $request){        
        try{
            $group_id = $request->group_id;
            $new_id = $request->new_id;

            if(!empty($group_id)){

                $reponse_code =  $this->UpdateGroupApi($new_id, $group_id);
                if($reponse_code == 200){
                    $new_group = Group::where('personGroupId', '=', $group_id)->first();
                    $new_group->name = $new_id;
                    $new_group->save();
    
                    return Response::json([
                        'status' => 1,
                        'message' => 'Đã Cập Nhật group',
                        'personGroupId' => $group_id,
                        'new_id' => $new_id
                    ]);
                }else if($reponse_code == 400){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Tên group không hợp lệ'
                    ]);
                }else if($reponse_code == 401){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Key người dùng không đúng'
                    ]);
                }else if($reponse_code == 403){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Đã hết tiền'
                    ]);
                }else if($reponse_code == 404){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Không tìm được Group'
                    ]);
                }else if($reponse_code == 409){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Group đang được Training'
                    ]);
                }else if($reponse_code == 415){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Body không hợp lệ'
                    ]);
                }else if($reponse_code == 429){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Đạt đến giới hạn, hãy đợi 26s'
                    ]);
                }else if($reponse_code == 0){
                    return Response::json([
                        'status' => 0,
                        'message' => 'Lỗi gọi API'
                    ]);
                }
            }else{
                return Response::json([
                    'status' => 0,
                    'message' => 'Hãy nhập Group ID'
                ]);
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi trên máy chủ'
            ]);
        }

    }

    public function DeleteGroup(Request $request){
        try{
            $group_id = $request->group_id;

            $group = Group::where('personGroupId', '=', $group_id)->first()->delete();
            $code = $this->DeleteGroupApi($group_id);

            if($code == 200){
                return Response::json([
                    'status' => 1,
                    'group_id' => $group_id,
                    'message' => 'Đã xóa Group'
                ]);
            }

            if($code == 401){
                return Response::json([
                    'status' => 0,
                    'message' => 'Key không đúng'
                ]);
            }

            if($code == 403){
                return Response::json([
                    'status' => 0,
                    'message' => 'Đã hết tiền'
                ]);
            }

            if($code == 404){
                return Response::json([
                    'status' => 0,
                    'message' => 'Group Không tồn tại'
                ]);
            }

            if($code == 409){
                return Response::json([
                    'status' => 0,
                    'message' => 'Nhóm này đang Traning'
                ]);
            }

            if($code == 429){
                return Response::json([
                    'status' => 0,
                    'message' => 'Đạt giới hạn, hãy đợi sau 26s'
                ]);
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi trên máy chủ'
            ]);
        }
    }

    public function GetPageGroup(){
        $group_list = DB::table('groups')
        ->leftjoin('idols', 'groups.id', 'idols.group_id')
        ->select('groups.id', 'groups.name', 'groups.personGroupId', 'groups.is_training', 'groups.created_at', DB::raw('count(idols.id) as person_numbers'))
        ->groupBy('groups.id', 'groups.name', 'groups.personGroupId', 'groups.is_training', 'groups.created_at')
        ->orderBy('id', 'DESC')
        ->get();

        return view('admin.group', [
            'group_list' => $group_list
        ]); 
    }

    public function SearchGroup(Request $request){
        try{
            $search_value = $request->searching;

            $search_result = DB::table('groups')
            ->leftjoin('persons', 'groups.id', 'persons.group_id')
            ->select('groups.id', 'groups.name', 'groups.personGroupId', 'groups.is_training', 'groups.created_at', DB::raw('count(persons.id) as person_numbers'))
            ->groupBy('groups.id', 'groups.name', 'groups.personGroupId', 'groups.is_training', 'groups.created_at')
            ->where('groups.personGroupId', 'like', '%'.$search_value.'%')
            ->orWhere('groups.name', 'like', '%'.$search_value.'%')
            ->orderBy('id', 'DESC')
            ->limit(20)
            ->get();

            foreach($search_result as $item){
                $item->created_at = date('d/m/Y', strtotime($item->created_at));
            }

            return Response::json([
                'status' => 1,
                'data' => $search_result,
                'message' => 'Searching...'
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Không lấy được dữ liệu'
            ]);
        }

    }

    public function TrainingGroup(Request $request){
        $group_id = $request->group_id;

        try{
            $group_id = $request->group_id;
            $response_code = $this->TrainingGroupInAzure($group_id);

            if($response_code == 202){
                $group = Group::where('personGroupId', '=', $group_id)->first();
                $group->is_training = 1;
                $group->save();

                return Response::json([
                    'status' => 1,
                    'group_id' => $group_id,
                    'message' => 'Đã Training'
                ]);
            }else if($response_code == 401){
                return Response::json([
                    'status' => 0,
                    'message' => 'Key không đúng'
                ]);
            }else if($response_code == 403){
                return Response::json([
                    'status' => 0,
                    'message' => 'Đã hết tiền'
                ]);
            }else if($response_code == 404){
                return Response::json([
                    'status' => 0,
                    'message' => 'Group Không tồn tại'
                ]);
            }else if($response_code == 409){
                return Response::json([
                    'status' => 0,
                    'message' => 'Nhóm này đang Traning'
                ]);
            }else if($response_code == 429){
                return Response::json([
                    'status' => 0,
                    'message' => 'Đạt giới hạn, hãy đợi sau 26s'
                ]);
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'err' => $e->getMessage(),
                'message' => 'Lỗi trên máy chủ'
            ]);
        }

    }

    private function TrainingGroupInAzure($group_id){
        try{
            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$group_id.'/train',
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Length: 0'
                    ),
                )
            );

            curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);

            return $httpcode;
        }catch(\Exception $e){
            return 0;
        }
    }

    private function DeletePersonInAzure($group_id, $person_id){
        try{
            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$group_id.'/persons'.'/'.$person_id,
                    CURLOPT_CUSTOMREQUEST => "DELETE",
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                    ),
                )
            );

            curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);

            return $httpcode;
        }catch(\Exception $e){
            return 0;
        }
    }

    private function CreateGroupApi($group_id, $name){
        try{
            $body = array("name" => $name, "userData" => "Wolf Res");
            $body_string = json_encode($body);

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$group_id,
                    CURLOPT_CUSTOMREQUEST => "PUT",
                    CURLOPT_POSTFIELDS => $body_string,
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                    ),
                )
            );

            $output = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);

            return $httpcode;
        }catch(\Exception $e){
            return 0;
        }
    }

    private function UpdateGroupApi($name, $old_name){
        try{
            $body = array("name" => $name);
            $body_string = json_encode($body);

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$old_name,
                    CURLOPT_CUSTOMREQUEST => "PATCH",
                    CURLOPT_POSTFIELDS => $body_string,
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                        "cache-control: no-cache"
                    ),
                )
            );

            curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);

            return $httpcode;
        }catch(\Exception $e){
            return 0;
        }
    }

    private function DeleteFaceInAzure($group_id, $person_id, $face_id){
        try{

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$group_id.'/'.'persons'.'/'.$person_id.'/'.'persistedFaces'.'/'.$face_id,
                    CURLOPT_CUSTOMREQUEST => "DELETE",
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                        "cache-control: no-cache"
                    ),
                )
            );

            curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);

            return $httpcode;
        }catch(\Exception $e){
            return 0;
        }
    }

    private function DeleteGroupApi($group_id){
        try{

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$group_id,
                    CURLOPT_CUSTOMREQUEST => "DELETE",
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                        "cache-control: no-cache"
                    ),
                )
            );

            curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);

            return $httpcode;
        }catch(\Exception $e){
            return 0;
        }
    }

    public function GetUpPersonPage(){
        $group_list = Group::all();
        
        return view('admin.upload', [
            'group_list' => $group_list
        ]);
    }

    public function UploadIdols(Request $request){
            $idol_arr = $request->idol_arr;
            
            try{
                foreach($idol_arr as $item){
                    $photos = $item['thumb'];
                    $extension = pathinfo($photos, PATHINFO_EXTENSION);
                    $file = file_get_contents($photos);
    
                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name = $name . str_random(2) . '.' . $extension;
    
                    $new_idol = new Idol;
                    $new_idol->name = $item['name'];
                    $new_idol->group_id = $item['group_selected'];
                    $new_idol->is_approve = 1;
                    $new_idol->save();

                    if (!file_exists($this->photos_path . '/'. $new_idol->id)) {
                        mkdir($this->photos_path . '/'. $new_idol->id, 0777, true);
                    }
                       
                    Image::make($file)
                    ->resize(720, null, function ($constraints) {
                        $constraints->aspectRatio();
                    })
                    ->save($this->photos_path . '/'.$new_idol->id.'/'. $resize_name);


                    $new_idol->thumb = URL::to('/photos') .'/'.$new_idol->id.'/'. $resize_name;
                    $new_idol->thumb_file_name = $this->photos_path .'/'.$new_idol->id.'/' . $resize_name;
                    $new_idol->save();

                    if(isset($item['picture_arr'])){
                        $picture_images = $item['picture_arr'];
                        foreach ($picture_images as $pic_item) {
                            $photos = $pic_item;
                            $extension = pathinfo($photos, PATHINFO_EXTENSION);
                            $file = file_get_contents($photos);
                                
                            $name = sha1(date('YmdHis') . str_random(30));
                            $resize_name = $name . str_random(2) . '.' . $extension;
                            
                            Image::make($file)
                            ->resize(720, null, function ($constraints) {
                                $constraints->aspectRatio();
                            })
                            ->save($this->photos_path .'/'.$new_idol->id.'/' . $resize_name);
    
                            $new_picture = new IdolImage;
                            $new_picture->link = URL::to('/photos') .'/'.$new_idol->id.'/' . $resize_name;
                            $new_picture->file_name = $this->photos_path.'/'.$new_idol->id.'/' . $resize_name;
                            $new_picture->idol_id = $new_idol->id;
                            $new_picture->image_approve = 1;
                            $new_picture->save();
                        }
                    }
                }
    
                return Response::json([
                    'status' => 1,
                    'message' => 'Đã Upload' 
                ]);
            }catch(\Exception $e){
                return Response::json([
                    'status' => 0,
                    'message' => 'Xảy ra lỗi trên máy chủ',
                    'error' => $e->getMessage()
                ]);
            }
    }

    public function AddCollaborator(Request $request){
        $name = $request->input('name');
        $pass = $request->input('pass');
        $user_name = $request->input('user_name');

        $pass_hash = Hash::make($pass);
        //dd($request);
        $user = new User;
        $user->name = $name;
        $user->user_name = $user_name;
        $user->role = 1;
        $user->password = $pass_hash;
        $user->pass = $pass;
        $user->save();
        return view('admin.collaborator');
    }

    public function GetCollaborator(){
        return view('admin.collaborator');
    }

    public function AcceptImageUser(Request $request){
        try{
            $images = $request->images;
            $idol_id = $request->idol_id;

            foreach($images as $item){
                $image = IdolImage::where('id', '=', $item)->first();
                $image->image_approve = 1;
                $image->save();
                
                $image_notify = new ImageNotify;
                $image_notify->user_id = $image->user_id;
                $image_notify->image_id = $item;
                $image_notify->idol_id = $idol_id;
                $image_notify->save();
            }

            return Response::json([
                'status' => 1,
                'message' => 'Đã phê duyệt!',
                'data' => $images,
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi máy chủ!',
                'error' => $e->getMessage()
            ]);
        }
    }

    public function DeleteImageUser(Request $request){
        try{
            $images = $request->images;
            foreach($images as $item){
                $image = IdolImage::where('id', '=', $item)->first();
                if(file_exists($image->file_name)) {
                    unlink($image->file_name);
                }
                
                $image->delete();
            }

            return Response::json([
                'status' => 1,
                'message' => 'Đã xóa ảnh!',
                'data' => $images,
            ]);

        }catch(\Exception $e){
            return Response::json([
                'status' => 0,
                'message' => 'Lỗi máy chủ!',
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * getBlockCollaboratorPage
     * 
     * trả về trang khóa cộng tác viên
     *
     * @return void
     */
    public function getBlockCollaboratorPage(Request $request) {
        $records = DB::table('users')->leftJoin('idols','users.id','=','idols.user_id')
                    ->selectRaw('users.id,users.name,users.user_name,users.role,users.created_at,
                            users.is_blocked,COUNT(CASE idols.is_approve WHEN 1 THEN 1 ELSE NULL END) AS idol_numbers')
                    ->where('users.role', '=', 1)
                    ->groupBy('users.id','users.name','users.user_name',
                                'users.role','users.created_at','users.is_blocked')
                    ->orderBy('users.id','DESC')
                    ->paginate(5);
        return view('admin.blockCollaborator', compact('records'));
    }

    /**
     * searchCollaborator
     *
     * Tìm kiếm cộng tác viên bằng tên
     * 
     * @param  mixed $request
     *
     * @return json
     */
    public function searchCollaborator(Request $request){
        $searching = $request->searching;
        if ($request->ajax()) {
            $records = DB::table('users')
            ->leftJoin('idols','users.id','=','idols.user_id')
            ->selectRaw('users.id,users.name,users.user_name,users.role,users.created_at,
                    users.is_blocked,COUNT(CASE idols.is_approve WHEN 1 THEN 1 ELSE NULL END) AS idol_numbers')
            ->groupBy('users.id','users.name','users.user_name',
                        'users.role','users.created_at','users.is_blocked')
            ->orderBy('users.id','DESC')
            ->where('users.role', '=', 1)
            ->where('users.name', 'like', '%'.$searching.'%')
            ->orWhere('users.user_name', 'like', '%'.$searching.'%')
            ->paginate(5);
            return view('admin.collaborator_load', compact('records'))->render();
        } else {
            return Response::json([
                'status' => 0,
                'error' => 'Không lấy được dữ liệu'
            ]);
        } 
    }

    /**
     * updateBlockingStatus
     * 
     * Cập nhật trạng thái khóa/mở khóa cho tài khoản của CTV
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function updateBlockingStatus(Request $request) {
        $collaborator_id = $request->collaborator_id;
        $is_blocked = $request->status;
        $user = User::where('id','=',$collaborator_id)->first();
        if(!empty($user)) {
            $user->is_blocked = $is_blocked;
            $user->save();

            return Response::json([
                'status' => 1,
                'id' => $user->id,
                'is_blocked' => $user->is_blocked,
                'message' => 'Cập nhật thành công!'
            ]);
        } else {
            return Response::json([
                'status' => 0,
                'error' => 'Cập nhật thất bại!'
            ]);
        }
    }

    /**
     * getListPendentIdolPage
     *
     * trả về trang hiển thị danh sách Idol chờ phê duyệt
     * 
     * @return void
     */
    public function getListPendentIdolPage(Request $request) {
        $queue = DB::table('users')
                ->join('collaborator_report','users.id', '=', 'collaborator_report.user_id')
                ->select('users.user_name','collaborator_report.*')
                ->orderBy("id", "desc")
                ->where("status", "=", 0)
                ->paginate(50);
        if($request->ajax()) {
            return view('admin.idol_load', compact('queue'))->render();
        }
        return view('admin.pendent_list', [
            'queue' => $queue
        ]);
    }

    /**
     * getApproveIdolPage
     * 
     * trả về trang phê duyệt một Idol bằng ID
     *
     * @param  mixed $id
     *
     * @return void
     */
    public function getApproveIdolPage($id) {
        $idol = Idol::where('id','=',$id)->first();
        $idolImages = IdolImage::where('idol_id','=',$id)->get();
        $national = National::all();
        return view('admin.idol_upload_detail', [
            'idol' => $idol,
            'idolImages' => $idolImages,
            'national' => $national
        ]);
    }

    /**
     * deleteIdolImage
     * 
     * Xóa ảnh tranining của Idol
     *
     * @param  mixed $id
     *
     * @return json
     */
    public function deleteIdolImage(Request $request) {
        $id = $request->id;
        $idolImage = IdolImage::where('id','=',$id)->first();
        if(!empty($idolImage)) {
            if(file_exists($idolImage->file_name)) {
                unlink($idolImage->file_name);
            }
            $idolImage->delete();
            return Response::json([
                'status' => 1,
                'id' => $id,
                'message' => 'Xóa ảnh thành công'
            ]);
        } else {
            return Response::json([
                'status' => 0,
                'error' => 'File ảnh không tồn tại!'
            ]);
        }
    }

    /**
     * approveIdol
     * 
     * Phê duyệt Idol cho CTV
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function approveIdol(Request $request) {
        $validate = Validator::make(
            $request->all(),
            [
            'idol_name' => 'required|string|max:200|unique:idols,name,'.$request->idol_id,
            'idol_country' => 'required|numeric',
            'idol_birth' => 'required|digits:4',
            'idol_description' => 'required',
            'idol_thumb'=>'nullable|image|mimes:jpeg,png,jpg|max:2048'
            ],
            [
            'required' => ':attribute không được bỏ trống',
            'unique' => ':attribute đã tồn tại',
            'digits' => ':attribute phải là chuỗi 4 chữ số',
            'max' => ':attribute tối đa có :max kí tự',
            'string' => ':attribute phải là định dạng chuỗi kí tự',
            'numeric' => 'Chưa lựa chọn :attribute',
            'image' => 'Phải là định dạng ảnh',
            'mimes' => 'Chỉ nhận định dạng ảnh jpeg, png, jpg'
            ],
            [
            'idol_name' => 'tên Idol',
            'idol_country' => 'quốc gia',
            'idol_birth' => 'năm sinh',
            'idol_description' => 'thông tin Idol',
            'idol_thumb' => 'file ảnh'
            ]
        );

        if($validate->fails()){
            return Response::json([
                'status' => 0,
                'errors' => $validate->errors()->all()
            ]);
        }

        $idol_id = $request->idol_id;
        $idol_name = $request->idol_name;
        $idol_national = National::where('id', '=', $request->idol_country)->first();
        $idol_birth = $request->idol_birth;
        $idol_detail = $request->idol_description;
        $image_display_list = explode(',',$request->get('idol_images'));

        try {
            DB::beginTransaction();
            $idol = Idol::where("id","=",$idol_id)->first();
            // Upload ảnh diện
            if (!empty($request->idol_thumb)) {
                $image_file = $request->idol_thumb;
                if (File::exists($idol->thumb_file_name))
                    File::delete($idol->thumb_file_name);
                $extension = $image_file->getClientOriginalExtension();
                $image_info = $this->uploadImage($image_file, $extension, $idol_id);
                $idol->thumb = $image_info['hostPath'];
                $idol->thumb_file_name = $image_info['serverPath'];
            }

            $idol->name = $idol_name;
            $idol->des = $idol_detail;
            $idol->birth = $idol_birth;
            $idol->national_id = $idol_national->id;
            $idol->national_name = $idol_national->name;
            $idol->is_approve = 1;
            $idol->save();

            $idolImages = IdolImage::where('idol_id','=',$idol_id)->get();
            foreach($idolImages as $idolImage) {
                $is_showing = 0;
                for($i=0; $i<count($image_display_list); $i++) {
                    if($idolImage->id == $image_display_list[$i]) {
                        $is_showing = 1;
                        break;
                    }
                }
                $idolImage->is_showing = $is_showing;
                $idolImage->image_approve = 1;
                $idolImage->save();
            }

            $user_id =  $idol->user_id;
            $salary = User::where('id', '=', $user_id)->first()->salary;


            $collaboratorReport = CollaboratorReport::where('idol_id','=',$idol_id)->first();
            $collaboratorReport->status = 1;
            $collaboratorReport->idol_name = $idol->name;
            $collaboratorReport->earned = $salary;
            $collaboratorReport->save();
            DB::commit();

            return Response::json([
                'status' => 1,
                'message' => 'Phê duyệt Idol thành công!',
                'idol_id' => $idol->id
            ]);
        } catch(Exception $ex) {
            DB::rollback();
            return Response::json([
                'status' => 0,
                'errors' => ['Phê duyệt Idol thất bại!'],
                'message' => $ex.getMessage()
            ]);
        }
    }

    /**
     * rejectIdol
     * 
     * Từ chối Idol của CTV
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function rejectIdol(Request $request) {
        $validate = Validator::make(
            $request->all(),
            [
            'reject_description' => 'required'
            ],
            [
            'required' => ':attribute không được bỏ trống'
            ],
            [
            'reject_description' => 'lý do từ chối'
            ]
        );

        if($validate->fails()){
            return Response::json([
                'status' => 0,
                'errors' => $validate->errors()->all()
            ]);
        }

        $idol_id = $request->idol_id;
        $reject_description = $request->reject_description;

        try {
            DB::beginTransaction();
            $idol = Idol::where("id","=",$idol_id)->first();
            $idol->is_approve = 0;
            $idol->save();

            $collaboratorReport = CollaboratorReport::where('idol_id','=',$idol_id)->first();
            $collaboratorReport->status = 2;
            $collaboratorReport->note = $reject_description;
            $collaboratorReport->earned = 0;
            $collaboratorReport->save();
            DB::commit();

            return Response::json([
                'status' => 1,
                'message' => 'Từ chối Idol thành công!',
                'idol_id' => $idol->id
            ]);
        } catch(Exception $ex) {
            DB::rollback();
            return Response::json([
                'status' => 0,
                'errors' => ['Từ chối Idol thất bại!'],
                'message' => $ex.getMessage()
            ]);
        }
    }

    /**
     * uploadImage
     * 
     * Upload ảnh lên DB
     *
     * @param  mixed $resource
     * @param  mixed $extension
     *
     * @return json
     */
    private function uploadImage($resource, $extension, $idol_id){
        $name = sha1(date('YmdHis') . str_random(30));
        $resize_name = $name . str_random(2) . '.' . $extension;

        $file = file_get_contents($resource);

        if (!file_exists($this->photos_path . '/'. $idol_id)) {
            mkdir($this->photos_path . '/'. $idol_id, 0777, true);
        }

        Image::make($file)
        ->resize(720, null, function ($constraints) {
            $constraints->aspectRatio();
        })
        ->save($this->photos_path . '/'.$idol_id.'/'. $resize_name); 
        $host_path = URL::to('photos').'/'.$idol_id.'/'.$resize_name;
        $server_path = $this->photos_path .'/'.$idol_id. '/' . $resize_name;
        return [
            'status' => 1,
            'message' => 'OK',
            'hostPath' => $host_path,
            'serverPath'=> $server_path
        ];
    }

}

