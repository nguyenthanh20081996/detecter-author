<?php

namespace App\Http\Controllers;
use App\Idol;
use Illuminate\Http\Request;
use App\National;
use Auth;
use URL;
use Redirect;
use Validator;

use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Pagination\LengthAwarePaginator;

use App\IdolImage;
use App\Vote;
use App\User;
use App\CollaboratorReport;

class CollaboratorController extends Controller
{
    protected $report_per_page = 5;

    private $photos_path;

    public function __construct(){
        $this->photos_path = public_path('photos');
    }

    public function GetDashBoardPage(Request $request){
        $user_id = Auth::user()->id;
        $salary = Auth::user()->salary;
        $number_pending = CollaboratorReport::where('user_id', '=', $user_id)->where('status', '=', 0)->count();
        $number_approve = CollaboratorReport::where('user_id', '=', $user_id)->where('status', '=', 1)->count();
        $number_reject = CollaboratorReport::where('user_id', '=', $user_id)->where('status', '=', 2)->count();
        // Tạm thời set mỗi idol được phê duyệt là 2000 đồng
        $payment = $number_approve * $salary;
        $history_list = CollaboratorReport::where('user_id', '=', $user_id)->where('paid', '=', 0)->get();

        //tạo infinite scroll
        $reports = CollaboratorReport::orderBy("id", "desc")->where("user_id", "=", $user_id)
            ->paginate($this->report_per_page);
        if($request->ajax()) {
            $view = view('collaborators.data', compact('reports'))->render();
            if(count($reports) > 0) {
                return Response::json([
                    'status' => 1,
                    'message' => 'Tải thành công!',
                    'html' => $view
                ]);
            } else {
                return Response::json([
                    'status' => 0,
                    'warning' => 'Không còn báo cáo nào nữa!',
                ]);
            }
        }
        //kết thúc

        return view('collaborators.dashboard', [
            'number_pending' => $number_pending,
            'number_approve' => $number_approve,
            'number_reject' => $number_reject,
            'payment' => $payment,
            'history_list' => $history_list,
            'reports' => $reports
        ]);
    }

    /**
     * getUploadPage
     *
     * Trả về trang upload ảnh để cập nhật Idol
     * 
     * @return void
     */
    public function getUploadPage($id = null){
        $idol_national= National::all();
        if($id) {
            $idol = Idol::where("id","=",$id)->first();
            return view('collaborators.upload_image', [
                'id' => $id,
                'nationals' => $idol_national,
                'idol_name' => $idol->name,
                'idol_birth' => $idol->birth,
                'idol_national' => $idol->national_id,
                'idol_description' => $idol->des,
                'idol_image' => $idol->thumb_file_name
            ]);
        }
        return view('collaborators.upload_image', [
            'nationals' => $idol_national
        ]);
    }

    /**
     * uploadIdolByFile
     *
     * Gửi thông tin và ảnh đại diện của Idol bằng File
     * 
     * @param  mixed $request
     *
     * @return json
     */
    public function uploadIdolByFile(Request $request) {
        $validate = Validator::make(
            $request->all(),
            [
            'idol_name' => 'required|string|max:200|unique:idols,name,'.$request->idol_id,
            'idol_national' => 'required|numeric',
            'idol_birth' => 'required|digits:4',
            'idol_detail' => 'required',
            'file_data'=>'nullable|image|mimes:jpeg,png,jpg|max:2048'
            ],
            [
            'required' => ':attribute không được bỏ trống',
            'unique' => ':attribute đã tồn tại',
            'digits' => ':attribute phải là chuỗi 4 chữ số',
            'max' => ':attribute tối đa có :max kí tự',
            'string' => ':attribute phải là định dạng chuỗi kí tự',
            'numeric' => 'Chưa lựa chọn :attribute',
            'image' => 'Phải là định dạng ảnh',
            'mimes' => 'Chỉ nhận định dạng ảnh jpeg, png, jpg'
            ],
            [
            'idol_name' => 'tên Idol',
            'idol_national' => 'quốc gia',
            'idol_birth' => 'năm sinh',
            'idol_detail' => 'thông tin Idol',
            'file_data' => 'file ảnh'
            ]
        );

        if($validate->fails()){
            return Response::json([
                'status' => 0,
                'errors' => $validate->errors()->all()
            ]);
        }

        $idol_id = $request->idol_id;
        $idol_name = $request->idol_name;
        $idol_national = National::where('id', '=', $request->idol_national)->first();
        $idol_birth = $request->idol_birth;
        $idol_detail = $request->idol_detail;

        try {
            DB::beginTransaction();
            if(isset($idol_id) && !empty($idol_id)) {
                $collaboratorReport = CollaboratorReport::where('idol_id','=',$idol_id)->delete();
                $idol = Idol::where("id","=",$idol_id)->first();
                if (!empty($request->file_data)) {
                    $image_file = $request->file_data;
                    if (File::exists($idol->thumb_file_name))
                        File::delete($idol->thumb_file_name);
                    $extension = $image_file->getClientOriginalExtension();
                    $image_info = $this->uploadImage($image_file, $extension, $idol_id);
                    $idol->thumb = $image_info['hostPath'];
                    $idol->thumb_file_name = $image_info['serverPath'];
                }
            } else {
                if(empty($request->file_data)) {
                    return Response::json([
                        'status' => 0,
                        'errors' => ['file ảnh không được để trống!']
                    ]); 
                } 
                $idol = new Idol;
            }
            
            
            $idol->name = $idol_name;
            $idol->des = $idol_detail;
            $idol->group_id = 'idol-detect';
            $idol->birth = $idol_birth;
            $idol->national_id = $idol_national->id;
            $idol->national_name = $idol_national->name;
            $idol->user_id = Auth::user()->id;
            $idol->save();

            $image_file = $request->file_data;
            $extension = $image_file->getClientOriginalExtension();
            $image_info = $this->uploadImage($image_file, $extension, $idol->id);
            $idol->thumb = $image_info['hostPath'];
            $idol->thumb_file_name = $image_info['serverPath'];
            $idol->save();
           

            $collaboratorReport = new CollaboratorReport;
            $collaboratorReport->user_id = Auth::user()->id;
            $collaboratorReport->idol_id = $idol->id;
            $collaboratorReport->idol_name = $idol->name;
            $collaboratorReport->earned = 0;
            $collaboratorReport->save();
            DB::commit();

            return Response::json([
                'status' => 1,
                'message' => 'Tải thông tin Idol thành công!',
                'idol_id' => $idol->id
            ]);
        } catch(Exception $ex) {
            DB::rollback();
            return Response::json([
                'status' => 0,
                'errors' => ['Cập nhật Idol thất bại!'],
                'message' => $ex.getMessage()
            ]);
        }
        
    }

    /**
     * uploadIdolByUrl
     *
     * Gửi thông tin và ảnh đại diện của Idol bằng URL
     * 
     * @param  mixed $request
     *
     * @return json
     */
    public function uploadIdolByUrl(Request $request) {
        $validate = Validator::make(
            $request->all(),
            [
            'idol_name' => 'required|string|max:200|unique:idols,name,'.$request->idol_id,
            'idol_national' => 'required|numeric',
            'idol_birth' => 'required|digits:4',
            'idol_detail' => 'required',
            'file_url'=> 'nullable|string|max:200'
            ],
            [
            'required' => ':attribute không được bỏ trống',
            'unique' => ':attribute đã tồn tại',
            'digits' => ':attribute phải là chuỗi 4 chữ số',
            'max' => ':attribute tối đa có :max kí tự',
            'string' => ':attribute phải là định dạng chuỗi kí tự',
            'numeric' => 'Chưa lựa chọn :attribute'
            ],
            [
            'idol_name' => 'tên Idol',
            'idol_national' => 'quốc gia',
            'idol_birth' => 'năm sinh',
            'idol_detail' => 'thông tin Idol',
            'file_url' => 'URL ảnh'
            ]
        );

        if($validate->fails()){
            return Response::json([
                'status' => 0,
                'errors' => $validate->errors()->all()
            ]);
        }

        $idol_id = $request->idol_id;
        $idol_name = $request->idol_name;
        $idol_national = National::where('id', '=', $request->idol_national)->first();
        $idol_birth = $request->idol_birth;
        $idol_detail = $request->idol_detail;

        try {
            DB::beginTransaction();
            if(isset($idol_id) && !empty($idol_id)) {
                $collaboratorReport = CollaboratorReport::where('idol_id','=',$idol_id)->delete();
                $idol = Idol::where("id","=",$idol_id)->first();
                if (!empty($request->file_url)) {
                    $image_url = $request->file_url;
                    if (File::exists($idol->thumb_file_name))
                        File::delete($idol->thumb_file_name);
                    $extension = pathinfo($image_url, PATHINFO_EXTENSION);
                    $image_info = $this->uploadImage($image_url, $extension, $idol_id);
                    $idol->thumb = $image_info['hostPath'];
                    $idol->thumb_file_name = $image_info['serverPath'];
                }
            } else {
                if(!empty($request->file_url)) {
                    $idol = new Idol;
                    $image_url = $request->file_url;
                    $extension = pathinfo($image_url, PATHINFO_EXTENSION);
                    $image_info = $this->uploadImage($image_url, $extension, $idol_id);
                    $idol->thumb = $image_info['hostPath'];
                    $idol->thumb_file_name = $image_info['serverPath'];
                } else {
                    return Response::json([
                        'status' => 0,
                        'errors' => ['URL ảnh không được để trống!']
                    ]);
                }
            }
            
            $idol->name = $idol_name;
            $idol->des = $idol_detail;
            $idol->group_id = 'idol-detect';
            $idol->birth = $idol_birth;
            $idol->national_id = $idol_national->id;
            $idol->national_name = $idol_national->name;
            $idol->user_id = Auth::user()->id;
            $idol->save();
            //can xoa het anh
            $collaboratorReport = CollaboratorReport::where('idol_id','=',$idol_id)->delete();
            //
            $collaboratorReport = new CollaboratorReport;
            $collaboratorReport->user_id = Auth::user()->id;
            $collaboratorReport->idol_id = $idol->id;
            $collaboratorReport->idol_name = $idol->name;
            $collaboratorReport->earned = 0;
            // $collaboratorReport->earned = Auth::user()->salary;
            $collaboratorReport->save();
            DB::commit();

            return Response::json([
                'status' => 1,
                'message' => 'Tải thông tin Idol thành công!',
                'idol_id' => $idol->id
            ]);
        } catch(Exception $ex) {
            DB::rollback();
            return Response::json([
                'status' => 0,
                'errors' => ['Cập nhật Idol thất bại!'],
                'message' => $ex.getMessage()
            ]);
        }
    }

    /**
     * saveImageDropzoneHandle
     * 
     * Lưu ảnh qua DropzoneJs
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function saveImageDropzoneHandle(Request $request){
        $photos = $request->file('file');
        $idol_id = $request->idol_id;

        $user_id = 0;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }

        $report = CollaboratorReport::where('idol_id','=',$idol_id)->where('user_id', '=', $user_id)->first();
        if($report->status == 2){
            $report->status = 0;
            $report->save();
        }

        try {
            DB::beginTransaction();
            for ($i = 0; $i < count($photos); $i++) {
                $photo = $photos[$i];
                $extension = $photo->getClientOriginalExtension();
                $image_info = $this->uploadImage($photo, $extension, $idol_id);
                
                $new_image = new IdolImage;
                $new_image->link = $image_info['hostPath'];
                $new_image->idol_id = $idol_id;
                $new_image->file_name = $image_info['serverPath'];
                $new_image->user_id = $user_id;
                $new_image->original_name = basename($photo->getClientOriginalName());
                $new_image->save();
            }
            DB::commit();
            return Response::json([
                'status' => 1,
                'message' => ['Cập nhật Idol thành công!']
            ]);
        } catch(Exception $ex) {
            DB::rollback();
            return Response::json([
                'status' => 0,
                'errors' => ['Cập nhật Idol thất bại!'],
                'message' => $ex.getMessage()
            ]);
        }
    }

    /**
     * removeImageDropzoneHandle
     * 
     * Xóa ảnh qua DropzoneJs
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function removeImageDropzoneHandle(Request $request){
        $filename = $request->name;
        $uploaded_image = IdolImage::where('original_name', basename($request->name))->orderBy('id','desc')->first();

        if (File::exists($uploaded_image->file_name)) {
            File::delete($uploaded_image->file_name);

            $uploaded_image->delete();

            return Response::json([
                'message' => 'Ảnh đã được xóa thành công!', 
                'base' => basename($filename)], 200);
        } else {
            return Response::json([
                'err' => 'Không tìm thấy ảnh!',
                'file_name' => $uploaded_image
            ], 400);
        }
    }

    /**
     * uploadImage
     * 
     * Upload ảnh lên DB
     *
     * @param  mixed $resource
     * @param  mixed $extension
     *
     * @return json
     */
    private function uploadImage($resource, $extension, $idol_id){
        $name = sha1(date('YmdHis') . str_random(30));
        $resize_name = $name . str_random(2) . '.' . $extension;

        $file = file_get_contents($resource);

        if (!file_exists($this->photos_path . '/'. $idol_id)) {
            mkdir($this->photos_path . '/'. $idol_id, 0777, true);
        }

        Image::make($file)
        ->resize(720, null, function ($constraints) {
            $constraints->aspectRatio();
        })
        ->save($this->photos_path . '/'. $idol_id .'/'. $resize_name); 
        $host_path = URL::to('photos').'/'. $idol_id .'/'.$resize_name;
        $server_path = $this->photos_path .'/'. $idol_id .'/' . $resize_name;
        return [
            'status' => 1,
            'message' => 'OK',
            'hostPath' => $host_path,
            'serverPath'=> $server_path
        ];
    }

    /**
     * getProfilePage
     * 
     * trả về trang Profile
     *
     * @return view
     */
    public function getProfilePage() {
        return view('collaborators.profile');
    }

    /**
     * updateProfile
     *
     * Cập nhật profile của công tác viên
     * 
     * @param  mixed $request
     *
     * @return json
     */
    public function updateProfile(Request $request) {
        $validate = Validator::make(
            $request->all(),
            [
            'bank_name' => 'required|string|max:100',
            'account_name' => 'required|string|max:100',
            'account_number' => 'required|numeric|unique:users,bank_number,'.Auth::user()->id,
            'department' => 'required|string|max:200',
            'realname'=> 'required|string|max:50'
            ],
            [
            'required' => ':attribute không được bỏ trống',
            'unique' => ':attribute đã tồn tại',
            'max' => ':attribute tối đa có :max kí tự',
            'string' => ':attribute phải là định dạng chuỗi kí tự',
            'numeric' => ':attribute phải là chuỗi số'
            ],
            [
            'bank_name' => 'tên ngân hàng',
            'account_name' => 'tên tài khoản',
            'account_number' => 'số tài khoản',
            'department' => 'chi nhánh',
            'realname' => 'tên hiển thị'
            ]
        );

        if($validate->fails()){
            return Response::json([
                'status' => 0,
                'errors' => $validate->errors()->all()
            ]);
        }

        $bank_name = $request->bank_name;
        $account_name = $request->account_name;
        $account_number = $request->account_number;
        $department = $request->department;
        $realname = $request->realname;

        try {
            $user = Auth::user();
            $user->bank_name = $bank_name;
            $user->bank_account_holder = $account_name;
            $user->bank_number = $account_number;
            $user->bank_location = $department;
            $user->name = $realname;
            $user->save();

            return Response::json([
                'status' => 1,
                'message' => 'Cập nhật thông tin thành công!'
            ]);
        } catch(Exception $ex) {
            return Response::json([
                'status' => 0,
                'errors' => ['Cập nhật thông tin thất bại!'],
                'message' => $ex.getMessage()
            ]);
        }
    }

    /**
     * updatePassword
     * 
     * Cập nhật mật khẩu của cộng tác viên
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function updatePassword(Request $request) {
        $validate = Validator::make(
            $request->all(),
            [
            'current_password' => 'required|string|min:5|max:50',
            'password' => 'required|string|min:5|max:50|confirmed',
            'password_confirmation' => 'required'
            ],
            [
            'required' => ':attribute không được bỏ trống',
            'min' => ':attribute tối thiểu có :min kí tự',
            'max' => ':attribute tối đa có :max kí tự',
            'string' => ':attribute phải là định dạng chuỗi kí tự',
            'confirmed' => ':attribute không trùng khớp'
            ],
            [
            'password' => 'mật khẩu mới',
            'current_password' => 'mật khẩu hiện tại',
            'password_confirmation' => 'xác nhận mật khẩu'
            ]
        );

        if($validate->fails()){
            return Response::json([
                'status' => 0,
                'errors' => $validate->errors()->all()
            ]);
        }

        $password = $request->password;
        $current_password = $request->current_password;

        try {
            $user = Auth::user();
            if(Hash::check($current_password, $user->password)) {
                $user->password = Hash::make($password);
                $user->save();

                return Response::json([
                    'status' => 1,
                    'message' => 'Cập nhật mật khẩu thành công!'
                ]);
            } else {
                return Response::json([
                    'status' => 0,
                    'errors' => ['Mật khẩu hiện tại không chính xác!']
                ]);
            }
        } catch(Exception $ex) {
            return Response::json([
                'status' => 0,
                'errors' => ['Cập nhật mật khẩu thất bại!'],
                'message' => $ex.getMessage()
            ]);
        }
    }
}
