<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class TokenAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('access_token')){
            return $next($request);
        }else{
            return redirect('admin/access');
        }
    }
}
