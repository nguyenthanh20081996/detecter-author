<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use DB;

class Notify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user_id = Auth::user()->id;

            $notify_image = DB::table('image_notify')
            ->join('idols', 'image_notify.idol_id', 'idols.id')
            ->select('idols.name', 'idols.id', DB::raw('count(*) as image_numbers'))
            ->where('image_notify.user_id', '=', $user_id)
            ->groupBy('idols.name', 'idols.id')
            ->orderBy('image_numbers', 'DESC')
            ->get();

            $idol_notify = DB::table('idols')
            ->join('idol_notify', 'idols.id', 'idol_notify.idol_id')
            ->select('idols.name', 'idols.id')
            ->get();

            $total_notify = 0;
            $total_notify = count($notify_image) + count($idol_notify);

            Session::put('idol_notify', $idol_notify);
            Session::put('notify', $notify_image);
            Session::put('total_notify', $total_notify);
            return $next($request);
        }else{
            return $next($request);
        }
    }
}
