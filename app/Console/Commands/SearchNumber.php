<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\System;

class SearchNumber extends Command
{
    protected $signature = 'api:update-search-number';
    protected $description = 'Update search times for user';

    public function handle()
    {
        $users = User::all();
        $number_search_system = System::where('id', '=', 2)->first()->value_site;
        foreach($users as $user){
            $user->search_numbers = (int)$number_search_system;
            $user->save();
        }
    }
}