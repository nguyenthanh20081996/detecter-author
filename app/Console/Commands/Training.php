<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Idol;
use App\Person;
use App\Image;
use App\IdolImage;
use App\Report;
use App\ReportImage;
use App\Group;

class Training extends Command
{
    protected $signature = 'api:training-group';
    protected $description = 'Add data to api about number message from systems';

    private $key = '2e5659bee964404ab3625a7322ae3555';
    private $end_point = 'https://eastasia.api.cognitive.microsoft.com/face/v1.0/';

    public function handle()
    {
        $people = Idol::where('is_approve', '=', 1)->where('is_handle', '=', 0)->get();
        $report = array();

        foreach($people as $person){
            $name = $person->name;
            $group_id = $person->group_id;

                if(empty($person->person_id)){
                    $response = $this->UploadPersonToAzure($group_id, $name);
                    $response_code = $response[0];
                    $response_result = $response[1];

                    if($response_code == 200){
                            $person_id = $response_result->personId;

                            $report_line = $name.' | '.$group_id.' | '.$person_id.' | OK';
                            $report = new Report;
                            $report->text = $report_line;
                            $report->status = 200;
                            $report->save();

                            $image_idol = IdolImage::where('idol_id', '=', $person->id)->where('is_upload', '=', 0)->get();
                            
                            foreach($image_idol as $image){
                                $url = $image->link;
                                $response_image_upload = $this->UploadImageToAzure($group_id, $person_id, $url);
                                $response_image_code = $response_image_upload[0];
                                $response_image_result = $response_image_upload[1];

                                if($response_image_code == 200){
                                    $image->persistedFaceId = $response_image_result->persistedFaceId;
                                    $image->is_upload = 1;
                                    $image->save();
                                    
                                    $report_line = $url.' ||| OK';

                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 200;
                                    $report_image->save(); 
                                }else if($response_image_code == 400){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 400;
                                    $report_image->save(); 
                                }else if($response_image_code == 401){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 401;
                                    $report_image->save(); 
                                }else if($response_image_code == 403){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 403;
                                    $report_image->save(); 
                                }else if($response_image_code == 404){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 404;
                                    $report_image->save(); 
                                }else if($response_image_code == 408){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 408;
                                    $report_image->save(); 
                                }else if($response_image_code == 409){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 409;
                                    $report_image->save(); 
                                }else if($response_image_code == 415){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 415;
                                    $report_image->save(); 
                                }else if($response_image_code == 429){
                                    $message = $response_image_result->error->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 429;
                                    $report_image->save(); 
                                }else if($response_image_code == 0){
                                    $message = $response_image_result->message;

                                    $report_line = $url.' ||| '.$message;
                                    $report_image = new ReportImage;
                                    $report_image->report_id = $report->id;
                                    $report_image->text = $report_line;
                                    $report_image->status = 0;
                                    $report_image->save(); 
                                }

                                sleep(5);
                            }

                            $person->person_id = $person_id;
                            $person->is_handle = 1;
                            $person->save();

                            $group = Group::where('personGroupId', '=', $group_id)->first();
                            if ($group->is_training == 1) {
                                $group->is_training = 0;
                                $group->save();
                            }

                    }else if($response_code == 400){
                        $message = $response_result->error->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 400;
                        $report->save();
                    }else if($response_code == 401){
                        $message = $response_result->error->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 401;
                        $report->save();
                    }else if($response_code == 403){
                        $message = $response_result->error->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 403;
                        $report->save();
                    }else if($response_code == 404){
                        $message = $response_result->error->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 404;
                        $report->save();
                    }else if($response_code == 409){
                        $message = $response_result->error->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 409;
                        $report->save();
                    }else if($response_code == 415){
                        $message = $response_result->error->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 415;
                        $report->save();
                    }else if($response_code == 429){
                        $message = $response_result->error->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 429;
                        $report->save();
                    }else if($response_code == 0){
                        $message = $response_result->message;
            
                        $report_line = $name.' | '.$group_id.' | '.$person_id.' | '.$message;
                        $report = new Report;
                        $report->text = $report_line;
                        $report->status = 0;
                        $report->save();
                    }
            
                    sleep(5);
                }else{
                        $group_id = $person->group_id;
                        $person_id = $person->person_id;
                        $image_idol = IdolImage::where('idol_id', '=', $person->id)->where('is_upload', '=', 0)->where('image_approve', '=', 1)->get();
                        
                        foreach($image_idol as $image){
                            $url = $image->link;
                            $response_image_upload = $this->UploadImageToAzure($group_id, $person_id, $url);
                            $response_image_code = $response_image_upload[0];
                            $response_image_result = $response_image_upload[1];

                            if($response_image_code == 200){
                                $image->persistedFaceId = $response_image_result->persistedFaceId;
                                $image->is_upload = 1;
                                $image->save();
                                
                                $report_line = $url.' ||| OK';

                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 200;
                                $report_image->save(); 

                                $person->person_id = $person_id;
                                $person->is_handle = 1;
                                $person->save();

                                $group = Group::where('personGroupId', '=', $group_id)->first();
                                if($group->is_training == 1){
                                    $group->is_training = 0;
                                    $group->save();
                                }
                            }else if($response_image_code == 400){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 400;
                                $report_image->save(); 
                            }else if($response_image_code == 401){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 401;
                                $report_image->save(); 
                            }else if($response_image_code == 403){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 403;
                                $report_image->save(); 
                            }else if($response_image_code == 404){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = $report->id;
                                $report_image->text = $report_line;
                                $report_image->status = 404;
                                $report_image->save(); 
                            }else if($response_image_code == 408){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 408;
                                $report_image->save(); 
                            }else if($response_image_code == 409){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 409;
                                $report_image->save(); 
                            }else if($response_image_code == 415){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 415;
                                $report_image->save(); 
                            }else if($response_image_code == 429){
                                $message = $response_image_result->error->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 429;
                                $report_image->save(); 
                            }else if($response_image_code == 0){
                                $message = $response_image_result->message;

                                $report_line = $url.' ||| '.$message;
                                $report_image = new ReportImage;
                                $report_image->report_id = -1;
                                $report_image->text = $report_line;
                                $report_image->status = 0;
                                $report_image->save(); 
                            }

                            sleep(5);
                        }
                }
        }
    }

    private function UploadPersonToAzure($group_id, $idol_name){
        try{
            $body = array("name" => $idol_name);
            $body_string = json_encode($body);

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$group_id.'/persons',
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $body_string,
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                    ),
                )
            );

            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);

            return [$httpcode, json_decode($result)];
        }catch(\Exception $e){
            $result = json_decode([
                'message' => 'Lỗi trên máy chủ'
            ]);

            $httpcode = 0;

            return [$httpcode, $result];
        }
    }

    // Upload image to auzre after upload person
    private function UploadImageToAzure($group_id, $person_id, $url){
        try{
            $body = array("url" => $url);
            $body_string = json_encode($body);

            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => $this->end_point.'persongroups/'.$group_id.'/persons'.'/'.$person_id.'/persistedFaces',
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $body_string,
                    CURLOPT_RETURNTRANSFER => true, 
                    CURLOPT_HTTPHEADER => array(    
                        'Ocp-Apim-Subscription-Key:' . $this->key,
                        'Content-Type:'.'application/json',
                    ),
                )
            );

            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close ($ch);
            return [$httpcode, json_decode($result)];
        }catch(\Exception $e){
            $result = json_decode([
                'message' => 'Lỗi trên máy chủ'
            ]);

            $httpcode = 0;

            return [$httpcode, $result];
        }
    }

}