<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/facebook', 'Auth\LoginController@RedirectToProviderFacebook');
Route::get('auth/facebook/callback', 'Auth\LoginController@HandelProviderFacebookCallback');

Route::get('auth/google', 'Auth\LoginController@RedirectToProviderGoogle');
Route::get('auth/google/callback', 'Auth\LoginController@HandelProviderGoogleCallback');
Route::get('dang-xuat', function(){
    Auth::logout();
    return redirect('/');
});

Route::get('top-nguoi-dung', 'UserController@GetTopUserPage')->middleware('notifications');

Route::get('/', 'UserController@GetSearchPage')->middleware('notifications');
Route::get('danh-sach-idols', 'UserController@GetListIdolsPage')->middleware('notifications');
Route::get('chia-se-idols', 'UserController@GetShareIdolPage')->middleware('notifications');
Route::post('search', 'UserController@SeachIdol');
Route::post('idol/share/url', 'UserController@UploadIdolByUrl');
Route::post('idol/share/file', 'UserController@UploadIdolByFile');
Route::post('change/name', 'UserController@UserChangeName');
Route::get('idol/thong-tin/{id}', [
    'uses' => 'UserController@GetIdolDetail',
    'as' => 'IdolDetail'
])->middleware('notifications');
Route::post('vote', 'UserController@Voting');
Route::get('not-found', function(){
    return view('user.notfound');
});


Route::get('top/nguoi-dung/{id}/hinh-anh', [
    'uses' => 'UserController@GetTopUserWithImage',
    'as' => 'TopUserHinhAnh'
]);
Route::get('top/nguoi-dung/{id}/idol', [
    'uses' => 'UserController@GetTopUserWithIdol',
    'as' => 'TopUserIdol'
]);
Route::get('notification-image/{id}', [
    'uses' => 'UserController@RemoveNotificationImage',
    'as' => 'NotificationImage'
]);

Route::get('notification-idol/{id}', [
    'uses' => 'UserController@RemoveNotificationIdol',
    'as' => 'NotificationIdol'
]);

Route::get('ajax/idol/get', 'UserController@GetMoreIdol');
Route::get('ajax/idol/search', 'UserController@SearchIdol');
Route::post('ajax/idol/search/file', 'UserController@SearchIdolWithFile');
Route::get('ajax/total/new', 'UserController@GetNewTotal');
Route::post('ajax/image-idol/by-user', 'UserController@UploadImageByUser');

Route::get('ajax/top-user/images', 'UserController@GetImageByUserId');

Route::get('admin/access', 'AdminController@GetLoginPage');
Route::post('admin/access/handle', 'AdminController@Login');

//Route của collaborator
Route::get('collaborators/login', function () {
    return view('collaborators.login');
});
Route::post('collaborators/login', 'Auth\LoginController@CtvLogin');
Route::get('collaborators/logout','Auth\LoginController@CtvLogout');
Route::group(['prefix' => 'collaborators', 'middleware' => 'collaborator'], function(){
    Route::get('dashboard', 'CollaboratorController@GetDashBoardPage');
    Route::get('upload-image/{id?}', 'CollaboratorController@getUploadPage');
    Route::get('profile', 'CollaboratorController@getProfilePage');
    //route về upload thông tin idol
    Route::post('upload-idols/file','CollaboratorController@uploadIdolByFile');
    Route::post('upload-idols/url','CollaboratorController@uploadIdolByUrl');
    Route::post('upload-idols/upload-images','CollaboratorController@saveImageDropzoneHandle');
    Route::post('upload-idols/remove-images','CollaboratorController@removeImageDropzoneHandle');
    Route::post('profile/update', 'CollaboratorController@updateProfile');
    Route::post('profile/updatePassword', 'CollaboratorController@updatePassword');
});

Route::group(['prefix'=>'admin', 'middleware' => 'token_access'], function(){
    Route::get('dashboard', 'AdminController@GetPageDashboard');
    Route::get('groups', 'AdminController@GetPageGroup');
    Route::post('group/create', 'AdminController@CreateGroup');
    Route::post('group/update', 'AdminController@UpdateGroup');
    Route::post('group/delete', 'AdminController@DeleteGroup');
    Route::get('group/search', 'AdminController@SearchGroup');
    Route::post('group/training', 'AdminController@TrainingGroup');
    Route::get('up-person', 'AdminController@GetUpPersonPage');
    Route::get('person', 'AdminController@GetPersonPage');
    Route::post('person/update', 'AdminController@UpdatePerson');
    Route::post('person/delete', 'AdminController@DeletePerson');
    Route::get('person/search', 'AdminController@SearchPerson');
    Route::get('report-today', 'AdminController@GetReportToday');
    Route::post('up-person/upload', 'AdminController@UploadIdols');
    Route::get('approve', 'AdminController@GetApprovePage');
    Route::get('up-file', 'AdminController@UpPersonWithFile');

    Route::post('approve/handle', 'AdminController@ApproveHandle');
    Route::post('delete/handle', 'AdminController@DeleteHandle');
    Route::post('des/update', 'AdminController@SetDescription');
    Route::post('person/upload/addimage', 'AdminController@UploadAddImage');
    Route::post('person/image/delete/face', 'AdminController@DeleteFace');
    Route::post('person/upload/addimage/changethumb', 'AdminController@ChangeThumbForIdol');
    Route::post('images-save', 'AdminController@SaveImageDropzoneHandle');
    Route::post('images-delete', 'AdminController@DeleteImageDropzoneHandle');

    Route::post('add-collaborator', 'AdminController@AddCollaborator');
    Route::get('collaborator-management', 'AdminController@GetCollaborator');
    Route::get('collaborator-blocking', 'AdminController@getBlockCollaboratorPage');
    Route::get('collaborator/fetch-data', 'AdminController@searchCollaborator');
    Route::post('collaborator/blocking', 'AdminController@updateBlockingStatus');
    Route::get('pendent-list','AdminController@getListPendentIdolPage');
    Route::get('collaborator/check_approve/{id}','AdminController@getApproveIdolPage');
    Route::post('collaborator/check_approve','AdminController@approveIdol');
    Route::post('collaborator/check_reject','AdminController@rejectIdol');
    Route::get('idol-image/delete','AdminController@deleteIdolImage');

    Route::get('images-pending-list', 'AdminController@ImagePendingPage');

    Route::get('voting', 'AdminController@GetVotingPage');
    
    Route::get('person/image/showing/{id}', [
        'uses' => 'AdminController@GetPageSetImageShowing',
        'as' => 'SetImageShowing'
    ]);

    Route::get('add-image/{id}', [
        'uses' => 'AdminController@GetAddImagePage',
        'as' => 'AddImage'
    ]);

    Route::get('pending-images/{id}', [
        'uses' => 'AdminController@GetHandlePendingImagePage',
        'as' => 'PendingImages'
    ]);

    Route::post('images/setting', 'AdminController@UpdateImagesShowing');

    Route::get('discription/{id}', [
        'uses' => 'AdminController@UpdateDescription',
        'as' => 'Description'
    ]);

    Route::get('collaborator-payment', 'AdminController@GetPaymentPage');

    Route::post('pending-image/approve', 'AdminController@AcceptImageUser');
    Route::post('pending-image/delete', 'AdminController@DeleteImageUser');
    Route::post('ctv/payment', 'AdminController@CTVPayment');
});

Route::fallback(function(){ return redirect()->route('/'); });
